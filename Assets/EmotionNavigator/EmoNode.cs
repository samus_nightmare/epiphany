﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EmoNode : MonoBehaviour
{
    public UILabel Name;
    public UITexture Icon;
    public GameObject[] CurrentArrows;

    public Transform CurrentPoint;

    [HideInInspector]
    public EmotionalNode node = null;
    bool isSelected = false;

    void Update()
    {
        Orientate();
    }

    void Orientate()
    {
        Name.transform.LookAt(UIManager.Instance.EmoNavigator.NavigationCamera.transform.position);
        Icon.transform.LookAt(UIManager.Instance.EmoNavigator.NavigationCamera.transform.position);

        Name.transform.localScale = new Vector3(-0.2f, 0.2f, 1);
        Icon.transform.localScale = new Vector3(-0.5f, 0.5f, 1);

        Name.transform.rotation = Quaternion.Euler(new Vector3(Name.transform.rotation.eulerAngles.x, 180, Name.transform.rotation.eulerAngles.z));
        Icon.transform.rotation = Quaternion.Euler(new Vector3(Icon.transform.rotation.eulerAngles.x, 180, Icon.transform.rotation.eulerAngles.z));
    }

    public void SetCurrentArrows(bool active)
    {
        foreach (GameObject _obj in CurrentArrows)
        {
            _obj.SetActive(active);
        }
    }


    public void OnMouseDown()
    {
        if (!isSelected)
        {
            UIManager.Instance.EmoNavigator.ClickOnNode(this, node);
        }
    }

    public void SetData(EmotionalNode newNode, bool newIsSelected = false)
    {
        node = newNode;
        isSelected = newIsSelected;
        SetCurrentArrows(isSelected);

        EmotionalNode.Data _data = node.Info.FirstOrDefault(x => x.language == LocalizationSystem.currentLanguage);
        if (_data == null)
        {
            Debug.LogError("Not found language " + LocalizationSystem.currentLanguage + " for node " + node.gameObject.name);
            return;
        }

        Name.text = _data.name;
    }
}
