﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EmotionsNavigator : MonoBehaviour
{
    public UILabel Description;
    public Camera NavigationCamera;
    public SpringPosition NavigationSpringPosition;
    public GameObject NodePrefab;
    public Transform CurrentNodePosition;
    public Transform[] NextNodesPositions;
    public FirstNodeDecider FirstNodeDecider;
    public GameObject FinalNodeButton;
    public EmotionRecipeControl RecipeControl;
    public GameObject[] OtherButtons;
    public TweenAlpha AlphaCurtain;

    Vector3 _originalCamPosition;

    [HideInInspector]
    public List<EmoNode> SpawnedNodes = new List<EmoNode>();

    [HideInInspector]
    public EmoNode CurrentlySelectedNode = null;

    void Start()
    {
        _originalCamPosition = NavigationCamera.transform.position;
        NavigationCamera.enabled = false;
        UIManager.Instance.MenuBackground.Play(true);
    }

    public void Open()
    {
        CurrentlySelectedNode = null;
        RemoveAllNodes();
        CloseNavigator();
        ShowFinalNodeMsg(false);
        FirstNodeDecider.Open();
        ShowOtherButtons(false);

        NavigationCamera.enabled = true;
        UIManager.Instance.MenuBackground.Play(false);
    }

    public void Close()
    {
        NavigationCamera.enabled = false;
        CloseNavigator();
        FirstNodeDecider.Close();
        ShowFinalNodeMsg(false);
        RemoveAllNodes();
        ShowOtherButtons(false);

        UIManager.Instance.MenuBackground.Play(true);
    }

    public void SelectedCategory(EmotionalNode node)
    {
        FirstNodeDecider.Close();

        ShowOtherButtons(true);

        ShowNavigator();

        SelectNode(node);

        FinalNodeButton.SetActive(false);
    }

    public void ShowNavigator()
    {
        Description.transform.parent.gameObject.SetActive(true);
    }

    public void CloseNavigator()
    {
        Description.transform.parent.gameObject.SetActive(false);
        Description.text = "";
    }

    public void SpawnNewNode(EmotionalNode node, Transform SpawnPoint)
    {
        GameObject _newNodeObject = Instantiate(NodePrefab, SpawnPoint.position, Quaternion.identity) as GameObject;
        _newNodeObject.transform.parent = transform;
        _newNodeObject.transform.localScale = Vector3.one;

        EmoNode _emoNode = _newNodeObject.GetComponent<EmoNode>();

        EmotionalNode.Data _data = node.Info.FirstOrDefault(x => x.language == LocalizationSystem.currentLanguage);
        if (_data == null)
        {
            Debug.LogError("Not found language " + LocalizationSystem.currentLanguage + " for node " + node.gameObject.name);
            return;
        }

        //Rellenar con datos
        _emoNode.CurrentPoint = SpawnPoint;
        bool _isCurrent = (SpawnPoint == CurrentNodePosition);
        _emoNode.SetData(node, _isCurrent);
        if (_isCurrent)
        {
            CurrentlySelectedNode = _emoNode;
            Description.text = _data.description;
        }
        SpawnedNodes.Add(_emoNode);
    }

    void RemoveNode(Transform SpawnPoint)
    {
        int _toDelIndex = SpawnedNodes.FindIndex(x => x.CurrentPoint == SpawnPoint);
        if (_toDelIndex >= 0)
        {
            Destroy(SpawnedNodes[_toDelIndex].gameObject);
            SpawnedNodes.RemoveAt(_toDelIndex);
        }
    }

    void RemoveAllNodes()
    {
        while (SpawnedNodes.Count > 0)
        {
            Destroy(SpawnedNodes[0].gameObject);
            SpawnedNodes.RemoveAt(0);
        }
    }

    public void SelectNode(EmotionalNode node)
    {
        //Clean previous nodes
        RemoveAllNodes();

        //Selected node
        SpawnNewNode(node, CurrentNodePosition);

        if (node.Childs.Count < 1)
        {
            //Soy un nodo hoja
            ShowFinalNodeMsg(true);
        }
        else
        {
            //Show Immediate Children
            ShowFinalNodeMsg(false);

            List<EmotionalNode> _children = node.Childs;
            for (int i = 0; i < Mathf.Min(_children.Count, NextNodesPositions.Length); i++)
            {
                SpawnNewNode(_children[i], NextNodesPositions[i]);
            }
        }

    }

    public void ShowFinalNodeMsg(bool show)
    {
        FinalNodeButton.SetActive(show);
    }

    public void ProceedToSummary()
    {
        RecipeControl.Open(CurrentlySelectedNode.node);
        Close();
    }

    public void ClearNavigation()
    {
        Open();
    }

    public void BackNavigation()
    {
        //todo
    }

    void ShowOtherButtons(bool show)
    {
        foreach (GameObject _obj in OtherButtons)
        {
            _obj.SetActive(show);
        }
    }

    public void ClickOnNode(EmoNode _emoNode, EmotionalNode _infoNode)
    {
        NavigationSpringPosition.onFinished = delegate(SpringPosition spring)
        {
            AlphaCurtain.Toggle();

            SelectNode(_infoNode);

            NavigationSpringPosition.transform.position = CurrentNodePosition.position;

            NavigationSpringPosition.onFinished = null;

            NavigationSpringPosition.enabled = false;
        };

        NavigationSpringPosition.target = _emoNode.transform.position;

        NavigationSpringPosition.enabled = true;

        AlphaCurtain.Toggle();

    }

}
