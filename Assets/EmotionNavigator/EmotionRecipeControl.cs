﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EmotionRecipeControl : MonoBehaviour
{
    public GameObject NodePrefab;

    public UITable NodesTable;


    List<NodeSummaryElement> SpawnedNodesList = new List<NodeSummaryElement>();

    public void Open(EmotionalNode node)
    {
        RemoveAllNodes();

        UIManager.Instance.ChangeWindow(7);

        foreach (EmotionalNode _node in node.GetParentTrace())
        {
            AddNode(_node);
        }

        NodesTable.repositionNow = true;
    }

    public void RemoveAllNodes()
    {
        while (SpawnedNodesList.Count > 0)
        {
            Destroy(SpawnedNodesList[0].gameObject);
            SpawnedNodesList.RemoveAt(0);
        }
    }

    public void AddNode(EmotionalNode node)
    {
        GameObject _newNode = Instantiate(NodePrefab, NodesTable.transform.position, Quaternion.identity) as GameObject;
        _newNode.transform.parent = NodesTable.transform;
        _newNode.transform.localScale = Vector3.one;

        NodeSummaryElement _summary = _newNode.GetComponent<NodeSummaryElement>();

        _summary.Set(node);

        SpawnedNodesList.Add(_summary);
    }
}
