﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class NodeSummaryElement : MonoBehaviour
{
    public UILabel Name;
    public UITexture Icon;

    public void Set(EmotionalNode node)
    {
        EmotionalNode.Data _data = node.Info.FirstOrDefault(x => x.language == LocalizationSystem.currentLanguage);

        Name.text = _data.name;

        Icon.mainTexture = node.Icon;
    }
}
