﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FirstNodeDecider : MonoBehaviour
{
    public GameObject OptionPrefab;

    public UIGrid OptionsGrid;

    public UILabel Title;


    List<FirstNodeOption> OptionsList = new List<FirstNodeOption>();

    public void Open()
    {
        gameObject.SetActive(true);

        Title.text = LocalizationSystem.Instance.translation.emo_SelectCategory;
        Title.gameObject.SetActive(true);

        if (OptionsList.Count < 1)
        {
            foreach (EmotionalNode _node in UIManager.Instance.EmoTree.Childs)
            {
                AddNewCategory(_node);
            }
        }
    }

    public void AddNewCategory(EmotionalNode node)
    {
        GameObject _newCatgObj = Instantiate(OptionPrefab, OptionsGrid.transform.position, Quaternion.identity) as GameObject;
        _newCatgObj.transform.parent = OptionsGrid.transform;
        _newCatgObj.transform.localScale = Vector3.one;

        FirstNodeOption _option = _newCatgObj.GetComponent<FirstNodeOption>();

        //Rellenar datos
        _option.SetNode(node);

        OptionsList.Add(_option);

        OptionsGrid.Reposition();
    }

    public void CleanAll()
    {
        while (OptionsList.Count > 0)
        {
            Destroy(OptionsList[0].gameObject);
            OptionsList.RemoveAt(0);
        }
    }

    public void Close()
    {
        gameObject.SetActive(false);
        Title.gameObject.SetActive(false);
    }
}
