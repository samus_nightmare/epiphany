﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FirstNodeOption : MonoBehaviour
{
    public UITexture Icon;
    public UILabel Name;

    EmotionalNode Node = null;

    void OnClick()
    {
        UIManager.Instance.EmoNavigator.SelectedCategory(Node);
    }

    public void SetNode(EmotionalNode newNode)
    {
        Node = newNode;

        EmotionalNode.Data _data = Node.Info.FirstOrDefault(x => x.language == LocalizationSystem.currentLanguage);
        if (_data == null)
        {
            Debug.LogError("Not found language " + LocalizationSystem.currentLanguage + " for node " + newNode.gameObject.name);
            return;
        }

        Name.text = _data.name;
    }
}
