﻿using UnityEngine;
using System.Collections;

public class RotateElement : MonoBehaviour
{
    public enum Axises { X = 0, Y, Z };

    public Axises Axis = Axises.Y;

    public float Speed = 1;

    void Update()
    {
        switch (Axis)
        {
            case Axises.X: transform.Rotate(Vector3.right, Speed); break;
            case Axises.Y: transform.Rotate(Vector3.up, Speed); break;
            case Axises.Z: transform.Rotate(Vector3.forward, Speed); break;
        }
    }
}
