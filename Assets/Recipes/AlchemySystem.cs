﻿using UnityEngine;
using System;
using System.Collections.Generic;
using ProfileGlobals;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

public class AlchemySystem
{
    private static String ALCHEMY_PATH
    {
        get
        {     
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    return "Recipes/recipes";

                default:
                    return "Assets/Resources/Recipes/recipes.txt";
            }
        }
        set { }
    }

    [XmlRoot("alchemy")]
    public class XMLAlchemy
    {
        [XmlArray("book")]
        [XmlArrayItem("effect")]
        public List<XMLEffect> recipies;

        public override string ToString()
        {
            //Debug.Log("XMLAlchemy recipies -> " + recipies.Count);
            string r = "";
            foreach (XMLEffect e in recipies)
                r += "XMLEffect " + e.ToString() + "\n";
            return r;
        }
    }

    [XmlRoot("effect")]
    public class XMLEffect
    {
        [XmlArray("localization")]
        [XmlArrayItem("info")]
        public List<Info> localization = new List<Info>();

        [XmlArray("recipe")]
        [XmlArrayItem("ingredient")]
        public List<Ingredient> recipie = new List<Ingredient>();

        public Info FindLocalization(string lan)
        {
            if (localization.Count < 1)
                Debug.LogError("An XML Effect not has any language defineded");

            Info i = localization.Find(x => x.language == lan);

            if (i == null)
                i = localization.First();

            return i;
        }

        public override string ToString()
        {
            string r = "";
            foreach (Info e in localization)
                r += e.ToString() + "\n";
            return r;
        }
    }

    [Serializable]
    [XmlRoot("ingredient")]
    public class Ingredient
    {
        [XmlAttribute("emotion")]
        public string emotion { get; set; }
        [XmlText]
        public int count { get; set; }
    }

    [Serializable]
    [XmlRoot("info")]
    public class Info
    {
        [XmlAttribute("language")]
        public string language;
        [XmlElement("name")]
        public string name;
        [XmlElement("description")]
        public string description;

        public override string ToString()
        {
            return name + " - " + language + " - " + description;
        }
    }


    public class Alchemy
    {
        ///Sumary
        ///int: Indice potencia de los elementos para indexar
        ///List: Lista de Efectos que coinciden en potencia
        ///Order. Al devolver los elementos, ordenarlos para cumplir los requisitos
        ///     1. Por Potencia
        ///     2. Numero de elementos diferentes
        ///     3. El que tenga el elemento con mas cantidad
        public Dictionary<int, List<Effect>> AllEffects = new Dictionary<int, List<Effect>>();

        public override string ToString()
        {
            string r = "";

            foreach (KeyValuePair<int, List<Effect>> pair in AllEffects)
            {
                r += "Potency -> " + pair.Key + " Count -> " + pair.Value.Count + "\n";
                foreach (Effect e in pair.Value)
                    r += e.ToString() + "\n";
            }
            return r;
        }

        public Effect GetEffect(Dictionary<Feelings, int> _newEmotionsTable)
        {
            List<Effect> CandidateList = new List<Effect>();
            List<Effect> AuxList = new List<Effect>();


            int potence = 0;
            foreach (KeyValuePair<ProfileGlobals.Feelings, int> entry in _newEmotionsTable)
                potence += entry.Value;

            //Debug.Log("Calculate Potence -> " + potence);

            for (int i = 1; i <= potence; i++)
            {
                //Debug.Log("**************************************************************\nPaso por la I -> " + i);
                //En el caso de no existir
                if (AllEffects.TryGetValue(i, out AuxList))
                {
                    //Debug.Log("Potencia!");
                    foreach (Effect effect in AuxList)
                    {
                        //Debug.Log("Compare con " + effect.Name);
                        if (effect.CompareWithEmotionsTable(_newEmotionsTable))
                        {
                            //Debug.Log("Añado " + effect.Name);
                            //Debug.Log("Find Effect -> " + effect.Name);
                            CandidateList.Add(effect);
                        }
                    }
                }
                //else
                //Debug.Log("Noy hay " + i + " en Alquimia\n" + this.ToString());
            }

            //Debug.Log("Sort");
            CandidateList.Sort();

            //string r = "CandidateList [";
            //foreach(Effect we in CandidateList)
            //    r +=  we.Name + ", ";
            //Debug.Log(r + " ]");

            return CandidateList.FirstOrDefault();
        }
    }

    public class Effect : IComparable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        //todo: Aquí debería haber eventos en plan: pierde un brazo por el exceso de "magia", cae en coma, etc.

        public Dictionary<ProfileGlobals.Feelings, int> recipie = new Dictionary<Feelings, int>();

        public string DicToString(Dictionary<ProfileGlobals.Feelings, int> dic)
        {
            string r = "";
            foreach (KeyValuePair<ProfileGlobals.Feelings, int> pair in dic)
                r += "Feelings -> " + pair.Key.ToString() + " Count -> " + pair.Value + "\n";
            return r;
        }

        public bool CompareWithEmotionsTable(Dictionary<ProfileGlobals.Feelings, int> emotionsTable)
        {
            //Debug.Log("CompareWithEmotionsTable");

            foreach (KeyValuePair<ProfileGlobals.Feelings, int> Recipie in recipie)
            {
                //Debug.Log("emotionsTable\n" + DicToString(emotionsTable));
                //Debug.Log("recipie\n" + DicToString(recipie));

                int TableValue;
                if (emotionsTable.TryGetValue(Recipie.Key, out TableValue))
                {
                    //Debug.Log("Tengo " + pair.Key.ToString());
                    if (Recipie.Value > TableValue)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Returns Sum of all elements in the recipie
        /// </summary>
        public int GetPotency()
        {
            int sum = 0;
            foreach (KeyValuePair<ProfileGlobals.Feelings, int> entry in recipie)
                // do something with entry.Value or entry.Key
                sum += entry.Value;
            return sum;
        }

        /// <summary>
        /// Returns total of diferent elements in Recipie
        /// </summary>
        public int GetDifferentElements()
        {
            return recipie.Keys.Count;
        }

        /// <summary>
        /// Returns the biggest element Count
        /// </summary>
        public int GetBiggerElementPotency()
        {
            return recipie.Values.Max();
        }

        public int CompareTo(object obj)
        {
            Effect otherEffect = obj as Effect;
            if (otherEffect != null)
            {
                int result = otherEffect.GetPotency().CompareTo(this.GetPotency());
                result = result == 0 ? result : result = otherEffect.GetDifferentElements().CompareTo(this.GetDifferentElements());
                return result == 0 ? result : result = otherEffect.GetBiggerElementPotency().CompareTo(this.GetBiggerElementPotency());
            }
            else
                throw new ArgumentException("Object is not a Effect");
        }

        public override bool Equals(object obj)
        {
            Effect otherEffect = obj as Effect;
            if (otherEffect == null)
                return false;
            else 
                return this.recipie.Count == otherEffect.recipie.Count && !this.recipie.Except(otherEffect.recipie).Any();
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public static Alchemy LoadAlchemy()
    {
#if UNITY_WEBPLAYER
        return new Alchemy();
#else

        //UIManager.Instance.AndroidDebug("Load Alchemy");
        XmlSerializer ser = new XmlSerializer((typeof(XMLAlchemy)));
        XMLAlchemy readedAlchemy = null;
        TextReader reader;
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                TextAsset RecipesFile = Resources.Load(ALCHEMY_PATH) as TextAsset; //Recordar que para hacer el Resources.Load() no se pone la extensión del archivo.
                reader = new StringReader(RecipesFile.text);
                break;
            default:
                reader = new StringReader(System.IO.File.ReadAllText(ALCHEMY_PATH));
                break;
        }

        readedAlchemy = (XMLAlchemy)ser.Deserialize(reader);

        //TODO AQUÍ: Hay que pasar el texto de este RecipesFiles.text al deserializer. Esto no sé cómo hacerlo.

        if (readedAlchemy == null) {
            UIManager.Instance.AndroidDebug("Alchemy == null");
            return null;
        }

        //Debug.Log("Alchemy = " + readedAlchemy.ToString());
        //UIManager.Instance.AndroidDebug("Alchemy Loaded");

        //Convertir a Alchemy
        Alchemy alchemy = new Alchemy();

        //Debug.Log("Numero de Emociones leidas " + readedAlchemy.recipies.Count);
        foreach (XMLEffect effect in readedAlchemy.recipies)
        {
            Effect e = new Effect();
            Info info = effect.FindLocalization(LocalizationSystem.currentLanguage);

            //Convertimos de XML a Effect
            e.Name = info.name;
            e.Description = info.description;

            //Sacamos la lista de ingredientes de XML
            foreach (Ingredient ingredient in effect.recipie)
            {
                e.recipie.Add((Feelings)Enum.Parse(typeof(Feelings), ingredient.emotion), ingredient.count);
            }

            int potency = e.GetPotency();
            //Añadimos a la Alquimia Indexada por Potencia
            //Debug.Log(e.ToString() + " - " + "Potency " + potency );

            if (!alchemy.AllEffects.ContainsKey(potency))
                alchemy.AllEffects[potency] = new List<Effect>();

            //TODO: Detectar aqui "por debug", que no tengo otras emociones con la misma receta
            var element = alchemy.AllEffects[potency].FindAll(s => s.Equals(e) || s.Name == e.Name);
            if (element.Any()) 
                Debug.Log("Elementos iguales a " + e.Name + " -> {" + 
                    String.Join(", ", element.ConvertAll(x => x.Name).ToArray<string>()) + "}");

            alchemy.AllEffects[potency].Add(e);
        }
        //UIManager.Instance.AndroidDebug("Alchemy Break 6");
        return alchemy;

#endif
    }
}
