﻿using UnityEngine;
using System.Collections;
using ProfileGlobals;
using System.IO;
using System.Collections.Generic;

public class ProfileManager : MonoBehaviour
{
    public Profile profile;

    [HideInInspector]
    public ProfileManagerDamage pmDamage;

    [HideInInspector]
    public ProfileManagerEmotions pmEmotions;

    [HideInInspector]
    public ProfileManagerSituations pmSituations;

    [HideInInspector]
    public static List<Profile.Modifier> DefaultModifiers = new List<Profile.Modifier>();

    [HideInInspector]
    public AlchemySystem.Alchemy pmAlchemy;

    List<string> storedProfiles = new List<string>();


    public void Init()
    {
        Setup();

        storedProfiles = ProfileXMLSerializer.getAllStoredProfiles();

        if (storedProfiles.Count < 1)
        {

            //Recién iniciado en el sistema (está vacío). Crear los profiles por defecto
            CreateDefaultProfiles();
            storedProfiles.AddRange(ProfileXMLSerializer.getAllStoredProfiles());

        }

        LoadFirstProfile();
    }

    public void loadDefaultProfile()
    {
        loadProfile(storedProfiles[0]);
    }

    public void loadProfile(string name)
    {
        profile = ProfileXMLSerializer.deserializeProfile(name);
        profile.experience = NUIManager.Instance.ProfileEditor.ExpCount(profile);
    }

    public void writeProfile()
    {
        ProfileXMLSerializer.serializeProfile(profile);
    }

    public void deleteProfile(string deletingFile)
    {
        //todo borrar el profile con el nombre "deletingFile".
        //todo: si borras el mismo profile que tienes cargado, al borrarse, tiene que cargarse el primer profile que haya (o uno random).

        ProfileXMLSerializer.DeleteFileByName(deletingFile);
        storedProfiles = ProfileXMLSerializer.getAllStoredProfiles();
    }

    public Profile getProfileInfo(string name)
    {
        Profile pinfo = ProfileXMLSerializer.deserializeProfile(name);
        return pinfo;
    }

    void Setup()
    {

        pmDamage = new ProfileManagerDamage();
        pmDamage.profileParent = this;

        pmSituations = new ProfileManagerSituations();
        pmSituations.profileParent = this;

        pmEmotions = new ProfileManagerEmotions();
        pmEmotions.profileParent = this;

        pmAlchemy = AlchemySystem.LoadAlchemy();

        GameObject gomgr = GameObject.Find("UIManager");
        if (gomgr != null)
        {
            pmEmotions._UIManager = gomgr.GetComponent<UIManager>();
        }


        LoadDefaultModifiers();

        if (Application.platform == RuntimePlatform.Android)
            ProfileXMLSerializer.ConvertAllBuiltInProfiles();

    }

    void LoadDefaultModifiers()
    {
        //todo: cargar los modificadores por defecto de un XML o algo así
        ProfileManager.DefaultModifiers.Clear();

        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Medicina"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Informática"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Tecnología"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Lucha"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Charlatán"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Oratoria"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Armas fuego"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Artillería"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Armas cortas"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Sigilo"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Orientación"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(1, "Teatro"));

        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(-1, "Obsesión"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(-1, "Enfermedad"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(-3, "Miembro inútil"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(-1, "Alergia"));
        ProfileManager.DefaultModifiers.Add(new Profile.Modifier(-1, "Rutina"));
    }

    public void CreateNewEmptyProfile(string newProfileName = "New Character")
    {
        profile = new Profile();
        profile.name = newProfileName;
        profile.setAtribute(Attributes.INSIGHT, string.Empty + (int)AttributesLevel.POOR);
        profile.setAtribute(Attributes.PRESENCE, string.Empty + (int)AttributesLevel.POOR);
        profile.setAtribute(Attributes.SKILL, string.Empty + (int)AttributesLevel.POOR);
        profile.setAtribute(Attributes.VIGOR, string.Empty + (int)AttributesLevel.POOR);

        profile.setFeeling(Feelings.ANGER, string.Empty + 0);
        profile.setFeeling(Feelings.ANTICIPATION, string.Empty + 0);
        profile.setFeeling(Feelings.DISGUST, string.Empty + 0);
        profile.setFeeling(Feelings.FEAR, string.Empty + 0);
        profile.setFeeling(Feelings.JOY, string.Empty + 0);
        profile.setFeeling(Feelings.SADNESS, string.Empty + 0);
        profile.setFeeling(Feelings.SURPRISE, string.Empty + 0);
        profile.setFeeling(Feelings.TRUST, string.Empty + 0);

        writeProfile();
    }

    public void CreateDefaultProfiles()
    {
        //Crear profiles por defecto aquí

        CreateDefaultProfile(
            "Ciudadano",            //Nombre
            false,                  //Es exponente
            "",                     //Conducta
            AttributesLevel.POOR,   //VIG
            AttributesLevel.POOR,   //DES
            AttributesLevel.AVERAGE,   //INT
            AttributesLevel.POOR,   //PRE
            //Modificadores
            new List<Profile.Modifier> { new Profile.Modifier(1, "Conducción") },
            //Prodigios
            new List<string> { },
            //Anotaciones
            ""
            );


        CreateDefaultProfile(
            "Búho",            //Nombre
            true,                  //Es exponente
            "Patrón",                     //Conducta
            AttributesLevel.COMPETENT,        //VIG
            AttributesLevel.EXCELLENT,   //DES
            AttributesLevel.EXCELLENT,   //INT
            AttributesLevel.AVERAGE,    //PRE
            //Modificadores
            new List<Profile.Modifier> { 
                new Profile.Modifier(1, "Armas disparo") ,
                new Profile.Modifier(1, "Disfraz") ,
                new Profile.Modifier(2, "Perceptivo") ,
            },
            //Prodigios
            new List<string> {
            "LEERMENTE",
            "SINCRONIA",
            "NAVEGACION",
            "MOVIMIENTOSUP"
            },
            //Anotaciones
            ""
            );



        CreateDefaultProfile(
           "Militar",            //Nombre
           false,                  //Es exponente
           "",                     //Conducta
           AttributesLevel.COMPETENT,        //VIG
           AttributesLevel.EXCELLENT,   //DES
           AttributesLevel.AVERAGE,   //INT
           AttributesLevel.POOR,    //PRE
            //Modificadores
           new List<Profile.Modifier> { 
                new Profile.Modifier(1, "Armas disparo") ,
                new Profile.Modifier(1, "Combate C.a.C.") ,
            },
            //Prodigios
           new List<string>
           {

           },
            //Anotaciones
           ""
           );

        CreateDefaultProfile(
           "Snob",            //Nombre
           false,                  //Es exponente
           "",                     //Conducta
           AttributesLevel.POOR,        //VIG
           AttributesLevel.POOR,   //DES
           AttributesLevel.AVERAGE,   //INT
           AttributesLevel.COMPETENT,    //PRE
            //Modificadores
           new List<Profile.Modifier> { 
                new Profile.Modifier(1, "Persuasión") ,
                new Profile.Modifier(1, "Arte") ,
            },
            //Prodigios
           new List<string>
           {

           },
            //Anotaciones
           ""
           );

        CreateDefaultProfile(
           "Rebelde",            //Nombre
           true,                  //Es exponente
           "",                     //Conducta
           AttributesLevel.AVERAGE,        //VIG
           AttributesLevel.EXCELLENT,   //DES
           AttributesLevel.EXCELLENT,   //INT
           AttributesLevel.AVERAGE,    //PRE
            //Modificadores
           new List<Profile.Modifier> { 
                new Profile.Modifier(1, "Acróbata") ,
                new Profile.Modifier(1, "Orientación") ,
            },
            //Prodigios
           new List<string>
           {
               "NAVEGACION",
               "IMPULSO",
               "VELO"
           },
            //Anotaciones
           ""
           );


    }

    void CreateDefaultProfile(string _name, bool _isExponent, string _conduct, AttributesLevel _VIG, AttributesLevel _DEX, AttributesLevel _INT, AttributesLevel _PRE, List<Profile.Modifier> _Modifiers, List<string> _Powers, string _Annotations)
    {
        Profile _profile = new Profile();

        _profile.name = _name;
        _profile.health = HealthLevels.FINE;
        _profile.catharsis = _isExponent ? 1 : 0;
        _profile.conduct = _conduct;

        _profile.setAtribute(ProfileGlobals.Attributes.VIGOR, _VIG);
        _profile.setAtribute(ProfileGlobals.Attributes.SKILL, _DEX);
        _profile.setAtribute(ProfileGlobals.Attributes.INSIGHT, _INT);
        _profile.setAtribute(ProfileGlobals.Attributes.PRESENCE, _PRE);

        _profile.modifiers = _Modifiers;

        _profile.powers = _Powers.ToArray();

        _profile.annotations = _Annotations;

        _profile.unalterable = true;

        storedProfiles.Add(_name);

        profile = _profile;

        #if UNITY_WEBPLAYER

        #else
                writeProfile();
        #endif
    }

    public List<Profile> GetAllProfiles()
    {
        storedProfiles = ProfileXMLSerializer.getAllStoredProfiles();

        List<Profile> _profiles = new List<Profile>();

        foreach (string _f in storedProfiles)
        {
            _profiles.Add(getProfileInfo(_f));
        }

        return _profiles;
    }

    public void LoadFirstProfile()
    {
        if (storedProfiles.Count > 0)
        {
            loadProfile(storedProfiles[0]);
        }
        else
        {
            Debug.LogError("Error: no profiles loaded");
        }
    }
}
