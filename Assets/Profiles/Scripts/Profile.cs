﻿using UnityEngine;
using System;
using System.Collections.Generic;
using ProfileGlobals;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

[XmlRoot("Profile")]
public class Profile
{
    [XmlArray("Attributes")]
    [XmlArrayItem("attr")]
    public string[] attributes;

    [XmlArray("Feelings")]
    [XmlArrayItem("feel")]
    public string[] feelings;

    [Serializable]
    [XmlRoot("Modifier")]
    public class Modifier
    {
        [XmlElement("Level")]
        public int level { get; set; }

        [XmlElement("Name")]
        public string name { get; set; }

        public Modifier()
        {

        }

        public Modifier(int _level, string _name)
        {
            this.level = _level;
            this.name = _name;
        }
    }
    [XmlArray("Modifiers")]
    [XmlArrayItem("modifier")]
    public List<Modifier> modifiers { get; set; }

    [XmlElement("MetaType")]
    public ProfileMetaTypes metaType { get; set; }

    [XmlElement("Tribe")]
    public Tribes tribe { get; set; }

    [XmlElement("Name")]
    public string name { get; set; }

    [XmlElement("Experience")]
    public int experience { get; set; }

    [XmlElement("UsedExperience")]
    public int used_experience { get; set; } //CARGARSE ESTE Y COMENTAR O BORRAR LO QUE PETE

    [XmlElement("HealthLevels")]
    public HealthLevels health;

    [XmlElement("Unalterable")]
    public bool unalterable;

    [XmlElement("Annotations")]
    public string annotations;

    [XmlElement("Conduct")]
    public string conduct;

    [XmlArray("Powers")]
    [XmlArrayItem("pwr")]
    public string[] powers;

    [XmlElement("Catharsis")]
    public int catharsis { get; set; }


    public Profile()
    {
        name = "New Character";
        attributes = new string[(int)Attributes.length];
        feelings = new string[(int)Feelings.length];
        modifiers = new List<Modifier>();
        powers = new string[0];
        conduct = "";
        annotations = "";
        catharsis = 1;
        unalterable = false;

        setAtribute(Attributes.VIGOR, "1");
        setAtribute(Attributes.INSIGHT, "1");
        setAtribute(Attributes.PRESENCE, "1");
        setAtribute(Attributes.SKILL, "1");

        setFeeling(Feelings.ANGER, "0");
        setFeeling(Feelings.ANTICIPATION, "0");
        setFeeling(Feelings.DISGUST, "0");
        setFeeling(Feelings.FEAR, "0");
        setFeeling(Feelings.JOY, "0");
        setFeeling(Feelings.SADNESS, "0");
        setFeeling(Feelings.SURPRISE, "0");
        setFeeling(Feelings.TRUST, "0");
    }

    public string toXML()
    {
        XmlSerializer ser = new XmlSerializer(typeof(Profile));
        StringWriter writer = new StringWriter();
        ser.Serialize(writer, this);

        return writer.ToString();
    }

    //*******************************************************************************************************
    public void addModifier(int level, string name)
    {
        Modifier mod = new Modifier();
        mod.level = level;
        mod.name = name;
        modifiers.Add(mod);
    }

    public void deleteModifier(string name)
    {
        modifiers.RemoveAll(structure => structure.name == name);
    }

    //*******************************************************************************************************

    public void setAtribute(string attr, string val)
    {
        attributes[(int)Enum.Parse(typeof(Attributes), attr)] = val;
    }

    public void setAtribute(Attributes attr, string val)
    {
        attributes[(int)attr] = val;
    }

    public void setAtribute(Attributes attr, AttributesLevel val)
    {
        attributes[(int)attr] = (val).ToString();
    }

    public string getAtribute(string attr)
    {
        return attributes[(int)Enum.Parse(typeof(Attributes), attr)];
    }

    public string getAtribute(Attributes attr)
    {
        return attributes[(int)attr];
    }

    public ProfileGlobals.AttributesLevel getAttributeLevel(Attributes attr)
    {
        return getAttributeLevel(attributes[(int)attr]);
    }

    public ProfileGlobals.AttributesLevel getAttributeLevel(string attr)
    {
        return (ProfileGlobals.AttributesLevel)Enum.Parse(typeof(ProfileGlobals.AttributesLevel), attr);
    }

    public void modify(Attributes attr, int n)
    {
        setAtribute(attr, Mathf.Max(0, Mathf.Min(Convert.ToInt32(getAtribute(attr)) + n, (int)AttributesLevel.length - 1)).ToString());
    }

    //*******************************************************************************************************

    public void setFeeling(string feel, string val)
    {
        feelings[(int)Enum.Parse(typeof(Feelings), feel)] = val;
    }

    public void setFeeling(Feelings feel, string val)
    {
        feelings[(int)feel] = val;
    }

    public string getFeeling(string feel)
    {
        return feelings[(int)Enum.Parse(typeof(Feelings), feel)];
    }

    public string getFeeling(Feelings feel)
    {
        return feelings[(int)feel];
    }

    public float GetEmotionsAverage()
    {
        int avg = 0;
        foreach (string fell in feelings)
        {
            avg += Convert.ToInt32(fell);
        }
        return avg / feelings.Length;
    }

    //*******************************************************************************************************

    public void AddPower(string ID)
    {
        List<string> _powers = powers.ToList();

        if (!_powers.Contains(ID))
            _powers.Add(ID);

        powers = _powers.Distinct().ToArray();
    }

    public void DeletePower(string ID)
    {
        List<string> _powers = powers.ToList();

        _powers.RemoveAll(x => x == ID);

        powers = _powers.ToArray();
    }

    public List<string> GetPowersIDs()
    {
        return powers.ToList();
    }
}
