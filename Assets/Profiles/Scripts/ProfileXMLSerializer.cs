﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ProfileGlobals;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public static class ProfileXMLSerializer
{
    private static string ep3 = ".ep3";

    public static String PROFILE_PATH
    {
        get
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    NUIManager.WriteError("PersistDataPath:" + Application.persistentDataPath);
                    return Application.persistentDataPath + "/Resources/Profiles";
                default:
                    return "Assets/Resources/Profiles";
            }
        }
        set { }
    }

    public static void serializeProfile(Profile profile)
    {
        if (!System.IO.Directory.Exists(PROFILE_PATH))
        {
            System.IO.Directory.CreateDirectory(PROFILE_PATH);
        }

        String uri = ConvertToFileName(profile.name);
        XmlDocument xdoc = new XmlDocument();
        xdoc.LoadXml(profile.toXML());
        xdoc.Save(uri);
    }

    public static Profile deserializeProfile(string name)
    {
#if UNITY_WEBPLAYER
        return new Profile();
#else

        String uri = ConvertToFileName(name);

        Profile profile = new Profile();
        XmlSerializer ser = new XmlSerializer((typeof(Profile)));
        TextReader reader = new StringReader(System.IO.File.ReadAllText(uri));
        return (Profile)ser.Deserialize(reader);
#endif

    }

    public static List<String> getAllStoredProfiles()
    {
#if UNITY_WEBPLAYER
        return new List<string>();
#else
        List<String> list = new List<String>();
        try
        {
            NUIManager.WriteError("PRF:" + PROFILE_PATH);

            DirectoryInfo dir = new DirectoryInfo(PROFILE_PATH);
            FileInfo[] info = dir.GetFiles("*" + ep3);

            NUIManager.WriteError("Hay " + info.Length + " archivos.");

            foreach (FileInfo f in info)
                list.Add(f.Name.Replace(ep3, ""));
        }
        catch (Exception e)
        {
            NUIManager.WriteError("ERROR:" + e.StackTrace);
        }

        return list;
#endif


    }

    public static void ConvertAllBuiltInProfiles()
    {
        try
        {
            XmlSerializer ser = new XmlSerializer((typeof(Profile)));
            TextReader reader;

            foreach (TextAsset ta in Resources.LoadAll("Profiles"))
            {
                reader = new StringReader(ta.text);
                ProfileXMLSerializer.serializeProfile((Profile)ser.Deserialize(reader));
            }
        }
        catch (Exception e) { UIManager.Instance.AndroidDebug(e.StackTrace); }
    }

    private static string ConvertToFileName(string name)
    {
        return PROFILE_PATH + "/" + name + ep3;
    }

    public static void DeleteFileByName(string name)
    {
        String uri = ConvertToFileName(name);
        System.IO.File.Delete(uri);
    }
}
