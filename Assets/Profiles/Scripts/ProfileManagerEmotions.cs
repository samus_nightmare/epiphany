﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using ProfileGlobals;

public class ProfileManagerEmotions : MonoBehaviour
{
    public ProfileManager profileParent = null;

    public List<Feelings> feelingsUsed = new List<Feelings>();

    public Dictionary<ProfileGlobals.Feelings, int> emotionsTable = new Dictionary<Feelings, int>();

    public UIManager _UIManager = null;

    public class ResolvedEmotion
    {
        public Accuracy Result = Accuracy.MISS;
        public List<DamageEvents> events = new List<DamageEvents>();
    }

    public void ReceiveFeelingsTable(Dictionary<Feelings, int> _newEmotionsTable)
    {
        emotionsTable = _newEmotionsTable;

        GetUsedFeelings();

        InvokeCombination(GetCombination());
    }

    public ResolvedEmotion ResolveEmotion(Dictionary<Feelings, int> _newEmotionsTable, AlchemySystem.Effect effect, int modifier = 0)
    {
        ResolvedEmotion returned = new ResolvedEmotion();

        int random = UnityEngine.Random.Range(-3, 100);

        if (modifier < 0)
            random -= 5 * Mathf.Abs(modifier);
        else if (modifier > 0)
            random += 5 * Mathf.Abs(modifier);

        int RecipiePotency = 0;

        foreach (KeyValuePair<Feelings, int> pair in _newEmotionsTable)
        {
            RecipiePotency += pair.Value;
        }

        int avg = RecipiePotency / _newEmotionsTable.Keys.Count;

        //Se cuenta el número de ingredientes distintos que hay. Por cada número encima de 3 
        //(es decir, si mezclas 4 o más emociones distintas a la vez) se resta un -3.
        //random -= Mathf.Max(0, RecipiePotency - 3) * 3;

        //Para cada ingrediente usado, por cada punto en que supere el mínimo se suma un +1. 
        //Por ejemplo si una receta requiere IRA 3 + ALEGRIA 5, y tu has usado IRA 4 y ALEGRIA 6, se suma un +4 al dado.
        //random += Mathf.Max(0, RecipiePotency - effect.GetPotency());

        //Para cada ingrediente usado, lo comparamos con lo que tiene en la ficha 
        //y le restamos la media. Los resultados positivos se suman al dado.
        //foreach (string fell in profileParent.profile.feelings)
        //{
        //    random += Mathf.Max(0, Convert.ToInt32(fell) - avg);
        //}

        random = Mathf.Clamp(random, 1, 100);

        if (random < 5)
            returned.Result = ProfileGlobals.Accuracy.CATASTROPHIC;
        else if (random < 18)
            returned.Result = ProfileGlobals.Accuracy.FAILURE;
        else if (random < 46)
            returned.Result = ProfileGlobals.Accuracy.MISS;
        else if (random < 80)
            returned.Result = ProfileGlobals.Accuracy.SUCCESS;
        else if (random < 93)
            returned.Result = ProfileGlobals.Accuracy.EXTRAORDINARY;
        else
            returned.Result = ProfileGlobals.Accuracy.PERFECT;

        return returned;
    }

  

    #region deprecated
    
    string temp_Title = "";
    string temp_Target = "";
    string temp_Duration = "";
    string temp_Text = "";

    void GetUsedFeelings()
    {
        feelingsUsed.Clear();

        foreach (var entry in emotionsTable)
        {
            if (entry.Value > 0)
            {
                feelingsUsed.Add(entry.Key);
            }
        }

        feelingsUsed.Sort();

    }

    string GetCombination()
    {
        string combination = "";

        foreach (var entry in feelingsUsed)
        {
            combination += entry + "+";
        }

        combination = combination.Remove(combination.Length - 1);

        if (combination.Length < 1)
        {
            Debug.LogError("Error");
            return null;
        }

        return combination;

    }

    void InvokeCombination(string combination)
    {
        switch (combination)
        {
            #region Invoke 1 element

            case "ANGER": Anger(); break;
            case "ANTICIPATION": Anticipation(); break;
            case "DISGUST": Disgust(); break;
            case "FEAR": Fear(); break;
            case "JOY": Joy(); break;
            case "SADNESS": Sadness(); break;
            case "SURPRISE": Surprise(); break;
            case "TRUST": Trust(); break;

            #endregion

            #region Invoke 2 elements

            case "ANGER+ANTICIPATION": Anger_Anticipation(); break;
            case "ANGER+DISGUST": Anger_Disgust(); break;
            case "ANGER+JOY": Anger_Joy(); break;
            case "ANGER+SADNESS": Anger_Sadness(); break;
            case "ANGER+SURPRISE": Anger_Surprise(); break;
            case "ANGER+TRUST": Anger_Trust(); break;

            case "ANTICIPATION+DISGUST": Anticipation_Disgust(); break;
            case "ANTICIPATION+FEAR": Anticipation_Fear(); break;
            case "ANTICIPATION+JOY": Anticipation_Joy(); break;
            case "ANTICIPATION+SADNESS": Anticipation_Sadness(); break;
            case "ANTICIPATION+TRUST": Anticipation_Trust(); break;

            case "DISGUST+FEAR": Disgust_Fear(); break;
            case "DISGUST+JOY": Disgust_Joy(); break;
            case "DISGUST+SADNESS": Disgust_Sadness(); break;
            case "DISGUST+SURPRISE": Disgust_Surprise(); break;

            case "FEAR+JOY": Fear_Joy(); break;
            case "FEAR+SADNESS": Fear_Sadness(); break;
            case "FEAR+SURPRISE": Fear_Surprise(); break;
            case "FEAR+TRUST": Fear_Trust(); break;

            case "JOY+SURPRISE": Joy_Surprise(); break;
            case "JOY+TRUST": Joy_Trust(); break;

            case "SADNESS+TRUST": Sadness_Trust(); break;

            case "SURPRISE+TRUST": Surprise_Trust(); break;

            #endregion

            #region Invoke 3 elements

            case "ANGER+ANTICIPATION+DISGUST": Anger_Anticipation_Disgust(); break;
            case "ANGER+ANTICIPATION+JOY": Anger_Anticipation_Joy(); break;
            case "ANGER+ANTICIPATION+SADNESS": Anger_Anticipation_Sadness(); break;
            case "ANGER+ANTICIPATION+TRUST": Anger_Anticipation_Trust(); break;
            case "ANGER+DISGUST+JOY": Anger_Disgust_Joy(); break;
            case "ANGER+DISGUST+SADNESS": Anger_Disgust_Sadness(); break;
            case "ANGER+DISGUST+SURPRISE": Anger_Disgust_Surprise(); break;
            case "ANGER+JOY+SURPRISE": Anger_Joy_Surprise(); break;
            case "ANGER+JOY+TRUST": Anger_Joy_Trust(); break;
            case "ANGER+SADNESS+SURPRISE": Anger_Sadness_Surprise(); break;
            case "ANGER+SADNESS+TRUST": Anger_Sadness_Trust(); break;
            case "ANGER+SURPRISE+TRUST": Anger_Surprise_Trust(); break;

            case "ANTICIPATION+DISGUST+FEAR": Anticipation_Disgust_Fear(); break;
            case "ANTICIPATION+DISGUST+JOY": Anticipation_Disgust_Joy(); break;
            case "ANTICIPATION+DISGUST+SADNESS": Anticipation_Disgust_Sadness(); break;
            case "ANTICIPATION+FEAR+JOY": Anticipation_Fear_Joy(); break;
            case "ANTICIPATION+FEAR+SADNESS": Anticipation_Fear_Sadness(); break;
            case "ANTICIPATION+FEAR+TRUST": Anticipation_Fear_Trust(); break;
            case "ANTICIPATION+JOY+TRUST": Anticipation_Joy_Trust(); break;
            case "ANTICIPATION+SADNESS+TRUST": Anticipation_Sadness_Trust(); break;

            case "DISGUST+FEAR+JOY": Disgust_Fear_Joy(); break;
            case "DISGUST+FEAR+SADNESS": Disgust_Fear_Sadness(); break;
            case "DISGUST+FEAR+SURPRISE": Disgust_Fear_Surprise(); break;
            case "DISGUST+JOY+SURPRISE": Disgust_Joy_Surprise(); break;
            case "DISGUST+SADNESS+SURPRISE": Disgust_Sadness_Surprise(); break;

            case "FEAR+JOY+DISGUST": Fear_Joy_Surprise(); break;
            case "FEAR+JOY+TRUST": Fear_Joy_Trust(); break;
            case "FEAR+SADNESS+SURPRISE": Fear_Sadness_Surprise(); break;
            case "FEAR+SADNESS+TRUST": Fear_Sadness_Trust(); break;
            case "FEAR+SURPRISE+TRUST": Fear_Surprise_Trust(); break;

            case "JOY+SURPRISE+TRUST": Joy_Surprise_Trust(); break;

            case "SADNESS+SURPRISE+TRUST": Sadness_Surprise_Trust(); break;

            #endregion

            #region Invoke 4 elements

            case "ANGER+ANTICIPATION+DISGUST+JOY": Anger_Anticipation_Disgust_Joy(); break;
            case "ANGER+ANTICIPATION+DISGUST+SADNESS": Anger_Anticipation_Disgust_Sadness(); break;
            case "ANGER+ANTICIPATION+JOY+TRUST": Anger_Anticipation_Joy_Trust(); break;
            case "ANGER+ANTICIPATION+SADNESS+TRUST": Anger_Anticipation_Sadness_Trust(); break;
            case "ANGER+DISGUST+JOY+SURPRISE": Anger_Disgust_Joy_Surprise(); break;
            case "ANGER+DISGUST+SADNESS+SURPRISE": Anger_Disgust_Sadness_Surprise(); break;
            case "ANGER+JOY+SURPRISE+TRUST": Anger_Joy_Surprise_Trust(); break;
            case "ANGER+SADNESS+SURPRISE+TRUST": Anger_Sadness_Surprise_Trust(); break;

            case "ANTICIPATION+DISGUST+FEAR+JOY": Anticipation_Disgust_Fear_Joy(); break;
            case "ANTICIPATION+DISGUST+FEAR+SADNESS": Anticipation_Disgust_Fear_Sadness(); break;
            case "ANTICIPATION+FEAR+JOY+TRUST": Anticipation_Fear_Joy_Trust(); break;
            case "ANTICIPATION+FEAR+SADNESS+TRUST": Anticipation_Fear_Sadness_Trust(); break;

            case "DISGUST+FEAR+JOY+SURPRISE": Disgust_Fear_Joy_Surprise(); break;
            case "DISGUST+FEAR+SADNESS+SURPRISE": Disgust_Fear_Sadness_Surprise(); break;

            case "FEAR+JOY+SURPRISE+TRUST": Fear_Joy_Surprise_Trust(); break;
            case "FEAR+SADNESS+SURPRISE+TRUST": Fear_Sadness_Surprise_Trust(); break;

            #endregion
        }

        if (_UIManager != null)
        {
            ResultToScreen();

            _UIManager.SendMessage("ChangeWindow", 5);
        }
        else
        {
            Debug.LogError("Error: UIManager no encontrado");
        }

    }

    void ResultToScreen()
    {
        temp_Text = LineAdjust(temp_Text);

        temp_Duration = LineAdjust(temp_Duration);

        temp_Target = LineAdjust(temp_Target);

        Log.LogTextReplace(temp_Title + "\n\nTarget:" + temp_Target + "\nDuration:" + temp_Duration + "\nEffect:" + temp_Text);
    }

    string LineAdjust(string textString)
    {
        int charactersLimit = 32;

        int i = 0;
        int whitespaceCount = 0;
        while (i < textString.Length)
        {
            if (i % charactersLimit == 0)
            {
                whitespaceCount = 0;
                while (i < textString.Length && textString[i] != ' ' && whitespaceCount < 12)
                {
                    i++;
                    whitespaceCount++;
                }
                textString = textString.Insert(i, "\n");
            }

            i++;
        }

        return textString;
    }

    #region Emotions Combination - 1 Element (8)

    void Anger()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English":
                temp_Title = "Rage";
                temp_Target = "Up to " + feelingsUsed.Count + " people in contact.";
                temp_Duration = "Up to " + feelingsUsed.Count + " hours ";
                temp_Text = "+" + (Mathf.FloorToInt(feelingsUsed.Count / 3)) + " VIGOR situations. Transitory frenzy state where they don't attend to arguments, can't talk, they are like crazy animals and will attack any perceived threat at sight.";
                break;

            case "Spanish":
                temp_Title = "Ira";
                temp_Target = "Hasta " + feelingsUsed.Count + " personas en contacto.";
                temp_Duration = "Hasta " + feelingsUsed.Count + " horas.";
                temp_Text = "+" + (Mathf.FloorToInt(feelingsUsed.Count / 3)) + " VIGOR. Enajenación transitoria, los objetivos no atienden a razones, pierden el habla, son como animales rabiosos y atacarán cualquier amenaza que perciban. Una persona puede negarse a ser afectada por este efecto si supera una situacion de CARACTER con - " + Mathf.FloorToInt(feelingsUsed.Count / 3);
                break;
        }
    }

    void Anticipation()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English":
                temp_Title = "Vigilance";
                temp_Target = "1 person in contact.";
                temp_Duration = "Up to " + feelingsUsed.Count + " hours ";
                temp_Text = "The person sharpen his senses sharply, gaining +" + (Mathf.FloorToInt(feelingsUsed.Count / 3)) + " INSIGHT bonus anticipating situations where sight and hearing matter.";
                break;

            case "Spanish":
                temp_Title = "Vigilancia";
                temp_Target = "1 persona en contacto.";
                temp_Duration = "Hasta " + feelingsUsed.Count + " horas.";
                temp_Text = "El afectado agudiza sus sentidos de forma extrema, ganando +" + (Mathf.FloorToInt(feelingsUsed.Count / 3)) + " PERSPICACIA para anticiparse a situaciones donde influyan la vista y el oído. Una persona puede negarse a ser afectada por este efecto.";
                break;
        }
    }

    void Disgust()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English"://todo
                temp_Title = "";
                temp_Target = "";
                temp_Duration = "";
                temp_Text = "";
                break;

            case "Spanish":
                temp_Title = "Aborrecimiento";
                temp_Target = "Una persona u objeto en contacto " + "(" + Mathf.RoundToInt(feelingsUsed.Count * 100) + ") Kg";
                temp_Duration = "Hasta " + Mathf.RoundToInt(feelingsUsed.Count * 2) + " horas.";
                temp_Text = "La persona afectada debe superar una tirada de PERSONALIDAD con -" + Mathf.RoundToInt(feelingsUsed.Count / 3) + " o será <<maldita>>, siendo desde ese momento objeto de hostilidad y agresividad por parte de las personas a su alrededor. Cualquier persona que interactúe con el objeto o individuo <<maldito>>, debe superar una tirada de INTELIGENCIA con un modificador que el Director de Juego considere. ";
                break;
        }
    }

    void Fear()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English"://todo
                temp_Title = "";
                temp_Target = "";
                temp_Duration = "";
                temp_Text = "";
                break;

            case "Spanish":
                temp_Title = "Terror";
                temp_Target = "1 persona u objeto en contacto " + "(" + Mathf.RoundToInt(feelingsUsed.Count * 100) + ") Kg";
                temp_Duration = "Hasta " + Mathf.RoundToInt(feelingsUsed.Count / 2) + " horas. ";
                temp_Text = "Provoca temor. El afectado debe superar una tirada de PERSONALIDAD con -" + Mathf.RoundToInt(feelingsUsed.Count / 3) + " o será afectado por el Terror. Si cae en el pánico, el autor del efecto produce un efecto intimidante muy poderoso y los mecanismos de huida y peligro se activan en la mente del afectado. Su único deseo es huir de una amenaza primitiva y peligrosa. No razona. Si se aplica sobre objetos y lugares, imprime a estos de una sensación lúgubre y tenebrosa. Por alguna razón produce una sensación inquietante estar cerca. Las paredes de los lugares se resquebrajan (no causa daño real) y el aspecto del objeto se vuelve sucio, oxidado, maltratado y ruinoso. Toda persona excepto el autor del efecto deben intentar superar una tirada de PERSONALIDAD con -" + Mathf.RoundToInt(feelingsUsed.Count / 3) + "para poder tocar o entrar en el objeto afectado, o serán totalmente reacios a interactuar con este objeto.";
                break;
        }
    }

    void Joy()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English"://todo
                temp_Title = "";
                temp_Target = "";
                temp_Duration = "";
                temp_Text = "";
                break;

            case "Spanish":
                temp_Title = "Extasis";
                temp_Target = "Hasta " + Mathf.RoundToInt(feelingsUsed.Count / 2) + " personas u objetos en contacto " + "(" + Mathf.RoundToInt(feelingsUsed.Count * 100) + ") Kg";
                temp_Duration = "Hasta " + feelingsUsed.Count + " horas.";
                temp_Text = "Los afectados se vuelven crédulos y joviales, neutralizando temporalmente toda intención hostil, agresividad, depresión o tristeza. Todos los efectos que lleven estos ingredientes se cancelan automáticamente. Los objetos se vuelven más alegres y renacen, florecen y cambian estéticamente para volverse muy alegres. Se colorean y transmiten alegría y aparecen radiantes. Toda persona u objeto afectado recupera Salud Física (o se auto-reparan equivalentemente) " + Mathf.RoundToInt(feelingsUsed.Count / 3) + " punto(s) de Salud. Una persona u objeto, al criterio del Director, puede negarse a someterse a este efecto.";
                break;
        }

    }

    void Sadness()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English"://todo
                temp_Title = "";
                temp_Target = "";
                temp_Duration = "";
                temp_Text = "";
                break;

            case "Spanish":
                temp_Title = "Aflicción";
                temp_Target = "Hasta" + Mathf.RoundToInt(feelingsUsed.Count) + " personas en contacto.";
                temp_Duration = "Hasta " + feelingsUsed.Count + " horas.";
                temp_Text = "Destruye la voluntad del individuo afectado (que obtiene un penalizador de -" + Mathf.RoundToInt(feelingsUsed.Count / 3) + " a cualquier situación que tenga que resolver con su PERSPICACIA). Todo intento de reanimarle se encuentra con que la persona está hundida en su propio delirio desesperado, inventa excusas e hila argumentos incoherentes y no atiende a razón. Para negar este efecto, la persona debe superar una situacion de CARACTER con -" + Mathf.RoundToInt(feelingsUsed.Count / 3);
                break;
        }
    }

    void Surprise()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English"://todo
                temp_Title = "";
                temp_Target = "";
                temp_Duration = "";
                temp_Text = "";
                break;

            case "Spanish":
                temp_Title = "Estupefaccion";
                temp_Target = "El usuario";
                temp_Duration = "Hasta " + feelingsUsed.Count * 5 + " minutos.";
                temp_Text = "El usuario, cada vez que toca con sus manos o con objetos en contacto con su cuerpo a cualquier otra persona, le produce un shock que le causa un estado de parálisis innatural. La persona pierde el control sobre sus capacidades motoras. La persona tiene un modificador negativo de -" + Mathf.CeilToInt(feelingsUsed.Count / 2) + " a VIGOR.";
                if (feelingsUsed.Count > 3)
                    temp_Text += "\nAdemás, la víctima recibe una descarga dolorosa que causa daños en su cuerpo y cristaliza sus músculos. Una corriente eléctrica puede visualizarse recorriendo el cuerpo del individuo, y disminuye" + Mathf.CeilToInt(feelingsUsed.Count / 4) + "punto(s) de Salud Física";
                break;
        }
    }

    void Trust()
    {
        switch (LocalizationSystem.currentLanguage)
        {
            case "English"://todo
                temp_Title = "";
                temp_Target = "";
                temp_Duration = "";
                temp_Text = "";
                break;

            case "Spanish":
                temp_Title = "Aflicción";
                temp_Target = "1 persona en contacto";
                temp_Duration = "Hasta " + feelingsUsed.Count * 3 + " horas.";
                temp_Text = "Despierta un sentimiento de confianza en los afectados hacia el autor del efecto y se vuelven más crédulos. A la hora de intentar hacerles creer algo, el autor tiene un modificador de +" + Mathf.CeilToInt(feelingsUsed.Count / 3) + "CARACTER";
                break;
        }
    }

    #endregion

    #region Emotions Combination - 2 Elements (24)

    void Anger_Anticipation()
    {

    }

    void Anger_Disgust()
    {

    }

    void Anger_Joy()
    {

    }

    void Anger_Sadness()
    {

    }

    void Anger_Surprise()
    {

    }

    void Anger_Trust()
    {

    }

    void Anticipation_Disgust()
    {

    }

    void Anticipation_Fear()
    {

    }

    void Anticipation_Joy()
    {

    }

    void Anticipation_Sadness()
    {

    }

    void Anticipation_Trust()
    {

    }

    void Disgust_Fear()
    {

    }

    void Disgust_Joy()
    {

    }

    void Disgust_Sadness()
    {

    }

    void Disgust_Surprise()
    {

    }

    void Fear_Joy()
    {

    }

    void Fear_Sadness()
    {

    }

    void Fear_Surprise()
    {

    }

    void Fear_Trust()
    {

    }

    void Joy_Surprise()
    {

    }

    void Joy_Trust()
    {

    }

    void Sadness_Surprise()
    {

    }

    void Sadness_Trust()
    {

    }

    void Surprise_Trust()
    {

    }

    #endregion

    #region Emotions Combination - 3 Elements (32)

    #region Anger (12)

    void Anger_Anticipation_Disgust()
    {

    }

    void Anger_Anticipation_Joy()
    {

    }

    void Anger_Anticipation_Sadness()
    {

    }

    void Anger_Anticipation_Trust()
    {

    }

    void Anger_Disgust_Joy()
    {

    }

    void Anger_Disgust_Sadness()
    {

    }

    void Anger_Disgust_Surprise()
    {

    }

    void Anger_Joy_Surprise()
    {

    }

    void Anger_Joy_Trust()
    {

    }

    void Anger_Sadness_Surprise()
    {

    }

    void Anger_Sadness_Trust()
    {

    }

    void Anger_Surprise_Trust()
    {

    }

    #endregion

    #region Anticipation (8)

    void Anticipation_Disgust_Fear()
    {

    }

    void Anticipation_Disgust_Joy()
    {

    }

    void Anticipation_Disgust_Sadness()
    {

    }

    void Anticipation_Fear_Joy()
    {

    }

    void Anticipation_Fear_Sadness()
    {

    }

    void Anticipation_Fear_Trust()
    {

    }

    void Anticipation_Joy_Trust()
    {

    }

    void Anticipation_Sadness_Trust()
    {

    }

    #endregion

    #region Disgust (5)

    void Disgust_Fear_Joy()
    {

    }

    void Disgust_Fear_Sadness()
    {

    }

    void Disgust_Fear_Surprise()
    {

    }

    void Disgust_Joy_Surprise()
    {

    }

    void Disgust_Sadness_Surprise()
    {

    }

    #endregion

    #region Fear (5)

    void Fear_Joy_Surprise()
    {

    }

    void Fear_Joy_Trust()
    {

    }

    void Fear_Sadness_Surprise()
    {

    }

    void Fear_Sadness_Trust()
    {

    }

    void Fear_Surprise_Trust()
    {

    }

    #endregion

    void Joy_Surprise_Trust()
    {

    }

    void Sadness_Surprise_Trust()
    {

    }

    #endregion

    #region Emotions Combination - 4 Elements (16)

    void Anger_Anticipation_Disgust_Joy()
    {

    }

    void Anger_Anticipation_Disgust_Sadness()
    {

    }

    void Anger_Anticipation_Joy_Trust()
    {

    }

    void Anger_Anticipation_Sadness_Trust()
    {

    }

    void Anger_Disgust_Joy_Surprise()
    {

    }

    void Anger_Disgust_Sadness_Surprise()
    {

    }

    void Anger_Joy_Surprise_Trust()
    {

    }

    void Anger_Sadness_Surprise_Trust()
    {

    }

    void Anticipation_Disgust_Fear_Joy()
    {

    }

    void Anticipation_Disgust_Fear_Sadness()
    {

    }

    void Anticipation_Fear_Joy_Trust()
    {

    }

    void Anticipation_Fear_Sadness_Trust()
    {

    }

    void Disgust_Fear_Joy_Surprise()
    {

    }

    void Disgust_Fear_Sadness_Surprise()
    {

    }

    void Fear_Joy_Surprise_Trust()
    {

    }

    void Fear_Sadness_Surprise_Trust()
    {

    }

    #endregion
    
    #endregion
}
