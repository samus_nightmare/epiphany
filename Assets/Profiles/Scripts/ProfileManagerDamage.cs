﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProfileGlobals;
using System;

public class ProfileManagerDamage : MonoBehaviour
{
    public ProfileManager profileParent = null;

    public class ResolveDamage
    {
        public ProfileGlobals.HealthLevels originDamage { get; set; }
        public ProfileGlobals.HealthLevels updatedDamage { get; set; }
        public List<DamageEvents> events = new List<DamageEvents>();
    }

    ResolveDamage CalculateDamage(ProfileGlobals.DamageTypes damageType, ProfileGlobals.ImpactLocalization impactLocalization = ImpactLocalization.INDETERMINATE, int modifier = 0)
    {
        ResolveDamage returned = new ResolveDamage();

        returned.originDamage = profileParent.profile.health;
        returned.updatedDamage = profileParent.profile.health;


        /*
                   1. Calcular la Gravedad
                   GRAVEDAD = 5 - VIGOR
                   Por cada -1 en el modificador, restamos +0,5 a GRAVEDAD.
                   Por cada +1 en el modificador, sumamos -0,5 a GRAVEDAD

                   De esta forma, GRAVEDAD estará siempre entre -10 (daño muy superficial) y 10 (daño extremadamente letal)
                */
        float modChange = -modifier * 0.2f;
        if (modifier > 0)
        {
            modChange = -modifier * 0.2f;
        }

        float GRAVEDAD = 2 - Convert.ToInt32(profileParent.profile.getAtribute(Attributes.VIGOR)) * (UnityEngine.Random.Range(0.1f, 4)) + modChange;

        /*
            2. Aplicar la naturaleza
            Según la naturaleza del daño, la GRAVEDAD puede verse modificada por varios factores extra.
            CORTE: Restamos (-0.25 * DESTREZA) a la fórmula, de modo que cuanto más DESTREZA se tiene más fácil es evitar este daño.

            PERFORACIÓN: Restamos (-0.25 * DESTREZA) a la fórmula, de modo que cuanto más DESTREZA se tiene más fácil es evitar este daño.

            IMPACTO: Restamos (-0.25 * VIGOR) a la fórmula, de modo que cuanto más VIGOR se tiene más fácil es evitar este daño.

            ABRASIÓN: Por cada +1 en el modificador, le restamos -0,25. Por cada -1 en el modificador le aumentamos +0,25. 
            Esto hace que las heridas por abrasión tiendan a ser o muy superficiales, o muy graves.
         */

        switch (damageType)
        {
            case DamageTypes.ABRASION: //ABRASION
                GRAVEDAD += GRAVEDAD - modifier * 0.25f;
                break;
            case DamageTypes.IMPACT: //IMPACT
                GRAVEDAD += GRAVEDAD - Convert.ToInt32(profileParent.profile.getAtribute(Attributes.VIGOR)) * 0.25f;
                break;
            case DamageTypes.INCISION: //CORTE
                GRAVEDAD += GRAVEDAD - Convert.ToInt32(profileParent.profile.getAtribute(Attributes.VIGOR)) * 0.25f;
                break;
            case DamageTypes.PIERCE: //PERFORACIÓN
                GRAVEDAD += GRAVEDAD - Convert.ToInt32(profileParent.profile.getAtribute(Attributes.SKILL)) * 0.25f;
                break;
        }

        /*
             4. Aplicar la localización
            Ya tenemos la GRAVEDAD del daño sufrido. Ahora tenemos que sacar el resultado que mostraremos según la localización del daño especificada.
         */


        switch (impactLocalization)
        {
            case ImpactLocalization.LIMB:
                if (GRAVEDAD < 0)
                    returned.events.Add(DamageEvents.AVOID_DAMAGE);
                else if (GRAVEDAD >= 0 && GRAVEDAD < 2)
                    returned.updatedDamage = Operation(returned.originDamage, 1);
                else if (GRAVEDAD >= 2 && GRAVEDAD < 5)
                {
                    returned.updatedDamage = Operation(returned.originDamage, 1);
                    if (damageType == DamageTypes.PIERCE && UnityEngine.Random.Range(0, 10) > 8)
                        returned.events.Add(DamageEvents.LOST_THE_LIMB);
                }
                else if (GRAVEDAD >= 5 && GRAVEDAD < 8)
                {
                    returned.updatedDamage = Operation(returned.originDamage, 1);
                    if (damageType != DamageTypes.ABRASION && UnityEngine.Random.Range(0, 10) > 8)
                        returned.events.Add(DamageEvents.MINUS_1_MODIFIER);
                }
                else if (GRAVEDAD >= 8)
                {
                    returned.updatedDamage = Operation(returned.originDamage, 2);
                    returned.events.Add(DamageEvents.MINUS_1_MODIFIER);
                    if (damageType != DamageTypes.ABRASION && UnityEngine.Random.Range(0, 10) > 8)
                        returned.events.Add(DamageEvents.LOST_THE_LIMB);
                }
                break;



            case ImpactLocalization.INDETERMINATE:
                if (GRAVEDAD < 0)
                    returned.events.Add(DamageEvents.AVOID_DAMAGE);
                else if (GRAVEDAD >= 0 && GRAVEDAD < 2)
                    returned.updatedDamage = Operation(returned.originDamage, 1);
                else if (GRAVEDAD >= 2 && GRAVEDAD < 4)
                {
                    returned.updatedDamage = Operation(returned.originDamage, 1);
                    if (UnityEngine.Random.Range(0, 10) > 8)
                        returned.events.Add(DamageEvents.MINUS_1_MODIFIER);
                }
                else if (GRAVEDAD >= 4 && GRAVEDAD < 8)
                {
                    returned.updatedDamage = Operation(returned.originDamage, 2);
                    if (UnityEngine.Random.Range(0, 10) > 8)
                        returned.events.Add(DamageEvents.MINUS_2_MODIFIER);
                }
                else if (GRAVEDAD >= 8)
                {
                    returned.updatedDamage = Operation(returned.originDamage, 2);
                    if (UnityEngine.Random.Range(0, 10) > 8)
                        returned.events.Add(DamageEvents.MINUS_4_MODIFIER);
                }
                break;



            case ImpactLocalization.SUPERFICIAL:
                if (GRAVEDAD < 0)
                    returned.events.Add(DamageEvents.AVOID_DAMAGE);
                else if (GRAVEDAD >= 0 && GRAVEDAD < 2 && UnityEngine.Random.Range(0, 10) > 8)
                    returned.events.Add(DamageEvents.MINUS_1_MODIFIER);
                else if (GRAVEDAD >= 3 && UnityEngine.Random.Range(0, 10) > 8)
                    returned.events.Add(DamageEvents.MINUS_2_MODIFIER);
                break;



            case ImpactLocalization.VITAL:
                if (GRAVEDAD < 0)
                    returned.events.Add(DamageEvents.AVOID_DAMAGE);
                else if (GRAVEDAD >= 0 && GRAVEDAD < 2)
                    returned.updatedDamage = Operation(returned.originDamage, 1);
                else if (GRAVEDAD >= 2 && GRAVEDAD < 4)
                    returned.updatedDamage = Operation(returned.originDamage, 2);
                else if (GRAVEDAD >= 4)
                    returned.updatedDamage = Operation(returned.originDamage, 3);
                break;
        }

        profileParent.profile.health = returned.updatedDamage;

        return returned;
    }



    public void OkButton(ProfileGlobals.DamageTypes damageType, ProfileGlobals.ImpactLocalization impactLocalization, int modifier = 0)
    {
        //Debug.Log("Hemos hecho daño! Resultado = " + CalculateDamage(damageType, impactLocalization, modifier));

        ResolveDamage result = CalculateDamage(damageType, impactLocalization, modifier);

        string resultado = "";


        if (result.originDamage == result.updatedDamage)
        {
            resultado += LocalizationSystem.Instance.translation.dm_event_nodamage + "\n";
            result.events.Clear();
        }
        else
        {
            resultado += profileParent.profile.name + "\n" + LocalizationSystem.Instance.translation.dm_event_statusitsin + " " + LocalizationSystem.HealthName(result.updatedDamage) + "\n";
        }

        foreach (var nEvent in result.events)
        {
            resultado += TextOfEvents(nEvent) + ".\n";
        }

        Log.LogTextReplace(resultado);

    }

    public HealthLevels Operation(HealthLevels hl, int num)
    {
        int newResult = Mathf.Clamp((int)hl + num, (int)HealthLevels.FINE, (int)HealthLevels.DEAD);

        return (HealthLevels)newResult;

        //return (HealthLevels)Mathf.Clamp((int)hl + num, (int)HealthLevels.FINE, (int)HealthLevels.DEAD);
    }

    public string TextOfEvents(DamageEvents input)
    {
        string output = "";

        switch (input)
        {
            case DamageEvents.AVOID_DAMAGE:
                output = LocalizationSystem.Instance.translation.dm_event_nodamage;
                break;
            case DamageEvents.LOST_THE_LIMB:
                output = LocalizationSystem.Instance.translation.dm_event_lostlimb;
                break;
            case DamageEvents.MINUS_1_MODIFIER:
                output = LocalizationSystem.Instance.translation.dm_event_modifierapply + " " + "-1";
                break;
            case DamageEvents.MINUS_2_MODIFIER:
                output = LocalizationSystem.Instance.translation.dm_event_modifierapply + " " + "-2";
                break;
            case DamageEvents.MINUS_3_MODIFIER:
                output = LocalizationSystem.Instance.translation.dm_event_modifierapply + " " + "-3";
                break;
            case DamageEvents.MINUS_4_MODIFIER:
                output = LocalizationSystem.Instance.translation.dm_event_modifierapply + " " + "-4";
                break;
        }

        return output;
    }
}