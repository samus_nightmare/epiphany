﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProfileGlobals;
using System;

public class ProfileManagerSituations : MonoBehaviour
{
    public ProfileManager profileParent = null;
    /*Primero, se hace una media de las tiradas para decidir el FACTORPJ.
        Por ejemplo si un personaje tira Vigor 5 y Perspicacia 2, su FACTORPJ será 3,5.

        Después, añadimos los modificadores. 

        Cada +1 añade 0,25 al FACTORPJ.
        Cada -1 resta 0,5 al FACTORPJ.

        El máximo FACTORPJ alcanzable jamás es 8. El mínimo FACTORPJ alcanzable es -5.

        Se hace una tirada de 1 a 100. A esta tirada, le sumamos FACTORPJ*8.

        Tirada = CLAMP( Random(1,100) + (FACTORPJ*8),1,100)

        Y después se comprueba en la tabla para saber el grado de éxito obtenido:

    */
    private float[] results = { 5, 15, 40, 80, 95, 100 };

    public Accuracy ResolveSituation(int mod, List<Attributes> activeAttributes)
    {
        if (activeAttributes.Count < 1)
        {
            Debug.LogError("Error: ResolveSituation has been called without Attributes used!");
            return Accuracy.CATASTROPHIC;
        }

        float FACTORPJ = 0;

        foreach (Attributes attr in activeAttributes)
        {
            FACTORPJ += (float)profileParent.profile.getAttributeLevel(attr); //int.Parse(profileParent.profile.getAtribute(attr));
        }
        FACTORPJ /= activeAttributes.Count;

        //Penalizador por nivel de salud
        switch (profileParent.profile.health)
        {
            case HealthLevels.BRUISED:
                if (activeAttributes.Contains(Attributes.VIGOR) || activeAttributes.Contains(Attributes.SKILL))
                {
                    FACTORPJ *= 0.9f;
                }
                break;

            case HealthLevels.HURT:
                if (activeAttributes.Contains(Attributes.VIGOR) || activeAttributes.Contains(Attributes.SKILL))
                {
                    FACTORPJ *= 0.8f;
                }
                break;

            case HealthLevels.INJURED:
                FACTORPJ *= 0.7f;
                break;

            case HealthLevels.MORIBUND:
                FACTORPJ *= 0.6f;
                break;
        }


        if (mod > 0)
            FACTORPJ += mod * 0.25f;
        else if (mod < 0)
            FACTORPJ += mod * 1.5f;

        FACTORPJ = Mathf.Clamp(FACTORPJ, -5, 8);
        int Tirada = Mathf.Clamp((int)(UnityEngine.Random.Range(0, 75) + (FACTORPJ * 8)), 1, 100);

        if (Tirada <= 5)
            return Accuracy.CATASTROPHIC;
        else if (Tirada > 5 && Tirada <= 15)
            return Accuracy.FAILURE;
        else if (Tirada > 15 && Tirada <= 40)
            return Accuracy.MISS;
        else if (Tirada > 40 && Tirada <= 80)
            return Accuracy.SUCCESS;
        else if (Tirada > 80 && Tirada <= 95)
            return Accuracy.EXTRAORDINARY;
        else
            return Accuracy.PERFECT;
    }


    public void OkButton(int modifier = 0)
    {

        //Log.LogTextReplace(profileParent.profile.name + " " + LocalizationSystem.Instance.translation.sit_resolve + ": \n" + ResolveSituation(modifier));

    }
}
