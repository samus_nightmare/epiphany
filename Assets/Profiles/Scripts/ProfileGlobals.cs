﻿using UnityEngine;
using System;
using System.Collections;

namespace ProfileGlobals
{

    static class utils
    {
        public static Feelings getOposite(Feelings attr)
        {
            switch (attr)
            {
                case Feelings.ANGER: return Feelings.FEAR;
                case Feelings.FEAR: return Feelings.ANGER;
                case Feelings.JOY: return Feelings.SADNESS;
                case Feelings.SADNESS: return Feelings.JOY;
                case Feelings.DISGUST: return Feelings.TRUST;
                case Feelings.TRUST: return Feelings.DISGUST;
                case Feelings.ANTICIPATION: return Feelings.SURPRISE;
                case Feelings.SURPRISE: return Feelings.ANTICIPATION;

                default: return Feelings.ANGER;
            }
        }
    }

    public struct e3Values
    {
        public const int MinModifier = -10;
        public const int MaxModifier = 10;
        public const int MinCharacterModifier = -4;
        public const int MaxCharacterModifier = 5;
    }

    public class experienceCosts
    {
        public static float[] PositiveModifiers = { 3, 5, 7, 9, 13 };
        public static float[] NegativeModifiers = { -3, -5, -7, -9 };
        public static float[] basicEmotions = { 5, 2, 3, 5, 8, 12, 17, 23, 30, 38 };
        public static float[] attributesUpgrading = { 1, 2, 5, 12, 30 };

    }


    public enum ProfileMetaTypes
    {
        SYSTEM = 0,
        USER,
        length
    };

    public enum Tribes
    {
        CIENTIFICOS = 0,
        GARRULOS,
        PEGAPALOS,
        CIBORGS,
        length
    };

    public enum Attributes
    {
        VIGOR = 0,
        INSIGHT,
        SKILL,
        PRESENCE,
        length
    };

    public enum AttributesLevel
    {
        SUBHUMAN = 0,
        POOR,
        AVERAGE,
        COMPETENT,
        EXCELLENT,
        PRODIGIOUS,
        length
    };

    public enum Accuracy
    {
        PERFECT = 0,
        EXTRAORDINARY,
        SUCCESS,
        MISS,
        FAILURE,
        CATASTROPHIC,
        length
    };

    public enum HealthLevels
    {
        FINE = 0,
        BRUISED,
        HURT,
        INJURED,
        MORIBUND,
        DEAD,
        length
    }

    public enum Feelings
    {
        ANGER = 0,
        ANTICIPATION,
        DISGUST,
        FEAR,
        JOY,
        SADNESS,
        SURPRISE,
        TRUST,
        length
    };

    public enum DamageTypes
    {
        ABRASION = 0,
        PIERCE,
        INCISION,
        IMPACT,
        lenght
    };

    public enum ImpactLocalization
    {
        LIMB = 0,
        VITAL,
        INDETERMINATE,
        SUPERFICIAL,
        lenght
    };

    public enum DamageEvents
    {
        LOST_THE_LIMB = 0,
        MINUS_1_MODIFIER,
        MINUS_2_MODIFIER,
        MINUS_3_MODIFIER,
        MINUS_4_MODIFIER,
        AVOID_DAMAGE,
        lenght
    }
}
