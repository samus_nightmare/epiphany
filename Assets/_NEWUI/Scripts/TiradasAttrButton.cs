﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TiradasAttrButton : MonoBehaviour
{
    public Image Check;
    public Text Text;
    public Image Base;
    public Color ColorNormal, ColorSelected;


    public ProfileGlobals.Attributes Attribute = ProfileGlobals.Attributes.VIGOR;


    [HideInInspector]
    public bool Selected = false;



    public void Select(bool s)
    {
        if (Selected != s)
            Select();
    }

    public void Select()
    {
        Selected = !Selected;

        if (Selected)
        {
            Base.color = ColorSelected;
        }
        else
        {
            Base.color = ColorNormal;
        }

        Check.gameObject.SetActive(Selected);
    }
}
