﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gradient : MonoBehaviour
{
    public Color from, to;
    public float speed = 0.5f;
    public bool OnlyOnce;

    [HideInInspector]
    public int cycles = 0;

    Image currentImage = null;
    Text currentText = null;

    float mLerp = 0;
    int sentido = 1;

    void OnEnable()
    {
        cycles = 0;
        currentImage = GetComponent<Image>();
        currentText = GetComponent<Text>();
        mLerp = 0;
        SetColor(from);
        sentido = 1;
    }

    void Update()
    {
        mLerp += sentido * Time.deltaTime * speed;
        SetColor(Color.Lerp(from, to, mLerp));

        if (mLerp >= 1)
        {
            sentido = -1;
            if (OnlyOnce) enabled = false;
        }
        else if (mLerp <= 0)
        {
            sentido = 1;
            cycles++;
            if (OnlyOnce) enabled = false;
        }
    }

    void SetColor(Color color)
    {
        if (currentImage != null)
            currentImage.color = color;

        if (currentText != null)
            currentText.color = color;
    }
}
