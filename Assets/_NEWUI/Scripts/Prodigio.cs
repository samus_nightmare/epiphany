﻿using UnityEngine;
using System.Collections;

public class Prodigio : MonoBehaviour
{
    public string ID = "";
    public string Name = "empty";
    public Sprite Icon;
    public int Level = 1;

    [Multiline]
    public string Description = "";

    [Multiline]
    public string AdditionalOptions = "";

    void OnValidate()
    {
        gameObject.name = Name;
    }

}
