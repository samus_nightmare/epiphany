﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProdigiosDescriptorSection : MonoBehaviour
{
    [HideInInspector]
    Prodigio readingProdigio = null;

    public ModifiersBar ModifiersBar;

    public Text Title;
    public Image Icon;

    public Text Description;
    public Text AdditionalOptions;

    public Color ResultsColor = Color.white;

    public void Open(Prodigio prd)
    {
        readingProdigio = prd;
        gameObject.SetActive(true);
        NUIManager.Instance.NMain.Prodigios.CloseSubSections(gameObject);
        Load();
        ModifiersBar.Modifier = 0;
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Load()
    {
        Title.text = readingProdigio.Name;

        Icon.sprite = readingProdigio.Icon;

        //todo: las opciones adicionales están cutremente implementadas como texto. Lo guay sería que fueran toggles o algo así.
        Description.text = readingProdigio.Description;
        AdditionalOptions.text = readingProdigio.AdditionalOptions;
    }

    public void Back()
    {
        NUIManager.Instance.NMain.Prodigios.Selector.Open();
    }

    public void LaunchDice(int modifier)
    {
        if (NUIManager.Instance.ProfileManager.profile.catharsis == 0)
        {
            SectionLog.ShowPower("La Catarsis de " + NUIManager.TruncateString(NUIManager.Instance.ProfileManager.profile.name, 12) + " es cero. No puede ejecutar Prodigios.");
            return;
        }

        string _msg = NUIManager.TruncateString(NUIManager.Instance.ProfileManager.profile.name, 12);
        _msg += " ejecuta " + NUIManager.TruncateString(readingProdigio.Name, 21) + " (" + PEditModifiers.WrapLevelWithColor(modifier) + ")";
        _msg += "\n";


        //El modificador por nivel
        int _modNivel = Mathf.FloorToInt(NUIManager.Instance.ProfileManager.profile.experience / 50f);

        modifier += _modNivel;

        //El nivel del personaje 
        int _dado = UnityEngine.Random.Range(0, 100);
        _dado = Mathf.Clamp(Mathf.CeilToInt(_dado + (4.5f * modifier)), 0, 100);

        bool _pierdeCatarsis = false;

        //resultado entre 0 y 100
        ProfileGlobals.Accuracy _resultado = ProfileGlobals.Accuracy.MISS;
        if (_dado < 5)
        {
            _resultado = ProfileGlobals.Accuracy.CATASTROPHIC;
            _pierdeCatarsis = PierdeCatarsis(readingProdigio.Level * 2);
        }
        else if (_dado < 17)
        {
            _resultado = ProfileGlobals.Accuracy.FAILURE;
            _pierdeCatarsis = PierdeCatarsis(readingProdigio.Level * 1.5f);
        }
        else if (_dado < 50)
        {
            _resultado = ProfileGlobals.Accuracy.MISS;
            _pierdeCatarsis = PierdeCatarsis(readingProdigio.Level);
        }
        else if (_dado < 83)
        {
            _resultado = ProfileGlobals.Accuracy.SUCCESS;
        }
        else if (_dado < 95)
        {
            _resultado = ProfileGlobals.Accuracy.EXTRAORDINARY;
        }
        else
        {
            _resultado = ProfileGlobals.Accuracy.PERFECT;
        }

        _msg += "Resultado: " + NLocalization.LocalizeAccuracy(_resultado);

        if (_pierdeCatarsis)
        {
            NUIManager.Instance.ProfileManager.profile.catharsis = Mathf.Clamp(NUIManager.Instance.ProfileManager.profile.catharsis + 1, 0, 10);
            _msg += "\nNivel de catarsis aumenta a " + NUIManager.Instance.ProfileManager.profile.catharsis;
        }

        SectionLog.ShowPower(_msg);

        NUIManager.Instance.NMain.CurrentProfile.Refresh();
        NUIManager.Instance.ProfileManager.writeProfile();
    }


    bool PierdeCatarsis(float factor)
    {
        return true; //Por ahora siempre se pierde catarsis al pifiar


        // int _dado = UnityEngine.Random.Range(1, 10);
        //return (_dado <= factor);
    }

}
