﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SectionTiradas : MonoBehaviour
{

    public TiradasAttrButton VIG, DEX, INT, PRE;

    public ModifiersBar ModifiersBar;

    public TiradasHelpTable TiradasHelpTable;


    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.NMain.CloseOtherSections(gameObject);
        NUIManager.Instance.NMain.lastVisitedSection = NMain.Sections.Tiradas;
        Clear();
    }

    public void Close()
    {
        gameObject.SetActive(false);
        TiradasHelpTable.Close();
    }

    public void Clear()
    {
        VIG.Select(false);
        DEX.Select(false);
        INT.Select(false);
        PRE.Select(false);
        ModifiersBar.Modifier = 0;
    }


    public void HelpTable()
    {
        TiradasHelpTable.Open();
    }

    public void LaunchDice(int modifier)
    {
        List<ProfileGlobals.Attributes> _usedAtt = GetUsedAttributes();
        if (modifier > 10 || modifier < -10 || _usedAtt.Count < 1)
        {
            return;
        }

        string _msg = NUIManager.TruncateString(NUIManager.Instance.ProfileManager.profile.name, 12);

        _msg += ":";
        _msg += string.Join("+", _usedAtt.ConvertAll(x => NLocalization.LocalizeAttributeShort(x)).ToArray());
        _msg += "(" + (modifier > 0 ? "+" + modifier.ToString() : modifier.ToString()) + ")";
        _msg += "\n";

        ProfileGlobals.Accuracy _result = NUIManager.Instance.ProfileManager.pmSituations.ResolveSituation(modifier, _usedAtt);
        string result = NLocalization.LocalizeAccuracy(_result).ToUpper();
        SectionLog.ShowResult(_msg, result);
    }

    public List<ProfileGlobals.Attributes> GetUsedAttributes()
    {
        List<ProfileGlobals.Attributes> _used = new List<ProfileGlobals.Attributes>();

        if (VIG.Selected) _used.Add(VIG.Attribute);
        if (DEX.Selected) _used.Add(DEX.Attribute);
        if (INT.Selected) _used.Add(INT.Attribute);
        if (PRE.Selected) _used.Add(PRE.Attribute);

        return _used;
    }

}
