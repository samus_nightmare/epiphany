﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProdigioSlot : MonoBehaviour
{
    public Text Label;
    public Image Icon;

    [HideInInspector]
    Prodigio prodigio = null;

    public void Load(Prodigio prd)
    {
        prodigio = prd;

        Label.text = prodigio.Name + " ( Nivel " + prodigio.Level + ")";
        Icon.sprite = prodigio.Icon;
    }

    public void Click()
    {
        NUIManager.Instance.ProfileEditor.ProdigiosSection.Editor.Open(prodigio, ProdigiosEdit.Mode.EditExisting);
    }
}
