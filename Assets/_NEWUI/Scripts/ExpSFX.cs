﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExpSFX : MonoBehaviour
{
    public Text Highlight, Shadow;
    public SpringPosition Spring;

    public Color Increment, Decrement;

    public void Show(int exp)
    {
        if (exp == 0)
            return;

        if (exp < 0)
            Show(exp.ToString(), Increment);
        else
            Show("+" + exp.ToString(), Decrement);
    }

    public void Show(string str, Color colorToUse)
    {
        transform.position = Input.mousePosition;
        Spring.target = transform.position + Vector3.up * 15;
        Highlight.text = str;
        Shadow.text = str;
        Highlight.color = colorToUse;
        gameObject.SetActive(true);
        Spring.enabled = true;
    }

    void EndTravel()
    {
        gameObject.SetActive(false);
    }
}
