﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PEditModifierSlot : MonoBehaviour
{
    public Profile.Modifier modifier;

    public Text Text;

    public void Click()
    {
        NUIManager.Instance.ProfileEditor.ModifiersSection.OpenEditor(this);
    }

    public void Load(Profile.Modifier mod)
    {
        modifier = mod;

        Text.text = modifier.name + " (" + PEditModifiers.WrapLevelWithColor(modifier.level) + ")";
    }
}
