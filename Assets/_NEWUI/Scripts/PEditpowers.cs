﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PEditpowers : MonoBehaviour
{
    public ShowProdigios Selector;
    public ProdigiosEdit Editor;
    public ProdigiosGallery Gallery;

    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.ProfileEditor.CloseAllOtherSubsections(gameObject);

        Selector.Open();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void CloseOthers(GameObject exception)
    {
        if (exception != Selector.gameObject) { Selector.Close(); }
        if (exception != Editor.gameObject) { Editor.Close(); }
        if (exception != Gallery.gameObject) { Gallery.Close(); }
    }
}
