﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class DecoText : MonoBehaviour
{
    int numberOfLines = 50;
    int currentLines = 0;
    int currentPhrase = 0;

    string[] phrases =
        {
            "EXECUTE ","PROMPT ","REMOVAL ","ACCESS ","SUBSYSTEM ","ID ","COMMAND ", "EMOTION ","INTERFACE ","LOCATION ","USER ", "INTERACTION ", "WAIT ","REQUEST ", "PROFILE "
        };

    public Text Text;

    void OnEnable()
    {
        Text.text = "   ";
        currentPhrase = Random.Range(0, phrases.Length - 1);

        InvokeRepeating("Type", 0, Random.Range(0.05f, 0.33f));
    }

    void Type()
    {
        Change(Random.Range(1, 10) > 8);
    }

    void Change(bool addLineJump)
    {
        if (currentLines > numberOfLines)
        {
            currentLines = 0;
            Text.text = "   ";
            currentPhrase = Random.Range(0, phrases.Length - 1);
        }

        Text.text = Text.text.Remove(Text.text.Length - 1);

        Text.text += (addLineJump ? "\n" : "") + RandomLetter() + "\u2588";
        currentLines++;
        currentPhrase++;
    }

    string RandomLetter()
    {
        //int desiredCodeLength = UnityEngine.Random.Range(3, 8);
        //string code = "";
        //char[] characters = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        //while (code.Length < desiredCodeLength)
        //{
        //    code += characters[Random.Range(0, characters.Length)];
        //}

        if (currentPhrase >= phrases.Length)
            currentPhrase = 0;

        return phrases[currentPhrase];
    }
}
