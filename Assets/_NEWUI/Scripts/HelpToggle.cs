﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HelpToggle : MonoBehaviour
{
    public string ModifierText = "???";
    public int ModifierLevel = 1;

    public Toggle Toggle;
    public Text Text;
    public TiradasHelpTable Controller;

    string originalString = "";

    public void Click()
    {
        Controller.Toggle(this);
    }

    void OnValidate()
    {
        Text.text = ModifierText + " (" + PEditModifiers.WrapLevelWithColor(ModifierLevel) + ")";
    }

    void Awake()
    {
        originalString = Text.text;
    }

    public void Refresh()
    {
        Text.text = DecodeText(originalString);
    }

    string DecodeText(string txt)
    {
        return txt.Replace("<%SKILL%>", GetMostImportantUsedSkill());
    }

    string GetMostImportantUsedSkill()
    {
        List<ProfileGlobals.Attributes> _usedAttr = NUIManager.Instance.NMain.Tiradas.GetUsedAttributes();


        if (_usedAttr.Count == 0)
            return NLocalization.LocalizeAttributeShort(ProfileGlobals.Attributes.SKILL);

        if (_usedAttr.Count == 1)
            return NLocalization.LocalizeAttributeShort(_usedAttr[0]);

        if (_usedAttr.Contains(ProfileGlobals.Attributes.SKILL))
            return NLocalization.LocalizeAttributeShort(ProfileGlobals.Attributes.SKILL);
        else if (_usedAttr.Contains(ProfileGlobals.Attributes.INSIGHT))
            return NLocalization.LocalizeAttributeShort(ProfileGlobals.Attributes.INSIGHT);
        else
            return NLocalization.LocalizeAttributeShort(ProfileGlobals.Attributes.VIGOR);

    }

    public void ForceLoad(int lvl, string str, TiradasHelpTable Table)
    {
        ModifierText = str;
        ModifierLevel = lvl;
        Controller = Table;

        Text.text = str + " (" + PEditModifiers.WrapLevelWithColor(ModifierLevel) + ")";//(ModifierLevel > 0 ? "+" + ModifierLevel.ToString() : ModifierLevel.ToString())
        ModifierText = Text.text;
        originalString = Text.text;
    }

}
