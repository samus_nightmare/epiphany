﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class ProdigiosEdit : MonoBehaviour
{
    public enum Mode { AddNew = 0, EditExisting };

    public Text Title;
    public Text Description;
    public Text OpcAdicion;
    public Image Icon;

    public GameObject ModeAdd, ModeEdit;

    [HideInInspector]
    public Prodigio editingProdigio;

    public void Open(Prodigio prd, Mode mode = Mode.AddNew)
    {
        gameObject.SetActive(true);

        NUIManager.Instance.ProfileEditor.ProdigiosSection.CloseOthers(gameObject);

        editingProdigio = prd;

        Load();

        Description.transform.parent.gameObject.SetActive(true);

        ModeAdd.SetActive(mode == Mode.AddNew);
        ModeEdit.SetActive(mode == Mode.EditExisting);

    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    void Load()
    {
        Title.text = editingProdigio.Name + " (Nivel " + editingProdigio.Level + ")";
        Description.text = editingProdigio.Description;
        OpcAdicion.text = editingProdigio.AdditionalOptions;
        Icon.sprite = editingProdigio.Icon;
    }

    public void Delete()
    {
        NUIManager.Instance.ProfileEditor.editingProfile.DeletePower(editingProdigio.ID);
        NUIManager.Instance.ProfileEditor.Save();
        Done();
    }

    public void Done()
    {
        NUIManager.Instance.ProfileEditor.RefreshXP();
        NUIManager.Instance.ProfileEditor.ProdigiosSection.Selector.Open();
    }

    public void Back()
    {
        NUIManager.Instance.ProfileEditor.ProdigiosSection.Gallery.Open();
    }

    public void Add()
    {
        NUIManager.Instance.ProfileEditor.editingProfile.AddPower(editingProdigio.ID);
        NUIManager.Instance.ProfileEditor.Save();

        Done();
    }
}
