﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ProfileFile : MonoBehaviour
{
    public Profile profile = null;
    public Text Text;
    public InputField newNameInput;

    public void Click()
    {
        NUIManager.Instance.ProfileManager.profile = profile;
        NUIManager.Instance.NMain.Open();
    }

    public void ChangeName(string newName)
    {
        newNameInput.gameObject.SetActive(false);

        if (string.IsNullOrEmpty(newName))
            return;

        //Delete old profile
        NUIManager.Instance.ProfileManager.deleteProfile(profile.name);

        //Change name to profile
        profile.name = newName;

        //Save as a new profile
        Profile _previousProfile = NUIManager.Instance.ProfileManager.profile;
        NUIManager.Instance.ProfileManager.profile = profile;
        NUIManager.Instance.ProfileManager.writeProfile();
        NUIManager.Instance.ProfileManager.profile = _previousProfile;

        Load(profile);
        Edit();
    }

    public void Edit()
    {
        NUIManager.Instance.ProfileEditor.Open(profile);
    }

    public void Load(Profile prf)
    {
        profile = prf;
        Text.text = prf.name;
        Text.text += "\n" + "EXP: " + NUIManager.Instance.ProfileEditor.ExpCount(prf);
    }

    public void DisplayNameChange()
    {
        newNameInput.gameObject.SetActive(!newNameInput.gameObject.activeInHierarchy);
        if (newNameInput.gameObject.activeInHierarchy)
        {
            EventSystem.current.SetSelectedGameObject(newNameInput.gameObject, null);
            newNameInput.ActivateInputField();
        }
    }

   
}
