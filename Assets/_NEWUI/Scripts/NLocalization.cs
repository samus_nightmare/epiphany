﻿using UnityEngine;
using System.Collections;

public class NLocalization : MonoBehaviour
{
    public static string LocalizeAccuracy(ProfileGlobals.Accuracy original)
    {
        //todo: ñapa provisional para español. Esto dependerá en el futuro de un sistema de localizacion más serio.
        switch (original)
        {
            case ProfileGlobals.Accuracy.CATASTROPHIC: return "Catástrofe";
            case ProfileGlobals.Accuracy.FAILURE: return "Fracaso";
            case ProfileGlobals.Accuracy.MISS: return "Fallo";
            case ProfileGlobals.Accuracy.SUCCESS: return "Acierto";
            case ProfileGlobals.Accuracy.EXTRAORDINARY: return "Extraordinario";
            case ProfileGlobals.Accuracy.PERFECT: return "Perfecto";

            default: return "???";
        }
    }

    public static string LocalizeAttribute(ProfileGlobals.Attributes att)
    {
        switch (att)
        {
            case ProfileGlobals.Attributes.VIGOR: return "Vigor";
            case ProfileGlobals.Attributes.SKILL: return "Destreza";
            case ProfileGlobals.Attributes.INSIGHT: return "Inteligencia";
            case ProfileGlobals.Attributes.PRESENCE: return "Presencia";

            default: return "???";
        }
    }

    public static string LocalizeAttributeShort(ProfileGlobals.Attributes att)
    {
        switch (att)
        {
            case ProfileGlobals.Attributes.VIGOR: return "VIG";
            case ProfileGlobals.Attributes.SKILL: return "DES";
            case ProfileGlobals.Attributes.INSIGHT: return "INT";
            case ProfileGlobals.Attributes.PRESENCE: return "PRE";

            default: return "???";
        }
    }

    public static string LocalizeHealthLevel(ProfileGlobals.HealthLevels lvl)
    {
        switch (lvl)
        {
            case ProfileGlobals.HealthLevels.FINE: return "Sano";
            case ProfileGlobals.HealthLevels.BRUISED: return "Magullado";
            case ProfileGlobals.HealthLevels.HURT: return "Herido";
            case ProfileGlobals.HealthLevels.INJURED: return "Grave";
            case ProfileGlobals.HealthLevels.MORIBUND: return "Moribundo";
            case ProfileGlobals.HealthLevels.DEAD: return "Muerto";

            default: return "???";
        }
    }

    public static string LocalizeAttributeLevel(ProfileGlobals.AttributesLevel lvl)
    {
        switch (lvl)
        {
            case ProfileGlobals.AttributesLevel.PRODIGIOUS: return "Prodigioso";
            case ProfileGlobals.AttributesLevel.EXCELLENT: return "Excelente";
            case ProfileGlobals.AttributesLevel.COMPETENT: return "Competente";
            case ProfileGlobals.AttributesLevel.AVERAGE: return "Mediocre";
            case ProfileGlobals.AttributesLevel.POOR: return "Pobre";
            case ProfileGlobals.AttributesLevel.SUBHUMAN: return "Pésimo";

            default: return "???";
        }
    }

    public static string LocalizeCatharsisLevel(int level)
    {
        if (level < 1)
            return "No se aplica";

        return "Nivel " + level.ToString();
    }

    public static string LocalizeCatharsisLevelShort(int level)
    {
        if (level < 1)
            return "N/A";

        return level.ToString();
    }
}
