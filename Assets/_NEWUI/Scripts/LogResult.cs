﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LogResult : MonoBehaviour 
{
    public Image Image;
    public Text Text_Up;
    public Text Text_Down;
    public TweenScale TweenScale;
}
