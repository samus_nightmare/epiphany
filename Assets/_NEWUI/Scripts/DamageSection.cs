﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DamageSection : MonoBehaviour
{
    public GameObject Content;

    public Text Label;

    public string Title = "???";

    public void Click()
    {
        Content.SetActive(!Content.activeInHierarchy);
    }


    void OnValidate()
    {
        Label.text = Title;
    }


    public void Toggle_Abrasion(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetNature(ProfileGlobals.DamageTypes.ABRASION); } }
    public void Toggle_Impact(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetNature(ProfileGlobals.DamageTypes.IMPACT); } }
    public void Toggle_Cut(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetNature(ProfileGlobals.DamageTypes.INCISION); } }
    public void Toggle_Pierce(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetNature(ProfileGlobals.DamageTypes.PIERCE); } }


    public void Toggle_Indeterminate(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetLocation(ProfileGlobals.ImpactLocalization.INDETERMINATE); } }
    public void Toggle_Limb(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetLocation(ProfileGlobals.ImpactLocalization.LIMB); } }
    public void Toggle_Superficial(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetLocation(ProfileGlobals.ImpactLocalization.SUPERFICIAL); } }
    public void Toggle_Vital(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetLocation(ProfileGlobals.ImpactLocalization.VITAL); } }


    public void Toggle_Intensity_1(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetIntensity(1); } }
    public void Toggle_Intensity_2(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetIntensity(2); } }
    public void Toggle_Intensity_3(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetIntensity(3); } }
    public void Toggle_Intensity_4(bool ab) { if (ab) { NUIManager.Instance.NMain.Dano.SetIntensity(4); } }


}
