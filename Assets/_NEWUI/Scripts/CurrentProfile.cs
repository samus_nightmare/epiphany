﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CurrentProfile : MonoBehaviour
{
    public Text Name;
    public Text Status;
    public Image Color;

    public void SelectProfile()
    {
        NUIManager.Instance.ProfileSelector.Open();
    }

    public void EditProfile()
    {
        NUIManager.Instance.ProfileEditor.Open(NUIManager.Instance.ProfileManager.profile);

    }


    public void ShowProfile(Profile prf)
    {
        Name.text = prf.name;
        Status.text = "Salud: " + NLocalization.LocalizeHealthLevel(prf.health) + "   " + "Catarsis: " + NLocalization.LocalizeCatharsisLevelShort(prf.catharsis);
    }

    public void Refresh()
    {
        ShowProfile(NUIManager.Instance.ProfileManager.profile);
    }

}
