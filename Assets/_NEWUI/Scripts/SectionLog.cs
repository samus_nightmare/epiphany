﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SectionLog : MonoBehaviour
{
    public GameObject LogResultPrefab;

    public GameObject LogDamagePrefab;

    public Transform List;

    public ScrollRect ListRect;

    public Scrollbar ListRectScrollbar;



    bool _busy = false;


    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.NMain.CloseOtherSections(gameObject);
        NUIManager.Instance.NMain.lastVisitedSection = NMain.Sections.Log;
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Clear()
    {
        NUIManager.DeleteAllChildren(List);
    }



    public void AddNewResult(string message, string result)
    {
        GameObject _obj = Instantiate(LogResultPrefab) as GameObject;

        _obj.transform.parent = List;
        //_obj.transform.localScale = LogResultPrefab.transform.localScale;

        LogResult _result = _obj.GetComponent<LogResult>();

        _result.Text_Up.text = message;
        _result.Text_Down.text = result;

        ListRectScrollbar.value = 1;

        _result.TweenScale.to = LogResultPrefab.transform.localScale;

        _result.TweenScale.Toggle();
    }

    public static void ShowResult(string message, string result)
    {
        NUIManager.Instance.StartCoroutine(NUIManager.Instance.NMain.Log.ShowResult_Seq(message, result));
    }

    public IEnumerator ShowResult_Seq(string message, string result)
    {
        if (!_busy)
        {
            _busy = true;

            yield return NUIManager.Instance.StartCoroutine(NUIManager.Instance.LoadingScreen.Show());

            if (!NUIManager.Instance.NMain.gameObject.activeInHierarchy)
                NUIManager.Instance.NMain.Open();

            NUIManager.Instance.NMain.OpenSection(NMain.Sections.Log);
            _busy = false;
        }

        AddNewResult(message, result);
    }



    public void AddNewDamage(string message)
    {
        GameObject _obj = Instantiate(LogDamagePrefab) as GameObject;

        _obj.transform.parent = List;

        LogDamage _box = _obj.GetComponent<LogDamage>();

        _box.Text.text = message;

        ListRectScrollbar.value = 1;

        _box.TweenScale.to = LogResultPrefab.transform.localScale;

        _box.TweenScale.Toggle();
    }


    public void AddNewPower(string message)
    {
        GameObject _obj = Instantiate(LogDamagePrefab) as GameObject;

        _obj.transform.parent = List;

        LogDamage _box = _obj.GetComponent<LogDamage>();

        _box.Text.text = message;
        _box.Color.color = NUIManager.Instance.NMain.Prodigios.Description.ResultsColor;

        ListRectScrollbar.value = 1;

        _box.TweenScale.to = LogResultPrefab.transform.localScale;

        _box.TweenScale.Toggle();
    }

    public static void ShowDamage(string message)
    {
        NUIManager.Instance.StartCoroutine(NUIManager.Instance.NMain.Log.ShowDamage_Seq(message));
    }

    public IEnumerator ShowDamage_Seq(string message)
    {
        if (!_busy)
        {
            _busy = true;

            yield return NUIManager.Instance.StartCoroutine(NUIManager.Instance.LoadingScreen.Show());

            if (!NUIManager.Instance.NMain.gameObject.activeInHierarchy)
                NUIManager.Instance.NMain.Open();

            NUIManager.Instance.NMain.OpenSection(NMain.Sections.Log);
            _busy = false;
        }

        AddNewDamage(message);
    }


    public static void ShowPower(string message)
    {
        NUIManager.Instance.StartCoroutine(NUIManager.Instance.NMain.Log.ShowPower_Seq(message));
    }

    public IEnumerator ShowPower_Seq(string message)
    {
        if (!_busy)
        {
            _busy = true;

            yield return NUIManager.Instance.StartCoroutine(NUIManager.Instance.LoadingScreen.Show());

            if (!NUIManager.Instance.NMain.gameObject.activeInHierarchy)
                NUIManager.Instance.NMain.Open();

            NUIManager.Instance.NMain.OpenSection(NMain.Sections.Log);
            _busy = false;
        }

        AddNewPower(message);
    }
}
