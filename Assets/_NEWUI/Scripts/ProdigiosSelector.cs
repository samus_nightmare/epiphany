﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ProdigiosSelector : MonoBehaviour
{
    public Transform List;
    public GameObject ProdigioSelectorButtonPrefab;

    List<ProdigioSelectorButton> addedButtons = new List<ProdigioSelectorButton>();

    public void Open()
    {
        gameObject.SetActive(true);

        NUIManager.Instance.NMain.Prodigios.CloseSubSections(gameObject);

        LoadAvailableProdigios();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }


    void LoadAvailableProdigios()
    {
        List<string> _myPowers = NUIManager.Instance.ProfileManager.profile.GetPowersIDs();

        List<Prodigio> _availableProdigios = NUIManager.Instance.ProdigiosList.Where(x => _myPowers.Contains(x.ID)).ToList();

        foreach (Prodigio _prodigion in _availableProdigios)
        {
            ProdigioSelectorButton _button = addedButtons.FirstOrDefault(x => x.Prodigio == _prodigion);
            if (_button == null)
            {
                AddProdigioToGrid(_prodigion);
            }
            else
            {
                _button.Load(_prodigion);
            }
        }

        //Delete obsolete buttons
        List<Prodigio> _toDelete = addedButtons.ConvertAll(x => x.Prodigio).ToList().Except(_availableProdigios).ToList();
        foreach (var _d in _toDelete)
        {
            ProdigioSelectorButton _button = addedButtons.FirstOrDefault(x => x.Prodigio == _d);
            if (_button != null)
            {
                Destroy(_button.gameObject);
            }
        }

    }

    void AddProdigioToGrid(Prodigio prd)
    {
        GameObject _obj = Instantiate(ProdigioSelectorButtonPrefab) as GameObject;
        _obj.transform.parent = List.transform;
        _obj.transform.localScale = ProdigioSelectorButtonPrefab.transform.localScale;

        ProdigioSelectorButton _container = _obj.GetComponent<ProdigioSelectorButton>();
        _container.Load(prd);

        addedButtons.Add(_container);
    }

}
