﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class ProdigiosGallery : MonoBehaviour
{
    public Transform List;
    public GameObject GalleryPrefab;

    public Text NotAvailableMsg;

    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.ProfileEditor.ProdigiosSection.CloseOthers(gameObject);

        Load();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    void Load()
    {
        ClearAll();

        //Carga todos los disponibles
        List<string> _yaTienes = NUIManager.Instance.ProfileEditor.editingProfile.GetPowersIDs();
        List<Prodigio> _available = NUIManager.Instance.ProdigiosList.Where(x => !_yaTienes.Contains(x.ID)).ToList();

        _available.ForEach(x => AddNew(x));

        NotAvailableMsg.transform.parent.gameObject.SetActive(_available.Count < 1);

    }

    void AddNew(Prodigio prd)
    {
        GameObject _obj = Instantiate(GalleryPrefab) as GameObject;
        _obj.transform.parent = List;
        _obj.transform.localScale = GalleryPrefab.transform.localScale;

        ProdigioGalleryButton _button = _obj.GetComponent<ProdigioGalleryButton>();
        _button.Load(prd);
    }

    void ClearAll()
    {
        NUIManager.DeleteAllChildren(List);
    }
}
