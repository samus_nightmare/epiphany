﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModifiersBar : MonoBehaviour
{
    public Text ModifierNumber;
    public GameObject Receptor;


    int _Modifier = 0;
    [HideInInspector]
    public int Modifier
    {
        get
        {
            return _Modifier;
        }

        set
        {
            _Modifier = value;
            ModifierNumber.text = PEditModifiers.WrapLevelWithColor(_Modifier);
        }
    }


    public void ModifierPlus()
    {
        Modifier = Mathf.Clamp(Modifier + 1, -10, 10);
    }

    public void ModifierMinus()
    {
        Modifier = Mathf.Clamp(Modifier - 1, -10, 10);
    }


    public void LaunchDice()
    {
        if (Receptor != null)
            Receptor.SendMessage("LaunchDice", _Modifier);
    }


}
