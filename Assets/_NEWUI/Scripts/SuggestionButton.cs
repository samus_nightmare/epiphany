﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SuggestionButton : MonoBehaviour
{
    public string Skill;
    public int Level;
    public string Description;

    public Text Label;



    void OnValidate()
    {
        Label.text = Skill;
    }
}
