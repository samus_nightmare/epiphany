﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ProfileSelector : MonoBehaviour
{
    public Transform List;

    public GameObject ProfileFilePrefab;

    List<ProfileFile> loadedProfiles = new List<ProfileFile>();


    public void Open()
    {
        gameObject.SetActive(true);

        NUIManager.Instance.CloseAllOtherWindows(gameObject);

        LoadAllProfiles();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Done()
    {
        NUIManager.Instance.NMain.Open();
    }


    void LoadAllProfiles()
    {
        List<Profile> _profiles = NUIManager.Instance.ProfileManager.GetAllProfiles();
        CleanAll();

        //Add/refresh profiles to the list
        foreach (Profile _prf in _profiles)
        {
            ProfileFile _container = loadedProfiles.FirstOrDefault(x => x.profile.name == _prf.name);

            if (_container == null)
                AddProfileToList(_prf);
            else
                _container.Load(_prf);
        }
    }

    ProfileFile AddProfileToList(Profile prf)
    {
        GameObject _f = Instantiate(ProfileFilePrefab) as GameObject;
        _f.transform.parent = List;
        _f.transform.localScale = ProfileFilePrefab.transform.localScale;

        ProfileFile _prfFile = _f.GetComponent<ProfileFile>();
        _prfFile.Load(prf);

        loadedProfiles.Add(_prfFile);

        return _prfFile;
    }

    public void CleanAll()
    {
        loadedProfiles.Clear();
        NUIManager.DeleteAllChildren(List.transform);
    }

    public void AddNew()
    {
        Profile _newProfile = new Profile();
        _newProfile.name = "";

        AddProfileToList(_newProfile).DisplayNameChange();

        //to-do: Scroll down to new profile created
        //List.localPosition -= Vector3.down * 100;
    }


}
