﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class TiradasHelpTable : MonoBehaviour
{
    List<HelpToggle> Toggles = new List<HelpToggle>();

    public Text TotalModifier;
    public Transform List;
    public ModifiersBar ModifiersBar;

    public GameObject HelpElementPrefab;
    List<HelpToggle> ModifiersAsToggles = new List<HelpToggle>();

    public void Open()
    {
        gameObject.SetActive(true);

        AddModifiersAsHelpToggles();

        ClearAll();

        SetModifier();

        RefreshAll();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Done()
    {
        ModifiersBar.Modifier = CalculateTotalModifier();
        Close();
    }


    void Start()
    {
        Toggles = List.GetComponentsInChildren<HelpToggle>().ToList();
        ClearAll();
        SetModifier();
    }

    public void Toggle(HelpToggle toggle)
    {
        SetModifier();
    }

    int CalculateTotalModifier()
    {
        int _mod = 0;
        foreach (var _toggle in Toggles)
        {
            if (_toggle.Toggle.isOn)
            {
                _mod += _toggle.ModifierLevel;
            }
        }

        return _mod;
    }

    void SetModifier()
    {
        TotalModifier.text = PEditModifiers.WrapLevelWithColor(CalculateTotalModifier());
    }

    public void ClearAll()
    {
        Toggles.ForEach(x => x.Toggle.isOn = false);
    }

    public void RefreshAll()
    {
        List.GetComponentsInChildren<HelpToggle>().ToList().ForEach(x => x.Refresh());
    }

    void AddModifiersAsHelpToggles()
    {
        ClearModifiersAsHelpToggles();

        List<Profile.Modifier> _modifiers = NUIManager.Instance.ProfileManager.profile.modifiers;

        foreach (Profile.Modifier _mod in _modifiers)
        {
            AddModifierAsToggle(_mod);
        }

        AddDESToggles();
    }

    void ClearModifiersAsHelpToggles()
    {
        while (ModifiersAsToggles.Count > 0)
        {
            DestroyImmediate(ModifiersAsToggles[0].gameObject);
            ModifiersAsToggles.RemoveAt(0);
        }
    }

    void AddModifierAsToggle(Profile.Modifier mod)
    {
        GameObject _obj = Instantiate(HelpElementPrefab) as GameObject;
        _obj.transform.parent = List;
        _obj.transform.localScale = HelpElementPrefab.transform.localScale;

        HelpToggle _toggle = _obj.GetComponent<HelpToggle>();
        _toggle.ForceLoad(mod.level, mod.name, this);

        ModifiersAsToggles.Add(_toggle);
    }

    void AddModifierAsToggle(string text, int level)
    {
        GameObject _obj = Instantiate(HelpElementPrefab) as GameObject;
        _obj.transform.parent = List;
        _obj.transform.localScale = HelpElementPrefab.transform.localScale;

        HelpToggle _toggle = _obj.GetComponent<HelpToggle>();
        _toggle.ForceLoad(level, text, this);

        ModifiersAsToggles.Add(_toggle);
    }

    void AddDESToggles()
    {
        //Cuantos niveles hay por encima
        ProfileGlobals.AttributesLevel _myLevel = NUIManager.Instance.ProfileManager.profile.getAttributeLevel(ProfileGlobals.Attributes.SKILL);

        for (int i = (int)ProfileGlobals.AttributesLevel.length - 1; i > (int)_myLevel; i--)
        {
            ProfileGlobals.AttributesLevel _level = (ProfileGlobals.AttributesLevel)i;
            int _mod = GetDEXAimPenalty(_level);
            string _text = "Acertar a enemigo con " + NLocalization.LocalizeAttributeShort(ProfileGlobals.Attributes.SKILL) + " " + NLocalization.LocalizeAttributeLevel(_level);

            AddModifierAsToggle(_text, _mod);
        }

    }

    int GetDEXAimPenalty(ProfileGlobals.AttributesLevel otherAttr)
    {
        int _nivelEnm = (int)otherAttr;
        int _nivelMio = (int)NUIManager.Instance.ProfileManager.profile.getAttributeLevel(ProfileGlobals.Attributes.SKILL);

        if (_nivelMio >= _nivelEnm)
            return 0;

        return _nivelMio - _nivelEnm;
    }
}
