﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PEditModifiers : MonoBehaviour
{
    public GameObject List;
    public PEditModifiersEdit Editor;

    public PEditModifierSlot SlotPrefab;

    List<PEditModifierSlot> loadedModifiers = new List<PEditModifierSlot>();


    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.ProfileEditor.CloseAllOtherSubsections(gameObject);

        OpenList();
        LoadList(NUIManager.Instance.ProfileEditor.editingProfile);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }


    public void OpenList()
    {
        List.transform.parent.gameObject.SetActive(true);
        List.SetActive(true);
        LoadList(NUIManager.Instance.ProfileEditor.editingProfile);
        Editor.Close();
    }

    public void OpenEditor(PEditModifierSlot slot)
    {
        Editor.Open(slot);
        List.SetActive(false);
        List.transform.parent.gameObject.SetActive(false);
    }

    void LoadList(Profile prf)
    {
        CleanAll();
        RemoveEmptyModifiers();

        foreach (var _mod in prf.modifiers)
        {
            PEditModifierSlot _slot = loadedModifiers.FirstOrDefault(x => x.modifier.name == _mod.name);

            if (_slot == null)
            {
                AddModifierToList(_mod);
            }
            else
            {
                _slot.Load(_mod);
            }
        }

        NUIManager.Instance.ProfileEditor.RefreshXP();
        NUIManager.Instance.ProfileEditor.Save();
    }

    void AddModifierToList(Profile.Modifier mod)
    {
        GameObject _slotObj = NUIManager.Instantiate(SlotPrefab.gameObject) as GameObject;

        _slotObj.transform.parent = List.transform;
        _slotObj.transform.localScale = SlotPrefab.transform.localScale;

        PEditModifierSlot _slot = _slotObj.GetComponent<PEditModifierSlot>();
        _slot.Load(mod);

        loadedModifiers.Add(_slot);
    }

    public void RemoveEmptyModifiers()
    {
        NUIManager.Instance.ProfileEditor.editingProfile.modifiers.RemoveAll(x => string.IsNullOrEmpty(x.name));
    }

    public void CleanAll()
    {
        loadedModifiers.Clear();
        NUIManager.DeleteAllChildren(List.transform);
    }

    public static string WrapLevelWithColor(int level)
    {
        return (level > 0 ? "<color=green>+" : level == 0 ? "<color=gray>" : "<color=red>") + level.ToString() + "</color>";
    }

    public void AddNew()
    {
        NUIManager.Instance.ProfileEditor.editingProfile.modifiers.Add(new Profile.Modifier(0, ""));
        AddModifierToList(NUIManager.Instance.ProfileEditor.editingProfile.modifiers.Last());
        OpenEditor(loadedModifiers.FirstOrDefault(x => x.modifier == NUIManager.Instance.ProfileEditor.editingProfile.modifiers.Last()));
    }
}
