﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class ShowProdigios : MonoBehaviour
{
    public Transform List;
    public GameObject ProdigioSlotPrefab;
    public Text ConductText;
    

    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.ProfileEditor.ProdigiosSection.CloseOthers(gameObject);

        LoadProdigios();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }


    void LoadProdigios()
    {
        ClearAll();

        List<string> _pIDs = NUIManager.Instance.ProfileEditor.editingProfile.GetPowersIDs();

        foreach (string _id in _pIDs)
        {
            AddProdigio(_id);
        }

        ConductText.text = NUIManager.Instance.ProfileEditor.editingProfile.conduct;
    }

    void AddProdigio(string ID)
    {
        //Busco el prodigio
        Prodigio _prd = NUIManager.Instance.ProdigiosList.FirstOrDefault(x => x.ID == ID);
        if (_prd == null)
        {
            Debug.LogError("Error reading Power (ID: " + ID + ")");
            return;
        }

        //Lo añado
        GameObject _obj = Instantiate(ProdigioSlotPrefab) as GameObject;
        _obj.transform.parent = List;
        _obj.transform.localScale = ProdigioSlotPrefab.transform.localScale;

        ProdigioSlot _slot = _obj.GetComponent<ProdigioSlot>();
        _slot.Load(_prd);

    }

    void ClearAll()
    {
        NUIManager.DeleteAllChildren(List);
    }

    public void ChangeConduct(string newConduct)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.conduct = newConduct;
        NUIManager.Instance.ProfileEditor.Save();
    }
}
