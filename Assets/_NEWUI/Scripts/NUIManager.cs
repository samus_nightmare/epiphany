﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class NUIManager : MonoBehaviour
{
    #region pseudosingleton
    public static NUIManager Instance = null;
    void Awake()
    {
        Instance = this;
    }
    #endregion

    public ProfileManager ProfileManager;

    public Popup Popup;

    public NMain NMain;

    public TitleScreen TitleScreen;

    public ProfileSelector ProfileSelector;

    public ProfileEditor ProfileEditor;

    public NLocalization Localization;

    public LoadingScreen LoadingScreen;

    public GameObject ProdigiosDescriptions;

    public Text ErrorConsole;

    public ExpSFX ExpSFX;

    [HideInInspector]
    public List<Prodigio> ProdigiosList = new List<Prodigio>();


    void Start()
    {
        ProfileManager.Init();
        LoadProdigios();
        TitleScreen.Open();
    }

    public void CloseAllOtherWindows(GameObject exception)
    {
        if (NMain.gameObject != exception) { NMain.Close(); }
        if (ProfileSelector.gameObject != exception) { ProfileSelector.Close(); }
        if (ProfileEditor.gameObject != exception) { ProfileEditor.Close(); }
        if (TitleScreen.gameObject != exception) { TitleScreen.Close(); }
    }

    public static void DeleteAllChildren(Transform parent)
    {
        var children = new List<GameObject>();
        foreach (Transform child in parent) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));
    }

    public void ChangedProfile()
    {
        NMain.Log.Clear();
    }

    public static void WriteError(string error)
    {
        if (!Instance.ErrorConsole.gameObject.activeInHierarchy)
            return;

        if (Instance.ErrorConsole.text.Length > 1000)
            Instance.ErrorConsole.text = "";

        Instance.ErrorConsole.text = error + "\n" + Instance.ErrorConsole.text;
    }

    public static string TruncateString(string username, int maxLength)
    {
        string _msg = username;

        if (_msg.Length > maxLength)
        {
            _msg.Remove(maxLength);
            _msg += "...";
        }

        return _msg;
    }

    public void LoadProdigios()
    {
        ProdigiosList = ProdigiosDescriptions.GetComponentsInChildren<Prodigio>().ToList();

    }

}
