﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class PEditModifiersEdit : MonoBehaviour
{
    public InputField NameInput;
    public Slider LevelSlider;
    public Text LevelText;
    public Text SuggestionsText;


    Profile.Modifier originalModifier = null;

    bool opening = false;

    public void Open(PEditModifierSlot slot)
    {
        opening = true;

        gameObject.SetActive(true);

        originalModifier = slot.modifier;

        NameInput.text = slot.modifier.name;

        SetLevel(slot.modifier.level);

        opening = false;
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Done()
    {
        if (!string.IsNullOrEmpty(NameInput.text))
        {
            int _mod = NUIManager.Instance.ProfileEditor.editingProfile.modifiers.FindIndex(x => x == originalModifier);

            if (NameInput.text != originalModifier.name)
            {
                //Overwriting a different skill
                int _mod2 = NUIManager.Instance.ProfileEditor.editingProfile.modifiers.FindIndex(x => x.name == NameInput.text);
                if (_mod2 >= 0)
                    _mod = _mod2;
            }

            NUIManager.Instance.ProfileEditor.editingProfile.modifiers[_mod].name = NameInput.text;
            NUIManager.Instance.ProfileEditor.editingProfile.modifiers[_mod].level = (int)LevelSlider.value;
        }

        NUIManager.Instance.ProfileEditor.RefreshXP();

        NUIManager.Instance.ProfileEditor.ModifiersSection.OpenList();
    }

    public void Delete()
    {
        if (!string.IsNullOrEmpty(NameInput.text))
        {
            NUIManager.Instance.ProfileEditor.editingProfile.modifiers.RemoveAll(x => x.name == NameInput.text);
        }

        NUIManager.Instance.ProfileEditor.RefreshXP();

        NUIManager.Instance.ProfileEditor.ModifiersSection.OpenList();
    }

    public void Suggestion(SuggestionButton sg)
    {
        SuggestionsText.text = sg.Skill.ToUpper() + "\n" + sg.Description;
        NameInput.text = sg.Skill;
        SetLevel(sg.Level);
    }

    public void ClearSuggestion()
    {
        SuggestionsText.text = "";
    }

    void SetLevel(int level)
    {
        LevelSlider.value = level;
        LevelText.text = PEditModifiers.WrapLevelWithColor(level);
    }

    public void CustomInputModifier(string input)
    {
        ClearSuggestion();
    }

    public void ChangeLevelBySlider(float level)
    {
        int _l = Mathf.RoundToInt(level);

        LevelText.text = PEditModifiers.WrapLevelWithColor(_l);

        //Enseñar exp que cuesta
        if (!opening)
        {
            int _nuevo = (int)NUIManager.Instance.ProfileEditor.GetXPFromModifierLevel(_l);
            int _viejo = (int)NUIManager.Instance.ProfileEditor.GetXPFromModifierLevel(originalModifier.level);

            NUIManager.Instance.ExpSFX.Show(_nuevo - _viejo);
        }
    }
}
