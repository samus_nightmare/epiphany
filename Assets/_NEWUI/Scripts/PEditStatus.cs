﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class PEditStatus : MonoBehaviour
{
    public Slider sHealth;
    public Text vHealth;
    public Slider sCatharsis;
    public Text vCatharsis;

    public Slider sVIG, sDEX, sINT, sPRE;
    public Text tVIG, tDEX, tINT, tPRE;


    public void Open()
    {
        gameObject.SetActive(true);

        NUIManager.Instance.ProfileEditor.CloseAllOtherSubsections(gameObject);

        Load(NUIManager.Instance.ProfileEditor.editingProfile);

    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    bool _firstLoad = false;
    public void Load(Profile prf)
    {
        _firstLoad = true;
        float _maxAttLvl = (float)ProfileGlobals.AttributesLevel.length;

        sHealth.value = (int)ProfileGlobals.HealthLevels.length - (int)prf.health;
        vHealth.text = NLocalization.LocalizeHealthLevel(prf.health);

        sCatharsis.value = NUIManager.Instance.ProfileEditor.editingProfile.catharsis;
        vCatharsis.text = NLocalization.LocalizeCatharsisLevel(NUIManager.Instance.ProfileEditor.editingProfile.catharsis);

        sVIG.value = (int)Enum.Parse(typeof(ProfileGlobals.AttributesLevel), prf.getAtribute(ProfileGlobals.Attributes.VIGOR));
        tVIG.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sVIG.value);

        sDEX.value = (int)Enum.Parse(typeof(ProfileGlobals.AttributesLevel), prf.getAtribute(ProfileGlobals.Attributes.SKILL));
        tDEX.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sDEX.value);

        sINT.value = (int)Enum.Parse(typeof(ProfileGlobals.AttributesLevel), prf.getAtribute(ProfileGlobals.Attributes.INSIGHT));
        tINT.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sINT.value);

        sPRE.value = (int)Enum.Parse(typeof(ProfileGlobals.AttributesLevel), prf.getAtribute(ProfileGlobals.Attributes.PRESENCE));
        tPRE.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sPRE.value);

        _firstLoad = false;
        NUIManager.Instance.ProfileEditor.RefreshXP();

    }

    public void ChangeHealth(float value)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.health = (ProfileGlobals.HealthLevels)((int)ProfileGlobals.HealthLevels.length - (int)value);
        vHealth.text = NLocalization.LocalizeHealthLevel(NUIManager.Instance.ProfileEditor.editingProfile.health);
        ValueChanged();
    }

    public void ChangeCatharsis(float value)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.catharsis = Mathf.RoundToInt(value);
        vCatharsis.text = NLocalization.LocalizeCatharsisLevel(NUIManager.Instance.ProfileEditor.editingProfile.catharsis);
        ValueChanged();
    }

    public void ChangeVIG(float value)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.setAtribute(ProfileGlobals.Attributes.VIGOR, (ProfileGlobals.AttributesLevel)value);
        tVIG.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sVIG.value);
        ValueChanged();
    }

    public void ChangeDEX(float value)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.setAtribute(ProfileGlobals.Attributes.SKILL, (ProfileGlobals.AttributesLevel)value);
        tDEX.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sDEX.value);
        ValueChanged();
    }

    public void ChangeINT(float value)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.setAtribute(ProfileGlobals.Attributes.INSIGHT, (ProfileGlobals.AttributesLevel)value);
        tINT.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sINT.value);
        ValueChanged();
    }

    public void ChangePRE(float value)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.setAtribute(ProfileGlobals.Attributes.PRESENCE, (ProfileGlobals.AttributesLevel)value);
        tPRE.text = NLocalization.LocalizeAttributeLevel((ProfileGlobals.AttributesLevel)sPRE.value);
        ValueChanged();
    }


    void ValueChanged()
    {
        if (!_firstLoad)
        {
            NUIManager.Instance.ProfileEditor.RefreshXP();
            NUIManager.Instance.ProfileEditor.Save();
        }
    }

}
