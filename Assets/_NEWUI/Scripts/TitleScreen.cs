﻿using UnityEngine;
using System.Collections;

public class TitleScreen : MonoBehaviour
{
    public void Open()
    {
        gameObject.SetActive(true);

        NUIManager.Instance.CloseAllOtherWindows(gameObject);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            NUIManager.Instance.NMain.Open();
        }
    }
}
