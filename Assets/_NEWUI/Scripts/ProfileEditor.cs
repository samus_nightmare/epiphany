﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class ProfileEditor : MonoBehaviour
{

    public CurrentEditingProfile ProfileSummary;
    public Scrollbar SectionScrollbar;
    public PEditStatus StatusSection;
    public PEditModifiers ModifiersSection;
    public PEditpowers ProdigiosSection;
    public PEditNotes NotesSection;



    public Profile editingProfile = null;
    bool _refreshedXP = false;


    public void Open(Profile prf)
    {
        if (prf == null)
        {
            Debug.LogError("Error: Profile to edit is null");
            return;
        }

        Clean();

        editingProfile = prf;

        gameObject.SetActive(true);

        NUIManager.Instance.CloseAllOtherWindows(gameObject);

        ProfileSummary.ShowProfile(editingProfile);

        StatusSection.Open();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void Done()
    {
        Save();

        NUIManager.Instance.ChangedProfile();

        NUIManager.Instance.NMain.Open();
    }

    public void Delete()
    {
        NUIManager.Instance.Popup.Show("Se borrará el perfil. ¿Continuar?",
            new Popup.Option[] 
            { 
                new Popup.Option("OK",
                        delegate 
                        {
                            NUIManager.Instance.ProfileManager.deleteProfile(editingProfile.name);
                            editingProfile = null;
                            NUIManager.Instance.ProfileManager.loadDefaultProfile();
                            NUIManager.Instance.ProfileSelector.Open();
                        }
                    ),
                new Popup.Option("Cancelar",
                    null
                    )
            }
            );
    }

    public void CloseAllOtherSubsections(GameObject exception, bool updateSection = true)
    {
        if (StatusSection.gameObject != exception) { StatusSection.Close(); } else { SetSectionScrollbar(0); }
        if (ModifiersSection.gameObject != exception) { ModifiersSection.Close(); } else { SetSectionScrollbar(1); }
        if (ProdigiosSection.gameObject != exception) { ProdigiosSection.Close(); } else { SetSectionScrollbar(2); }
        if (NotesSection.gameObject != exception) { NotesSection.Close(); } else { SetSectionScrollbar(3); }
    }

    bool setSectionLock = false;

    public void SetSectionScrollbar(float section)
    {
        if (setSectionLock)
        {
            setSectionLock = false;
            return;
        }

        SectionScrollbar.value = (section / SectionScrollbar.numberOfSteps) + 0.1f;
    }

    public void ChangeSectionScrollbar(float value)
    {
        int _v = Mathf.FloorToInt(value * SectionScrollbar.numberOfSteps);
        if (_v >= SectionScrollbar.numberOfSteps)
            _v = SectionScrollbar.numberOfSteps - 1;

        setSectionLock = true;
        switch (_v)
        {
            case 0: StatusSection.Open(); break;
            case 1: ModifiersSection.Open(); break;
            case 2: ProdigiosSection.Open(); break;
            default: NotesSection.Open(); break;
        }
    }

    public void Clean()
    {
        ModifiersSection.CleanAll();
    }

    public void Save()
    {
        NUIManager.Instance.ProfileManager.profile = editingProfile;

#if UNITY_WEBPLAYER

#else

        if (!NUIManager.Instance.ProfileManager.profile.unalterable)
        {
            NUIManager.Instance.ProfileManager.writeProfile();
        }

#endif
    }



    public void RefreshXP()
    {
        int _previousXP = editingProfile.experience;
        editingProfile.experience = ExpCount(editingProfile);
        _previousXP = editingProfile.experience - _previousXP;

        ProfileSummary.ShowProfile(editingProfile);

        NUIManager.Instance.ExpSFX.Show(_previousXP);

    }

    public int ExpCount(Profile _profile)
    {
        float xp = 0;
        int auxiliar;

        //Attributes
        foreach (string _attValue in _profile.attributes)
        {
            ProfileGlobals.AttributesLevel _attLevel = NUIManager.Instance.ProfileManager.profile.getAttributeLevel(_attValue);

            auxiliar = (int)_attLevel;
            for (int i = 0; i < auxiliar; i++)
            {
                xp += ProfileGlobals.experienceCosts.attributesUpgrading[i];
            }
        }

        //Modifiers
        foreach (var mod in _profile.modifiers)
        {
            xp += GetXPFromModifierLevel(mod.level);
        }

        //Powers
        List<Prodigio> _powers = GetProdigiosFromPlayer(_profile);
        foreach (Prodigio prd in _powers)
        {
            xp += GetXPFromPowerLevel(prd.Level);
        }

        return Mathf.CeilToInt(Mathf.Clamp(xp, 0, 999));
    }


    List<Prodigio> GetProdigiosFromPlayer(Profile _profile)
    {
        List<string> _ids = _profile.GetPowersIDs();
        return NUIManager.Instance.ProdigiosList.Where(x => _ids.Contains(x.ID)).ToList();
    }

    public float GetXPFromPowerLevel(int level)
    {
        switch (level)
        {
            case 1: return 4;
            case 2: return 7;
            case 3: return 11;
            case 4: return 16;
            default: return 0;
        }
    }

    public float GetXPFromModifierLevel(int level)
    {
        int auxiliar;
        if (level > 0)
        {
            auxiliar = Mathf.Clamp(level - 1, 0, ProfileGlobals.e3Values.MaxCharacterModifier);
            return ProfileGlobals.experienceCosts.PositiveModifiers[auxiliar];
        }
        else if (level < 0)
        {
            auxiliar = Mathf.Abs(level) - 1;
            return ProfileGlobals.experienceCosts.NegativeModifiers[auxiliar];
        }

        return 0;
    }
}
