﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CurrentEditingProfile : MonoBehaviour
{
    public Text Name;
    public Text Status;
    public Image Color;
    public Image LockedSymbol;

    public void ShowProfile(Profile prf)
    {
        Name.text = prf.name;
        Status.text = "Experiencia: " + prf.experience;
        LockedSymbol.gameObject.SetActive(prf.unalterable);
    }


}
