﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NMain : MonoBehaviour
{
    public enum Sections { Tiradas = 0, Prodigios, Dano, Log };


    public CurrentProfile CurrentProfile;
    public Scrollbar SelectorScroll;

    public SectionTiradas Tiradas;
    public SectionProdigios Prodigios;
    public SectionDano Dano;
    public SectionLog Log;

    public Sections lastVisitedSection = Sections.Tiradas;
    bool _ignoreScrollValue = false;

    public void Open()
    {
        CurrentProfile.ShowProfile(NUIManager.Instance.ProfileManager.profile);

        gameObject.SetActive(true);

        NUIManager.Instance.CloseAllOtherWindows(gameObject);

        OpenSection(lastVisitedSection);

        SetScrollbarToSection(lastVisitedSection);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void CloseOtherSections(GameObject exception)
    {
        if (exception != Tiradas.gameObject) { Tiradas.Close(); }
        if (exception != Prodigios.gameObject) { Prodigios.Close(); }
        if (exception != Dano.gameObject) { Dano.Close(); }
        if (exception != Log.gameObject) { Log.Close(); }
    }

    public void SectionSelector(float value)
    {
        if (_ignoreScrollValue)
            return;

        int _section = Mathf.FloorToInt(value * 4);
        if (_section > 2) _section = 3;

        OpenSection((Sections)_section, false);
    }

    public void OpenSection(Sections section, bool updateScrollbar = true)
    {
        switch (section)
        {
            case Sections.Tiradas: Tiradas.Open(); break;
            case Sections.Prodigios: Prodigios.Open(); break;
            case Sections.Dano: Dano.Open(); break;
            case Sections.Log: Log.Open(); break;
        }

        if (updateScrollbar)
            SetScrollbarToSection(section);
    }

    void SetScrollbarToSection(Sections section)
    {
        _ignoreScrollValue = true;

        switch (section)
        {
            case Sections.Tiradas: SelectorScroll.value = 0; break;
            case Sections.Prodigios: SelectorScroll.value = 0.25f; break;
            case Sections.Dano: SelectorScroll.value = 0.75f; break;
            case Sections.Log: SelectorScroll.value = 1; break;

        }

        _ignoreScrollValue = false;
    }


}
