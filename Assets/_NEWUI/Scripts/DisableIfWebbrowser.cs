﻿using UnityEngine;
using System.Collections;

public class DisableIfWebbrowser : MonoBehaviour
{
    void OnEnable()
    {
#if UNITY_WEBPLAYER
        gameObject.SetActive(false);
#endif
    }
}
