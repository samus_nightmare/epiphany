﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProdigioSelectorButton : MonoBehaviour
{
    public Text Label;
    public Image Icon;

    [HideInInspector]
    public Prodigio Prodigio = null;

    public void Click()
    {
        NUIManager.Instance.NMain.Prodigios.Description.Open(Prodigio);
    }

    public void Load(Prodigio prd)
    {
        Prodigio = prd;

        Label.text = string.IsNullOrEmpty(prd.Name) ? "???" : prd.Name;
        Icon.sprite = prd.Icon ?? Icon.sprite;
    }
}
