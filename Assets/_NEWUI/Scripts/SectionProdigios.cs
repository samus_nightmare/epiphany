﻿using UnityEngine;
using System.Collections;

public class SectionProdigios : MonoBehaviour
{
    public ProdigiosSelector Selector;
    public ProdigiosDescriptorSection Description;

    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.NMain.CloseOtherSections(gameObject);
        NUIManager.Instance.NMain.lastVisitedSection = NMain.Sections.Prodigios;

        Selector.Open();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    public void CloseSubSections(GameObject exception)
    {
        if (exception != Selector.gameObject) { Selector.Close(); }
        if (exception != Description.gameObject) { Description.Close(); }
    }
}
