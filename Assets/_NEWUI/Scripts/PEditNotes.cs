﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PEditNotes : MonoBehaviour
{
    public Text Notes;


    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.ProfileEditor.CloseAllOtherSubsections(gameObject);
        Load();
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

    void Load()
    {
        Notes.text = NUIManager.Instance.ProfileEditor.editingProfile.annotations;
    }

    public void ChangeText(string newText)
    {
        NUIManager.Instance.ProfileEditor.editingProfile.annotations = newText;
        NUIManager.Instance.ProfileEditor.Save();
    }
}
