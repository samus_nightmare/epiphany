﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SectionDano : MonoBehaviour
{

    public DamageSection Location;
    public DamageSection Nature;
    public DamageSection Intensity;

    ProfileGlobals.ImpactLocalization _location = ProfileGlobals.ImpactLocalization.INDETERMINATE;
    ProfileGlobals.DamageTypes _nature = ProfileGlobals.DamageTypes.IMPACT;
    int _intensity = 1;

    //Factor de letalidad
    /*Puede usarse para graduar la letalidad general del sistema.
    Cuanto más cercano a 1, más se sobrevive. Más cercano a 0, más letal.*/
    static float lethalityFactor = 0.6f;


    public void Open()
    {
        gameObject.SetActive(true);
        NUIManager.Instance.NMain.CloseOtherSections(gameObject);
        NUIManager.Instance.NMain.lastVisitedSection = NMain.Sections.Dano;

        Location.Content.gameObject.SetActive(true);
        Nature.Content.gameObject.SetActive(false);
        Intensity.Content.gameObject.SetActive(false);
    }

    public void Close()
    {
        gameObject.SetActive(false);

    }


    public void SetLocation(ProfileGlobals.ImpactLocalization loc)
    {
        _location = loc;
    }

    public void SetNature(ProfileGlobals.DamageTypes nat)
    {
        _nature = nat;
    }

    public void SetIntensity(int intensity)
    {
        _intensity = intensity;
    }






    public void LaunchDice()
    {
        //Calcular el daño
        int _vigor = (int)NUIManager.Instance.ProfileManager.profile.getAttributeLevel(ProfileGlobals.Attributes.VIGOR);
        float _dmg = GetIntensityMultiplier(_location, _nature) * ((1 - lethalityFactor) * _intensity);
        _dmg -= (UnityEngine.Random.Range(0, _vigor) * lethalityFactor);

        int _finalDmg = Mathf.RoundToInt(_dmg);

        //Restar el daño
        int _currentHealth = (int)NUIManager.Instance.ProfileManager.profile.health;
        _currentHealth += _finalDmg;
        if (_currentHealth >= (int)ProfileGlobals.HealthLevels.length)
            _currentHealth = (int)ProfileGlobals.HealthLevels.DEAD;

        NUIManager.Instance.ProfileManager.profile.health = (ProfileGlobals.HealthLevels)_currentHealth;
        NUIManager.Instance.NMain.CurrentProfile.Refresh();
        NUIManager.Instance.ProfileManager.writeProfile();

        //Calcular secuelas
        List<string> _aftermaths = new List<string>();
        if (NUIManager.Instance.ProfileManager.profile.health != ProfileGlobals.HealthLevels.DEAD)
        {
            _aftermaths = GetAftermathList(_location, _nature, _finalDmg);
        }

        //Mostrar los resultados
        string _msg = NUIManager.TruncateString(NUIManager.Instance.ProfileManager.profile.name, 12);

        if (NUIManager.Instance.ProfileManager.profile.health == ProfileGlobals.HealthLevels.DEAD)
        {
            _msg += " muere por el daño recibido.";
        }
        else if (_finalDmg > 0)
        {
            _msg += " queda en estado " + NLocalization.LocalizeHealthLevel(NUIManager.Instance.ProfileManager.profile.health);
            if (_aftermaths.Count > 0)
            {
                _msg += "\n" + "Secuelas: ";
                _msg += string.Join(",", _aftermaths.ToArray());
            }
        }
        else
        {
            _msg += " no sufre daños.";
        }

        SectionLog.ShowDamage(_msg);
    }

    float GetIntensityMultiplier(ProfileGlobals.ImpactLocalization loc, ProfileGlobals.DamageTypes nat)
    {
        switch (loc)
        {
            case ProfileGlobals.ImpactLocalization.INDETERMINATE:
                switch (nat)
                {
                    case ProfileGlobals.DamageTypes.ABRASION: return 1.25f;
                    case ProfileGlobals.DamageTypes.INCISION: return 2f;
                    case ProfileGlobals.DamageTypes.PIERCE: return 2f;
                    case ProfileGlobals.DamageTypes.IMPACT: return 1f;

                }
                break;

            case ProfileGlobals.ImpactLocalization.LIMB:
                switch (nat)
                {
                    case ProfileGlobals.DamageTypes.ABRASION: return 1f;
                    case ProfileGlobals.DamageTypes.INCISION: return 2.5f;
                    case ProfileGlobals.DamageTypes.PIERCE: return 0.75f;
                    case ProfileGlobals.DamageTypes.IMPACT: return 2f;

                }
                break;

            case ProfileGlobals.ImpactLocalization.SUPERFICIAL:
                switch (nat)
                {
                    case ProfileGlobals.DamageTypes.ABRASION: return 1f;
                    case ProfileGlobals.DamageTypes.INCISION: return 1.5f;
                    case ProfileGlobals.DamageTypes.PIERCE: return 0.5f;
                    case ProfileGlobals.DamageTypes.IMPACT: return 0.5f;

                }
                break;

            case ProfileGlobals.ImpactLocalization.VITAL:
                switch (nat)
                {
                    case ProfileGlobals.DamageTypes.ABRASION: return 0.5f;
                    case ProfileGlobals.DamageTypes.INCISION: return 1.5f;
                    case ProfileGlobals.DamageTypes.PIERCE: return 2.5f;
                    case ProfileGlobals.DamageTypes.IMPACT: return 2f;

                }
                break;

        }

        return 1;
    }

    List<string> GetAftermathList(ProfileGlobals.ImpactLocalization loc, ProfileGlobals.DamageTypes nat, int healthLost)
    {
        List<string> _list = new List<string>();

        float _chance = healthLost * 10;
        for (int i = 0; i < healthLost; i++)
        {
            //Por cada nivel de salud perdido probabilidad de sufrir secuela
            if (UnityEngine.Random.Range(0, 100) < _chance)
            {
                string _aftermath = GetRandomAftermath(loc, nat, Mathf.Clamp(Mathf.RoundToInt(UnityEngine.Random.Range(-1, 3) + healthLost), 1, 4));
                if (!string.IsNullOrEmpty(_aftermath))
                    _list.Add(_aftermath);
            }
        }

        return _list.Distinct().ToList();
    }


    //Lista de secuelas posibles
    string s_inconsciencia = "Inconsciencia";
    string s_amputacion = "Amputación";
    string s_desangramiento = "Desangramiento";
    string s_lesion = "Lesión";
    string s_marca = "Marca";
    string s_shock = "Shock";


    public string GetRandomAftermath(ProfileGlobals.ImpactLocalization loc, ProfileGlobals.DamageTypes nat, int choice)
    {
        //Secuelas de 1 a 4, donde 4 es la más rara y grave
        switch (nat)
        {
            #region Abrasión

            case ProfileGlobals.DamageTypes.ABRASION:
                switch (loc)
                {
                    case ProfileGlobals.ImpactLocalization.INDETERMINATE:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_marca;
                            case 3: return s_marca;
                            case 4: return s_shock;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.LIMB:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_marca;
                            case 3: return s_marca;
                            case 4: return s_lesion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.SUPERFICIAL:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_marca;
                            case 3: return s_marca;
                            case 4: return s_marca;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.VITAL:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_shock;
                            case 3: return s_inconsciencia;
                            case 4: return s_inconsciencia;
                        }
                        break;
                }
                break;
            #endregion
            #region Impacto
            case ProfileGlobals.DamageTypes.IMPACT:
                switch (loc)
                {
                    case ProfileGlobals.ImpactLocalization.INDETERMINATE:
                        switch (choice)
                        {
                            case 1: return s_shock;
                            case 2: return s_lesion;
                            case 3: return s_lesion;
                            case 4: return s_inconsciencia;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.LIMB:
                        switch (choice)
                        {
                            case 1: return s_shock;
                            case 2: return s_lesion;
                            case 3: return s_amputacion;
                            case 4: return s_amputacion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.SUPERFICIAL:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_marca;
                            case 3: return s_marca;
                            case 4: return s_lesion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.VITAL:
                        switch (choice)
                        {
                            case 1: return s_lesion;
                            case 2: return s_inconsciencia;
                            case 3: return s_inconsciencia;
                            case 4: return s_inconsciencia;
                        }
                        break;
                }
                break;
            #endregion
            #region Corte
            case ProfileGlobals.DamageTypes.INCISION:
                switch (loc)
                {
                    case ProfileGlobals.ImpactLocalization.INDETERMINATE:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_inconsciencia;
                            case 3: return s_desangramiento;
                            case 4: return s_amputacion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.LIMB:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_desangramiento;
                            case 3: return s_amputacion;
                            case 4: return s_amputacion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.SUPERFICIAL:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_marca;
                            case 3: return s_amputacion;
                            case 4: return s_amputacion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.VITAL:
                        switch (choice)
                        {
                            case 1: return s_inconsciencia;
                            case 2: return s_inconsciencia;
                            case 3: return s_desangramiento;
                            case 4: return s_desangramiento;
                        }
                        break;
                }
                break;
            #endregion
            #region Perforación
            case ProfileGlobals.DamageTypes.PIERCE:
                switch (loc)
                {
                    case ProfileGlobals.ImpactLocalization.INDETERMINATE:
                        switch (choice)
                        {
                            case 1: return s_shock;
                            case 2: return s_desangramiento;
                            case 3: return s_desangramiento;
                            case 4: return s_amputacion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.LIMB:
                        switch (choice)
                        {
                            case 1: return s_lesion;
                            case 2: return s_marca;
                            case 3: return s_desangramiento;
                            case 4: return s_amputacion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.SUPERFICIAL:
                        switch (choice)
                        {
                            case 1: return s_marca;
                            case 2: return s_marca;
                            case 3: return s_marca;
                            case 4: return s_lesion;
                        }
                        break;

                    case ProfileGlobals.ImpactLocalization.VITAL:
                        switch (choice)
                        {
                            case 1: return s_shock;
                            case 2: return s_inconsciencia;
                            case 3: return s_desangramiento;
                            case 4: return s_desangramiento;
                        }
                        break;
                }
                break;
            #endregion
        }

        return s_marca;
    }

}
