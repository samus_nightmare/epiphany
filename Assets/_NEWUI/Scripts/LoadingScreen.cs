﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour
{
    public Gradient MainImage;

    public IEnumerator Show()
    {
        gameObject.SetActive(true);
        MainImage.enabled = true;

        yield return new WaitForSeconds(2);

        MainImage.enabled = false;
        gameObject.SetActive(false);
    }



}
