﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;

public class Popup : MonoBehaviour
{
    public class Option
    {
        public string msg = "";
        public Action action = null;

        public Option() { }

        public Option(string _msg, Action _action)
        {
            this.msg = _msg;
            this.action = _action;
        }
    }


    public UnityEngine.UI.Button Button1, Button2;

    public Text Button1Text, Button2Text;

    public Text Message;

    public List<Option> storedOptions = new List<Option>();



    public void Show(string message, Option[] options)
    {
        Message.text = message;

        storedOptions = options.ToList();

        if (options.Length > 0)
        {
            Button1.gameObject.SetActive(true);
            Button1Text.text = options[0].msg;
        }
        else
        {
            Button1.gameObject.SetActive(false);
        }

        if (options.Length > 1)
        {
            Button2.gameObject.SetActive(true);
            Button2Text.text = options[1].msg;
        }
        else
        {
            Button2.gameObject.SetActive(false);
        }


        gameObject.SetActive(true);
    }


    public void ChooseOption(int option)
    {
        Close();

        if (storedOptions[option].action != null)
        {
            storedOptions[option].action();
        }
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

}
