﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProdigioGalleryButton : MonoBehaviour
{
    public Text Label;
    public Image Icon;
    public Text Level;

    [HideInInspector]
    public Prodigio prodigio;

    public void Load(Prodigio prd)
    {
        prodigio = prd;

        Label.text = prodigio.Name;
        Level.text = prodigio.Level.ToString();
        Icon.sprite = prodigio.Icon;
    }

    public void Click()
    {
        NUIManager.Instance.ProfileEditor.ProdigiosSection.Editor.Open(prodigio, ProdigiosEdit.Mode.AddNew);
    }
}
