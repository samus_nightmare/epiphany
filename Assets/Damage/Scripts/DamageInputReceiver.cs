﻿using UnityEngine;
using System.Collections;

public class DamageInputReceiver : MonoBehaviour
{
    public UIManager _UIManager;

    public UISprite btNatCorte, btNatPerforacion, btNatAbrasion, btNatImpacto, btLocIndeterminado, btLocExtremidad, btLocPuntoVital, btLocSuperficial;

    bool statusNatCorte = false, statusNatPerforacion = false, statusNatAbrasion = false, statusNatImpacto = false, statusLocIndeterminado = false, statusLocExtremidad = false, statusLocPuntoVital = false, statusLocSuperficial = false;




    void Button_Naturaleza_Corte()
    {
        _UIManager.ReceiveInput("dm_cut");
        TouchDamageNature(ref btNatCorte, ref statusNatCorte);
    }

    void Button_Naturaleza_Perforacion()
    {
        _UIManager.ReceiveInput("dm_pierce");
        TouchDamageNature(ref btNatPerforacion, ref statusNatPerforacion);
    }

    void Button_Naturaleza_Abrasion()
    {
        _UIManager.ReceiveInput("dm_abrasion");
        TouchDamageNature(ref btNatAbrasion, ref statusNatAbrasion);
    }

    void Button_Naturaleza_Impacto()
    {
        _UIManager.ReceiveInput("dm_impact");
        TouchDamageNature(ref btNatImpacto, ref statusNatImpacto);
    }




    void Button_Localizacion_Indeterminado()
    {
        _UIManager.ReceiveInput("dm_loc_indeterm");
        TouchDamageLocation(ref btLocIndeterminado, ref statusLocIndeterminado);
    }

    void Button_Localizacion_PuntoVital()
    {
        _UIManager.ReceiveInput("dm_loc_puntovital");
        TouchDamageLocation(ref btLocPuntoVital, ref statusLocPuntoVital);
    }

    void Button_Localizacion_Extremidad()
    {
        _UIManager.ReceiveInput("dm_loc_extremidad");
        TouchDamageLocation(ref btLocExtremidad, ref statusLocExtremidad);
    }

    void Button_Localizacion_Superficial()
    {
        _UIManager.ReceiveInput("dm_loc_superficial");
        TouchDamageLocation(ref btLocSuperficial, ref statusLocSuperficial);
    }



    void TouchDamageNature(ref UISprite sprite, ref bool status)
    {
        if (!status)
        {
            statusNatCorte = false;
            statusNatImpacto = false;
            statusNatAbrasion = false;
            statusNatPerforacion = false;

            btNatCorte.spriteName = "UIButton";
            btNatPerforacion.spriteName = "UIButton";
            btNatAbrasion.spriteName = "UIButton";
            btNatImpacto.spriteName = "UIButton";

            status = true;
            sprite.spriteName = "UIButtonSelected";
        }
    }

    void TouchDamageLocation(ref UISprite sprite2, ref bool status2)
    {
        if (!status2)
        {
            statusLocExtremidad = false;
            statusLocPuntoVital = false;
            statusLocIndeterminado = false;
            statusLocSuperficial = false;

            btLocPuntoVital.spriteName = "UIButton";
            btLocExtremidad.spriteName = "UIButton";
            btLocIndeterminado.spriteName = "UIButton";
            btLocSuperficial.spriteName = "UIButton";

            status2 = true;
            sprite2.spriteName = "UIButtonSelected";
        }
    }
}
