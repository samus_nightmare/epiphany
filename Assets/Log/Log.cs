﻿using UnityEngine;
using System.Collections;

public class Log : MonoBehaviour
{
    public static Log Instance = null;

    public UILabel textBody;
    public UILabel textTitle;


    public static string logText = "";

    public static void LogTextAdd(string text)
    {
        logText += text;

        RefreshText();
    }

    public static void LogTextReplace(string text)
    {
        logText = text;

        RefreshText();
    }

    public static void LogTextClean(string text)
    {
        LogTextReplace("");
    }





    public static void RefreshText()
    {
        if (Instance == null)
            Debug.Log("Es nulo");


        Instance.textTitle.text = LocalizationSystem.Instance.translation.log_title;
        Instance.textBody.text = logText;
        Instance.textTitle.text = LocalizationSystem.NormalizeString(LocalizationSystem.Instance.translation.log_title);
        Instance.textBody.text = LocalizationSystem.NormalizeString(logText);
    }





    void Awake()
    {

        if (Instance == null)
        {
            Instance = this;

            Instance.textBody = textBody;

            RefreshText();
        }
    }

}
