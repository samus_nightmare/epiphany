﻿using UnityEngine;
using System.Collections;

public class ResolutionControl : MonoBehaviour
{
    float originalWidth = 247;
    float originalHeight = 371;
    public static Vector3 scale = new Vector3();

    void OnGUI()
    {
        scale.x = Screen.width / originalWidth; // calculate hor scale
        scale.y = Screen.height / originalHeight; // calculate vert scale
        scale.z = 1;

        /*
        var svMat = GUI.matrix; // save current matrix
        // substitute matrix - only scale is altered from standard
        GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, scale);

        // restore matrix before returning
        GUI.matrix = svMat; // restore matrix
        */
    }

    public float fWidth = 9.0f; // Desired width

    void Start()
    {

        float fT = fWidth / Screen.width * Screen.height;
        fT = fT / (2.0f * Mathf.Tan(0.5f * Camera.main.fieldOfView * Mathf.Deg2Rad));
        Vector3 v3T = Camera.main.transform.position;
        v3T.z = -fT;
        transform.position = v3T;
    }
}
