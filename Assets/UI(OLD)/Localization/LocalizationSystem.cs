﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class LocalizationSystem : MonoBehaviour
{
    #region LocalizedStrings class

    public class Localizedstrings
    {
        public string op_button_log = "";
        public string op_disclaimerLabel = "epiphany 1.0";
        public string op_tab_situations = "";
        public string op_tab_emotions = "";
        public string op_tab_damage = "";

        public string dm_button_incision = "";
        public string dm_button_abrasion = "";
        public string dm_button_pierce = "";
        public string dm_button_impact = "";
        public string dm_button_ok = "";

        public string dm_event_nodamage = "";
        public string dm_event_statusitsin = "";
        public string dm_event_lostlimb = "";
        public string dm_event_modifierapply = "";



        public string healthNames_Fine = "";
        public string healthNames_Bruised = "";
        public string healthNames_Hurt = "";
        public string healthNames_Injured = "";
        public string healthNames_Moribund = "";
        public string healthNames_Dead = "";

        public string emo_Name_Anger = "";
        public string emo_Name_Disgust = "";
        public string emo_Name_Anticipation = "";
        public string emo_Name_Sadness = "";
        public string emo_Name_Joy = "";
        public string emo_Name_Surprise = "";
        public string emo_Name_Fear = "";
        public string emo_Name_Trust = "";

        public string emo_SelectCategory = "";

        public string sit_button_vigor = "";
        public string sit_button_insight = "";
        public string sit_button_skill = "";
        public string sit_button_presence = "";
        public string sit_button_ok = "";

        public string sit_resolve = "";

        public string prf_name = "";
        public string prf_experience = "";
        public string prf_health = "";
        public string prf_vigor = "";
        public string prf_insight = "";
        public string prf_skill = "";
        public string prf_presence = "";

        public string prf_save = "";
        public string prf_delete = "";
        public string prf_close = "";
        public string prf_editing = "";
        public string prf_addnew = "";

        public string log_title = "";
    }

    #endregion

    #region References

    public UILabel op_disclaimerLabel;

    public UILabel op_button_log;
    public UILabel op_tab_situations;
    public UILabel op_tab_emotions;
    public UILabel op_tab_damage;

    public GUIText dm_button_incision;
    public GUIText dm_button_abrasion;
    public GUIText dm_button_pierce;
    public GUIText dm_button_impact;
    public GUIText dm_button_ok;

    public GUIText em_button_anger;
    public GUIText em_button_anticipation;
    public GUIText em_button_disgust;
    public GUIText em_button_fear;
    public GUIText em_button_joy;
    public GUIText em_button_sadness;
    public GUIText em_button_surprise;
    public GUIText em_button_trust;
    public GUIText em_button_ok;
    public GUIText em_button_clear;

    public UILabel sit_button_vigor;
    public UILabel sit_button_insight;
    public UILabel sit_button_skill;
    public UILabel sit_button_presence;
    public UILabel sit_button_ok;

    #endregion

    [HideInInspector]
    public Localizedstrings translation;

    public static string currentLanguage = "English";

    public static LocalizationSystem Instance = null;

    public LocalizationSystem()
    {
        Instance = this;

        GetLanguage("English");
    }


    public bool GetLanguage(string language)
    {
        currentLanguage = language;

        translation = new Localizedstrings();

        //todo: Leer de XML segun el lenguage especificado, pero por ahora se ponen a pincho
        switch (language)
        {
            case "Spanish":

                translation.op_button_log = "Log";
                translation.op_disclaimerLabel = "epiphany 1.0b";
                translation.op_tab_situations = "Tiradas";
                translation.op_tab_emotions = "Efectos";
                translation.op_tab_damage = "Daño";

                translation.dm_button_incision = "Corte";
                translation.dm_button_abrasion = "Abrasión";
                translation.dm_button_pierce = "Penetrante";
                translation.dm_button_impact = "Impacto";
                translation.dm_button_ok = "OK";

                translation.dm_event_nodamage = "No sufre daño.";
                translation.dm_event_statusitsin = "queda";
                translation.dm_event_lostlimb = "pierde la extremidad";
                translation.dm_event_modifierapply = "aplica modificador";

                translation.healthNames_Fine = "Sano";
                translation.healthNames_Bruised = "Magullado";
                translation.healthNames_Hurt = "Herido";
                translation.healthNames_Injured = "Grave";
                translation.healthNames_Moribund = "Moribundo";
                translation.healthNames_Dead = "Muerto";


                translation.emo_Name_Anger = "Ira";
                translation.emo_Name_Anticipation = "Anticipacion";
                translation.emo_Name_Disgust = "Disgusto";
                translation.emo_Name_Fear = "Miedo";
                translation.emo_Name_Joy = "Alegría";
                translation.emo_Name_Sadness = "Tristeza";
                translation.emo_Name_Surprise = "Sorpresa";
                translation.emo_Name_Trust = "Confianza";
                translation.emo_SelectCategory = "Elige categoría";


                translation.sit_button_vigor = "Vigor";
                translation.sit_button_insight = "Intelecto";
                translation.sit_button_skill = "Destreza";
                translation.sit_button_presence = "Presencia";
                translation.sit_button_ok = "OK";

                translation.sit_resolve = "resuelve situacion";


                translation.prf_name = "Nombre:";
                translation.prf_experience = "Experiencia:";
                translation.prf_health = "Salud:";
                translation.prf_vigor = "VIGOR:";
                translation.prf_insight = "INTELIGENCIA:";
                translation.prf_skill = "DESTREZA:";
                translation.prf_presence = "PRESENCIA:";

                translation.prf_save = "Guardar";
                translation.prf_delete = "Borrar";
                translation.prf_close = "Cerrar";
                translation.prf_editing = "Editando:";
                translation.prf_addnew = "Nuevo...";

                translation.log_title = "Resultados";

                break;



            case "English":

                translation.op_button_log = "Log";

                translation.op_tab_situations = "Throws";
                translation.op_tab_emotions = "Effects";
                translation.op_tab_damage = "Damage";

                translation.dm_button_incision = "Cut";
                translation.dm_button_abrasion = "Abrasion";
                translation.dm_button_pierce = "Pierce";
                translation.dm_button_impact = "Impact";
                translation.dm_button_ok = "OK";

                translation.dm_event_nodamage = "avoid damage";
                translation.dm_event_statusitsin = "got";
                translation.dm_event_lostlimb = "loses limb";
                translation.dm_event_modifierapply = "applies modifier";

                translation.healthNames_Fine = "Fine";
                translation.healthNames_Bruised = "Bruised";
                translation.healthNames_Hurt = "Hurt";
                translation.healthNames_Injured = "Injured";
                translation.healthNames_Moribund = "Moribund";
                translation.healthNames_Dead = "Dead";

                translation.emo_Name_Anger = "Anger";
                translation.emo_Name_Anticipation = "Anticipation";
                translation.emo_Name_Disgust = "Disgust";
                translation.emo_Name_Fear = "Fear";
                translation.emo_Name_Joy = "Joy";
                translation.emo_Name_Sadness = "Sadness";
                translation.emo_Name_Surprise = "Surprise";
                translation.emo_Name_Trust = "Trust";
                translation.emo_SelectCategory = "Select Category";


                translation.sit_button_vigor = "Vigor";
                translation.sit_button_insight = "Insight";
                translation.sit_button_skill = "Skill";
                translation.sit_button_presence = "Presence";
                translation.sit_button_ok = "OK";

                translation.sit_resolve = "resolve situation";


                translation.prf_name = "Name:";
                translation.prf_experience = "Experience:";
                translation.prf_health = "Health:";
                translation.prf_vigor = "VIGOR:";
                translation.prf_insight = "INSIGHT:";
                translation.prf_skill = "SKILL:";
                translation.prf_presence = "PRESENCE:";

                translation.prf_save = "Save";
                translation.prf_delete = "Delete";
                translation.prf_close = "Close";
                translation.prf_editing = "Editing:";
                translation.prf_addnew = "Add new...";

                translation.log_title = "Results";

                break;
        }



        return true;
    }

    public void Translate(string language)
    {
        if (!GetLanguage(language))
        {
            Debug.LogError("Error reading language " + language);
            return;
        }

        if (op_button_log != null)
        {
            op_button_log.text = LocalizationSystem.NormalizeString(translation.op_button_log);
            op_disclaimerLabel.text = LocalizationSystem.NormalizeString(translation.op_disclaimerLabel);
            op_tab_situations.text = LocalizationSystem.NormalizeString(translation.op_tab_situations);
            op_tab_emotions.text = LocalizationSystem.NormalizeString(translation.op_tab_emotions);
            op_tab_damage.text = LocalizationSystem.NormalizeString(translation.op_tab_damage);
        }

        dm_button_incision.text = LocalizationSystem.NormalizeString(translation.dm_button_incision);
        dm_button_abrasion.text = LocalizationSystem.NormalizeString(translation.dm_button_abrasion);
        dm_button_pierce.text = LocalizationSystem.NormalizeString(translation.dm_button_pierce);
        dm_button_impact.text = LocalizationSystem.NormalizeString(translation.dm_button_impact);
        dm_button_ok.text = LocalizationSystem.NormalizeString(translation.dm_button_ok);




        sit_button_vigor.text = LocalizationSystem.NormalizeString(translation.sit_button_vigor);
        sit_button_insight.text = LocalizationSystem.NormalizeString(translation.sit_button_insight);
        sit_button_skill.text = LocalizationSystem.NormalizeString(translation.sit_button_skill);
        sit_button_presence.text = LocalizationSystem.NormalizeString(translation.sit_button_presence);
        sit_button_ok.text = LocalizationSystem.NormalizeString(translation.sit_button_ok);
    }


    public static string NormalizeString(string input)
    {
        //Solution 0: no hacer na
        //return input;

        //Solution 1: Regex
        //input = input.Normalize();
        //Regex reg = new Regex("[^a-zA-Z0-9 ]");
        //input = reg.Replace(input, "");

        if (input == null)
            return "";

        if (input.Length < 1)
            return "";

        //Solution 2: Replace manual
        input = input.Replace('ñ', 'n');
        input = input.Replace('á', 'a');
        input = input.Replace('é', 'e');
        input = input.Replace('í', 'i');
        input = input.Replace('ó', 'o');
        input = input.Replace('ú', 'u');
        input = input.Replace('Á', 'A');
        input = input.Replace('É', 'E');
        input = input.Replace('Í', 'I');
        input = input.Replace('Ó', 'O');
        input = input.Replace('Ú', 'U');
        input = input.Replace('ç', 'c');
        input = input.Replace('Ç', 'C');
        input = input.Replace('ä', 'a');
        input = input.Replace('ë', 'e');
        input = input.Replace('ï', 'i');
        input = input.Replace('ö', 'o');
        input = input.Replace('ü', 'u');
        input = input.Replace('Ä', 'A');
        input = input.Replace('Ë', 'E');
        input = input.Replace('Ï', 'I');
        input = input.Replace('Ö', 'O');
        input = input.Replace('Ü', 'U');
        input = input.Replace('à', 'a');
        input = input.Replace('è', 'e');
        input = input.Replace('ì', 'i');
        input = input.Replace('ò', 'o');
        input = input.Replace('ù', 'u');

        return input;
    }

    public static string HealthName(ProfileGlobals.HealthLevels level)
    {
        switch (level)
        {
            case ProfileGlobals.HealthLevels.FINE: return Instance.translation.healthNames_Fine;
            case ProfileGlobals.HealthLevels.BRUISED: return Instance.translation.healthNames_Bruised;
            case ProfileGlobals.HealthLevels.HURT: return Instance.translation.healthNames_Hurt;
            case ProfileGlobals.HealthLevels.INJURED: return Instance.translation.healthNames_Injured;
            case ProfileGlobals.HealthLevels.MORIBUND: return Instance.translation.healthNames_Moribund;
            case ProfileGlobals.HealthLevels.DEAD: return Instance.translation.healthNames_Dead;
            default: return "";
        }
    }

}
