﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    public static bool AndroidDebugConsoleActive = true;

    //Public vars
    public EmotionButton[] emotionButtons;
    public GameObject[] windows;
    public ButtonDamage[] damageButtons;
    public UILabel damageLabel;
    public UILabel situationLabel;
    public UILabel emotionLabel;
    public UIProfile profileUIManager;
    public ProfileManager profileManager;
    public Transform upPanel;

    public UILabel profileNameLabel;

    public Color modifierPositiveColor = Color.green, modifierNegativeColor = Color.red, modifierNeutralColor = Color.white;

    public UIEmotions UIEmotions;

    public EmotionsWheel emotionsWheel;

    public UILabel DebugLabel;

    public UISprite statusBarSprite;

    public TweenAlpha VectorAnimator;
    public AudioSource AudioSource;
    public float VectorAnimatorDuration = 2.5f;

    public EmotionsNavigator EmoNavigator;

    public EmotionalTree EmoTree;

    public TweenAlpha MenuBackground;

    //Private vars
    Dictionary<ProfileGlobals.Feelings, int> emotionsTable = new Dictionary<ProfileGlobals.Feelings, int>();
    int currentWindow = -1;
    List<int> previousWindows = new List<int>();
    int damageModifier = 0;
    int situationModifier = 0;
    int emotionModifier = 0;

    ProfileGlobals.DamageTypes selectedDamage = ProfileGlobals.DamageTypes.lenght;
    ProfileGlobals.ImpactLocalization selectedLocation = ProfileGlobals.ImpactLocalization.lenght;

    public static UIManager Instance = null;

    void Start()
    {
        Instance = this;

        ResetEmotionsTable();

        LocalizationSystem.Instance.Translate("Spanish");   //Idioma por defecto

        profileManager.Init();

        ChangeWindow(1);

        RefreshUI();

        profileUIManager.CreateCommandSlot();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackButton();
        }

        if (Input.GetKeyDown(KeyCode.Menu))
        {
            MenuButton();
        }


    }

    public void ReceiveInput(string input)
    {
        //Debug.Log("UI MANAGER Recibe input " + input);

        RefreshUI();
        switch (input)
        {

            //General
            case "op_situations":
                ChangeWindow(1);
                break;

            case "op_emotions":
                ChangeWindow(2);
                break;

            case "op_damage":
                ChangeWindow(3);
                break;

            case "op_profilesel":
                ChangeWindow(4);
                break;

            case "op_log":
                ChangeWindow(5);
                break;

            //Situations
            case "sit_ok":
                //if (profileManager.pmSituations.activeAttributes.Count > 0)
                //{
                //    profileManager.pmSituations.OkButton(situationModifier);
                //    ShowVectorAnimation(true);
                //}
                break;

            //case "sit_insight":
            //    profileManager.pmSituations.PushButton(ProfileGlobals.Attributes.INSIGHT);
            //    break;

            //case "sit_vigor":
            //    profileManager.pmSituations.PushButton(ProfileGlobals.Attributes.VIGOR);
            //    break;

            //case "sit_presence":
            //    profileManager.pmSituations.PushButton(ProfileGlobals.Attributes.PRESENCE);
            //    break;

            //case "sit_skill":
            //    profileManager.pmSituations.PushButton(ProfileGlobals.Attributes.SKILL);
            //    break;

            case "sit_modP":
                situationModifier = Mathf.Min(situationModifier + 1, ProfileGlobals.e3Values.MaxModifier);
                UpdateModifierNumbers();
                break;

            case "sit_modM":
                situationModifier = Mathf.Max(situationModifier - 1, ProfileGlobals.e3Values.MinModifier);
                UpdateModifierNumbers();
                break;


            //Damage
            case "dm_ok":
                if (selectedDamage != ProfileGlobals.DamageTypes.lenght && selectedLocation != ProfileGlobals.ImpactLocalization.lenght)
                {
                    profileManager.pmDamage.OkButton(selectedDamage, selectedLocation, damageModifier);
                    ShowVectorAnimation(true);
                }
                break;

            case "dm_modP":
                damageModifier = Mathf.Min(damageModifier + 1, ProfileGlobals.e3Values.MaxModifier);
                UpdateModifierNumbers();
                break;

            case "dm_modM":
                damageModifier = Mathf.Max(damageModifier - 1, ProfileGlobals.e3Values.MinModifier);
                UpdateModifierNumbers();
                break;

            case "dm_cut":
                selectedDamage = ProfileGlobals.DamageTypes.INCISION;
                break;

            case "dm_abrasion":
                selectedDamage = ProfileGlobals.DamageTypes.ABRASION;
                break;

            case "dm_impact":
                selectedDamage = ProfileGlobals.DamageTypes.IMPACT;
                break;

            case "dm_pierce":
                selectedDamage = ProfileGlobals.DamageTypes.PIERCE;
                break;

            case "dm_loc_indeterm":
                selectedLocation = ProfileGlobals.ImpactLocalization.INDETERMINATE;
                break;

            case "dm_loc_extremidad":
                selectedLocation = ProfileGlobals.ImpactLocalization.LIMB;
                break;

            case "dm_loc_puntovital":
                selectedLocation = ProfileGlobals.ImpactLocalization.VITAL;
                break;

            case "dm_loc_superficial":
                selectedLocation = ProfileGlobals.ImpactLocalization.SUPERFICIAL;
                break;

            //Emotion
            case "em_ok":
                Debug.Log("Receta por aquí");
                //TODO: seguir por aqui

                //if (EmotionsWheel.MixResult != null)
                //{
                //    ProfileManagerEmotions.ResolvedEmotion emoResult = profileManager.pmEmotions.ResolveEmotion(emotionsTable, EmotionsWheel.MixResult, emotionModifier);

                //    string showResult = "";
                //    showResult = profileManager.profile.name + ":\n" + emoResult.Result + "\n";

                //    foreach (var nEvent in emoResult.events)
                //    {
                //        showResult += profileManager.pmDamage.TextOfEvents(nEvent) + ".\n";
                //    }

                //    Log.LogTextReplace(showResult);
                //    ChangeWindow(5);
                //}
                break;

            case "em_modP":
                emotionModifier = Mathf.Max(emotionModifier + 1, ProfileGlobals.e3Values.MinModifier);
                UpdateModifierNumbers();
                break;

            case "em_modM":
                emotionModifier = Mathf.Max(emotionModifier - 1, ProfileGlobals.e3Values.MinModifier);
                UpdateModifierNumbers();
                break;

            case "em_clear":
                EmotionsClear();
                break;
        }
    }

    #region Emotions

    public void ReceiveInput_Emotion(string input, EmotionButton button)
    {
        Debug.Log("lleno la tabla con " + input);

        ProfileGlobals.Feelings myFeeling = ParseStringToFeeling(input);

        ProfileGlobals.Feelings oppFeeling = ProfileGlobals.utils.getOposite(myFeeling);

        emotionButtons[(int)oppFeeling].SetActive(false);

        emotionsTable[myFeeling]++;

        button.label.text = button.originalText + " (" + emotionsTable[myFeeling].ToString() + ")";

    }

    void ResetEmotionsTable()
    {
        emotionsTable.Clear();

        emotionsTable.Add(ProfileGlobals.Feelings.ANGER, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.ANTICIPATION, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.DISGUST, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.FEAR, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.JOY, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.SADNESS, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.SURPRISE, 0);
        emotionsTable.Add(ProfileGlobals.Feelings.TRUST, 0);
    }

    ProfileGlobals.Feelings ParseStringToFeeling(string input)
    {
        ProfileGlobals.Feelings _currentFeeling;

        switch (input)
        {
            case "em_anger":
                _currentFeeling = ProfileGlobals.Feelings.ANGER;
                break;

            case "em_anticipation":
                _currentFeeling = ProfileGlobals.Feelings.ANTICIPATION;
                break;

            case "em_disgust":
                _currentFeeling = ProfileGlobals.Feelings.DISGUST;
                break;

            case "em_fear":
                _currentFeeling = ProfileGlobals.Feelings.FEAR;
                break;

            case "em_joy":
                _currentFeeling = ProfileGlobals.Feelings.JOY;
                break;

            case "em_sadness":
                _currentFeeling = ProfileGlobals.Feelings.SADNESS;
                break;

            case "em_surprise":
                _currentFeeling = ProfileGlobals.Feelings.SURPRISE;
                break;

            case "em_trust":
                _currentFeeling = ProfileGlobals.Feelings.TRUST;
                break;

            default:
                _currentFeeling = ProfileGlobals.Feelings.ANGER;
                break;
        }

        return _currentFeeling;
    }

    void EmotionsClear()
    {
        ResetEmotionsTable();

        foreach (EmotionButton b in emotionButtons)
        {
            b.label.text = b.originalText;

            b.SetActive(true);

        }
    }

    #endregion

    #region UI Functions

    public void ChangeWindow(int window)
    {
        if (playingVector)
            return;

        if (window == 0)
        {
            for (int i = 1; i < windows.Length; i++)
                windows[i].SetActive(false);

            return;
        }

        if (currentWindow == window)
            return;

        for (int i = 1; i < windows.Length; i++)
        {
            if (i == window)
            {
                windows[i].SetActive(true);
            }
            else
            {
                windows[i].SetActive(false);
            }
        }


        //Special case: Log window refreshes text
        if (window == 5)
        {
            Log.RefreshText();
        }


        //Entering Emotions, refresh and clear
        if (window == 2)
        {
            emotionsWheel.Enter();
        }


        //Status panel shadow background
        switch (window)
        {
            case 1: statusBarSprite.spriteName = "UISectionBars_1"; break;
            case 2: statusBarSprite.spriteName = "UISectionBars_2"; break;
            case 3: statusBarSprite.spriteName = "UISectionBars_3"; break;
            case 5: statusBarSprite.spriteName = "UISectionBars_4"; break;
            default: statusBarSprite.spriteName = "UISectionBars"; break;
        }

        //Emotions Window
        if (window == 2)
        {
            EmoNavigator.Open();
        }
        else
        {
            EmoNavigator.Close();
        }


        //Special case: Profile windows (4 and 6) do not have Upper panel
        switch (window)
        {
            case 4:
                ShowUpPanel(false);
                profileUIManager.OpenSelector();
                break;

            case 6:
                ShowUpPanel(false);
                break;

            default:
                ShowUpPanel(true);
                profileUIManager.Save();
                break;
        }

        //Back-button window stack
        if (currentWindow != -1)
        {
            previousWindows.Add(currentWindow);
            if (previousWindows.Count > 4)
                previousWindows.RemoveAt(0);
        }
        currentWindow = window;
    }

    void UpdateModifierNumbers()
    {
        if (damageLabel != null)
        {
            damageLabel.text = Mathf.Abs(damageModifier).ToString();
            if (damageModifier < 0)
            {
                damageLabel.color = modifierNegativeColor;
                damageLabel.text = "-" + damageLabel.text;
            }
            else if (damageModifier > 0)
            {
                damageLabel.color = modifierPositiveColor;
                damageLabel.text = "+" + damageLabel.text;
            }
            else
            {
                damageLabel.color = modifierNeutralColor;
            }
        }

        if (situationLabel != null)
        {
            situationLabel.text = Mathf.Abs(situationModifier).ToString();

            if (situationModifier < 0)
            {
                situationLabel.color = modifierNegativeColor;
                situationLabel.text = "-" + situationLabel.text;
            }
            else if (situationModifier > 0)
            {
                situationLabel.color = modifierPositiveColor;
                situationLabel.text = "+" + situationLabel.text;
            }
            else
            {
                situationLabel.color = modifierNeutralColor;
            }
        }

        if (emotionLabel != null)
        {
            emotionLabel.text = Mathf.Abs(emotionModifier).ToString();

            if (emotionModifier < 0)
            {
                emotionLabel.color = modifierNegativeColor;
                emotionLabel.text = "-" + emotionLabel.text;
            }
            else if (emotionModifier > 0)
            {
                emotionLabel.color = modifierPositiveColor;
                emotionLabel.text = "+" + emotionLabel.text;
            }
            else
            {
                emotionLabel.color = modifierNeutralColor;
            }
        }

    }

    /*void UpdateDamageButtons()
    {
        //0 Abrasion    1 Impact    2 Incision   3 Pierce

        for (int i = 0; i < damageButtons.Length; i++)
        {
            if (i == 0 && selectedDamage == ProfileGlobals.DamageTypes.ABRASION)
            {
                damageButtons[i].SetActive(true);
            }
            else if (i == 1 && selectedDamage == ProfileGlobals.DamageTypes.IMPACT)
            {
                damageButtons[i].SetActive(true);
            }
            else if (i == 2 && selectedDamage == ProfileGlobals.DamageTypes.INCISION)
            {
                damageButtons[i].SetActive(true);
            }
            else if (i == 3 && selectedDamage == ProfileGlobals.DamageTypes.PIERCE)
            {
                damageButtons[i].SetActive(true);
            }
            else
            {
                damageButtons[i].SetActive(false);
            }
        }

    }
    */

    #endregion

    #region Buttons Mapping

    void Button_Downpanel_Situations() { ReceiveInput("op_situations"); }
    void Button_Downpanel_Emotions() { ReceiveInput("op_emotions"); }
    void Button_Downpanel_Damage() { ReceiveInput("op_damage"); }
    void Button_Downpanel_Log() { ReceiveInput("op_log"); }
    void Button_Uppanel_Profile() { ReceiveInput("op_profilesel"); }
    public void BackButton()
    {
        if (previousWindows.Count > 0)
        {
            ChangeWindow(previousWindows[previousWindows.Count - 1]);
            previousWindows.RemoveAt(previousWindows.Count - 1);
        }
        else
        {
            Debug.Log("Exit application");
            Application.Quit();
        }
    }
    public void MenuButton()
    {
        if (currentWindow != 4)
            ReceiveInput("op_profilesel");
        else
            BackButton();
    }

    #endregion

    public void RefreshUI()
    {
        if (profileManager.profile != null)
            profileNameLabel.text = LocalizationSystem.NormalizeString(profileManager.profile.name);
    }

    void ShowUpPanel(bool show)
    {
        upPanel.gameObject.SetActive(show);
    }

    int debugLines = 0;
    public void AndroidDebug(string show = "")
    {
        if (!UIManager.AndroidDebugConsoleActive)
        {
            DebugLabel.gameObject.SetActive(false);
            return;
        }


        debugLines++;
        if (debugLines > 15)
        {
            debugLines = 0;
            DebugLabel.text = "";
        }
        DebugLabel.text += "\n" + show;
    }


    bool playingVector = false;
    public void ShowVectorAnimation(bool show)
    {
        if (show)
        {
            if (!playingVector)
            {
                Debug.Log("Empieza animacion");

                ChangeWindow(0);

                VectorAnimator.Play(true);
                AudioSource.PlayOneShot(AudioSource.clip);

                Invoke("VectorAnimatorEnd", VectorAnimator.duration + VectorAnimatorDuration);
                playingVector = true;
            }
        }
        else
        {
            Debug.Log("Acaba animacion");

            VectorAnimator.Play(false);

        }
    }

    public void VectorAnimatorEnd()
    {
        ShowVectorAnimation(false);

        Invoke("OpenLog", VectorAnimator.duration);
    }

    void OpenLog()
    {
        playingVector = false;

        ChangeWindow(5);
    }

}
