﻿using UnityEngine;
using System.Collections;

public class DefaultModifierButton : MonoBehaviour
{
    public UILabel Text;

    public int Level = 1;

    [HideInInspector]
    public string ModifierName = "Unknown";

    [HideInInspector]
    public PopModifier PopController;

    public void OnClick()
    {
        PopController.SelectModifier(ModifierName, Level);
    }

}
