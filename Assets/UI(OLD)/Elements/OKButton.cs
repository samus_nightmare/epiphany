﻿using UnityEngine;
using System.Collections;

public class OKButton : MonoBehaviour
{
    public UIManager _UIManager;

    void Start()
    {
        _UIManager = GameObject.FindObjectOfType<UIManager>();
    }

    void sit_ok()
    {
        _UIManager.ReceiveInput("sit_ok");
    }

    void dm_ok()
    {
        _UIManager.ReceiveInput("dm_ok");
    }
}
