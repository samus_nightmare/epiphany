﻿using UnityEngine;
using System.Collections;

public class PopUpChangeName : MonoBehaviour
{
    public UIInput input;

    public void OkInput()
    {
        input.text = UIManager.Instance.profileUIManager.editingProfile.name = input.text;

        UIManager.Instance.profileUIManager.SaveAndRefresh();

        PopPupControl.Instance.CloseAllPopups();
    }

    public void Open()
    {
        input.text = UIManager.Instance.profileUIManager.editingProfile.name;

        PopPupControl.Instance.popChangeName.gameObject.SetActive(true);
    }
}
