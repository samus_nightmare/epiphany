﻿using UnityEngine;
using System.Collections;

public class PopupChangeEmotion : MonoBehaviour
{
    public UILabel title, bodytext;

    public UISlider slider;

    ProfileGlobals.Feelings emotionEditing = ProfileGlobals.Feelings.ANTICIPATION;
    int valueEditing = 0;



    void CloseButton()
    {
        UIManager.Instance.profileUIManager.StorePopupEmotion(emotionEditing, valueEditing);

        PopPupControl.Instance.CloseAllPopups();
    }

    public void Open(params object[] arguments)
    {
        gameObject.SetActive(true);

        if (arguments.Length > 0) emotionEditing = (ProfileGlobals.Feelings)arguments[0];
        if (arguments.Length > 1) valueEditing = (int)arguments[1];

        slider.sliderValue = valueEditing * 1f / (slider.numberOfSteps - 1);

        slider.onValueChange = delegate
        {
            valueEditing = Mathf.FloorToInt(slider.sliderValue * (slider.numberOfSteps - 1));

            bodytext.text = emotionEditing.ToString() + "  " + valueEditing.ToString();
        };

    }



}
