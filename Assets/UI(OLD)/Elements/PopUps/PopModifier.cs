﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopModifier : MonoBehaviour
{
    public UILabel SelectModifierText, OrTypeHereText, LevelText;

    public UIInput InputModifier;

    public UILabel Level;

    public UITable DefaultModifiersTable;

    public GameObject DefaultModifierPrefab;


    int currentModlevel = 1;

    List<string> defaultModifiersList = new List<string>();


    void OkButton()
    {
        //todo Añadir el modificador a la ficha InputModifier.text

        string modifierName = InputModifier.text;

        int f = -1;
        for (int i = 0; i < UIManager.Instance.profileUIManager.editingProfile.modifiers.Count; i++)
        {
            if (UIManager.Instance.profileUIManager.editingProfile.modifiers[i].name == modifierName)
            {
                f = i;
                break;
            }
        }
        if (f >= 0)
        {
            UIManager.Instance.profileUIManager.editingProfile.modifiers[f].level = currentModlevel;
        }
        else
        {
            Profile.Modifier newMod = new Profile.Modifier();
            newMod.name = modifierName;
            newMod.level = currentModlevel;
            UIManager.Instance.profileUIManager.editingProfile.modifiers.Add(newMod);
        }

        UIManager.Instance.profileUIManager.SaveAndRefresh();

        PopPupControl.Instance.CloseAllPopups();
    }

    public void Open()
    {
        gameObject.SetActive(true);

        currentModlevel = 1;

        PopulateTable();

        Refresh();
    }

    void Refresh()
    {
        if (currentModlevel > 0)
            Level.text = "+" + currentModlevel.ToString();
        else
            Level.text = currentModlevel.ToString();
    }

    void LevelP()
    {
        currentModlevel++;
        if (currentModlevel > ProfileGlobals.e3Values.MaxCharacterModifier)
            currentModlevel = ProfileGlobals.e3Values.MaxCharacterModifier;

        Refresh();
    }

    void LevelM()
    {
        currentModlevel--;
        if (currentModlevel < ProfileGlobals.e3Values.MinCharacterModifier)
            currentModlevel = ProfileGlobals.e3Values.MinCharacterModifier;

        Refresh();
    }

    void AddDefModToTable(Profile.Modifier mod)
    {
        GameObject newDefMod = GameObject.Instantiate(DefaultModifierPrefab) as GameObject;
        newDefMod.transform.parent = DefaultModifiersTable.transform;
        newDefMod.transform.localPosition = Vector3.zero;
        newDefMod.transform.localScale = Vector3.one;
        newDefMod.name = "Default Mod";

        DefaultModifierButton DefMod = newDefMod.GetComponent<DefaultModifierButton>();
        DefMod.PopController = this;
        DefMod.ModifierName = mod.name;
        DefMod.Level = mod.level;

        DefMod.Text.text = LocalizationSystem.NormalizeString(mod.name);
        DefMod.ModifierName = mod.name;
        DefMod.Level = mod.level;

        defaultModifiersList.Add(mod.name);
    }

    void PopulateTable()
    {
        //todo: coger las emociones por defecto de donde sea
        for (int i = 0; i < ProfileManager.DefaultModifiers.Count; i++)
        {
            if (!defaultModifiersList.Contains(ProfileManager.DefaultModifiers[i].name))
                AddDefModToTable(ProfileManager.DefaultModifiers[i]);
        }

        DefaultModifiersTable.Reposition();
    }

    public void SelectModifier(string mod, int lvl)
    {
        InputModifier.text = mod;

        InputModifier.label.text = mod;

        currentModlevel = lvl;

        Refresh();
    }

}
