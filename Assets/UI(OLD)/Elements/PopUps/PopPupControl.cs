﻿using UnityEngine;
using System.Collections;

public class PopPupControl : MonoBehaviour
{
    public static PopPupControl Instance = null;

    void Awake()
    {
        Instance = this;
    }

    public enum PopUps { changeEmotion = 0, deleteProfile, changeName, modifiers };

    public GameObject BackgroundBlock;
    public PopupChangeEmotion popChangeEmotion;
    public GameObject popDeleteProfile;
    public PopUpChangeName popChangeName;
    public UILabel popDeleteProfileLabel;
    public PopModifier popModifier;

    [HideInInspector]
    public string deletingFile = null;

    public void OpenPopup(PopUps which, params object[] arguments)
    {
        //Open the specified one
        switch (which)
        {
            case PopUps.changeEmotion:
                popChangeEmotion.Open(arguments);
                break;

            case PopUps.deleteProfile:
                popDeleteProfileLabel.text = "Delete ?\n" + deletingFile;
                popDeleteProfile.SetActive(true);
                break;

            case PopUps.changeName:
                popChangeName.Open();
                break;

            case PopUps.modifiers:
                popModifier.Open();
                break;
        }

        //todo: Inhabilitar el input sobre el resto de cosas
        BackgroundBlock.SetActive(true);
    }

    public void CloseAllPopups()
    {
        popChangeEmotion.gameObject.SetActive(false);
        popDeleteProfile.SetActive(false);
        popChangeName.gameObject.SetActive(false);
        popModifier.gameObject.SetActive(false);

        BackgroundBlock.SetActive(false);
    }

    public void OkDeleteButton()
    {
        if (deletingFile.Length > 0)
        {
            //todo: borrar el profile
            UIManager.Instance.profileManager.deleteProfile(deletingFile);

            UIManager.Instance.profileUIManager.OpenSelector();

            CloseAllPopups();
        }
    }
}
