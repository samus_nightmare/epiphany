﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIEmotions : MonoBehaviour
{
    public GameObject EmoEntryPrefab;

    public UITable EmoEntryTable;

    public UILabel UIData_Title, UIData_Description;



    public void ShowEmotionDescription()
    {
        UIData_Title.text = LocalizationSystem.NormalizeString(EmotionsWheel.MixResult.Name);

        UIData_Description.text = LocalizationSystem.NormalizeString(EmotionsWheel.MixResult.Description);
        //Debug.Log("Texto normal:\n " + EmotionsWheel.MixResult.Description);
        //Debug.Log("Texto Normalizado:\n " + LocalizationSystem.NormalizeString(EmotionsWheel.MixResult.Description));

    }


    /*
    List<EmoSpellEntry> entrySpells = new List<EmoSpellEntry>();

    public void AddSpell(string emoSpell)
    {
        GameObject newSpell = GameObject.Instantiate(EmoEntryPrefab) as GameObject;
        newSpell.transform.parent = EmoEntryTable.transform;
        newSpell.transform.localScale = Vector3.one;

        EmoSpellEntry newSpellData = newSpell.GetComponent<EmoSpellEntry>();

        //todo: rellenar con los datos de verdad. Ahora está provisional
        newSpellData.spellID = emoSpell;
        newSpellData.spellLabel.text = emoSpell;



        entrySpells.Add(newSpellData);
        EmoEntryTable.Reposition();
    }

    public void ClearAllSpells()
    {
        while (entrySpells.Count > 0)
        {
            DestroyImmediate(entrySpells[0].gameObject);
        }
    }
    */

}
