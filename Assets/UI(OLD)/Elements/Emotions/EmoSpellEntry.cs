﻿using UnityEngine;
using System.Collections;

public class EmoSpellEntry : MonoBehaviour
{
    public UILabel spellLabel;

    [HideInInspector]
    public string spellID = "";



    public void OnClick()
    {
        UIManager.Instance.ChangeWindow(7);

        UIManager.Instance.UIEmotions.ShowEmotionDescription();
    }

}
