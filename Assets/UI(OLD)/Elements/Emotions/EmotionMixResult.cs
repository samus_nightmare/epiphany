﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EmotionMixResult : MonoBehaviour
{
    int maximumNumberOfSparks = 80;
    float RotationSpeed = 40;

    public GameObject ResultFlashParticle;

    public UILabel resultLabel;
    public UISprite resultBack;

    [HideInInspector]
    public List<EmotionSpark> Sparks = new List<EmotionSpark>();

    [HideInInspector]
    public List<GameObject> SparkSlots = new List<GameObject>();




    public Dictionary<ProfileGlobals.Feelings, int> Mix = new Dictionary<ProfileGlobals.Feelings, int>();


    public void AddIngredientToMix(EmotionWheelIngredient Ingredient)
    {
        if (Ingredient.Sparks.Count > 0 && Sparks.Count < maximumNumberOfSparks)
        {
            Ingredient.Sparks[0].springPosition.enabled = true;
            Ingredient.Sparks[0].transform.parent = transform;

            Sparks.Add(Ingredient.Sparks[0]);

            Ingredient.Sparks.RemoveAt(0);
            Ingredient.numberOfSparks--;

            RearrangeSparks();

            //Add ingredient and calculate emotion
            if (!Mix.ContainsKey(Ingredient.BasicEmotion))
            {
                Mix.Add(Ingredient.BasicEmotion, 1);
            }
            else
            {
                Mix[Ingredient.BasicEmotion] = Mix[Ingredient.BasicEmotion] + 1;
            }

            ResultFlash();
        }
    }





    void RearrangeSparks()
    {
        //Borro antiguos slots
        for (int i = 0; i < SparkSlots.Count; i++)
        {
            Destroy(SparkSlots[i]);
        }
        SparkSlots.Clear();

        //Divido la esfera en tantas partes como sparks haya
        float AngleSize = Mathf.CeilToInt(360 / Sparks.Count);

        //Creo un slot en cada una de esas partes
        for (int i = 0; i < Sparks.Count; i++)
        {
            GameObject newSlot = new GameObject();
            newSlot.name = "SparkSlot " + i;
            newSlot.transform.parent = transform;
            float radius = resultBack.transform.localScale.y * 0.65f;
            newSlot.transform.localPosition = new Vector3(0, radius, 0);
            newSlot.transform.RotateAround(transform.position, Vector3.forward, AngleSize * i);
            SparkSlots.Add(newSlot);
        }

    }

    void Update()
    {
        //Rotar slots
        for (int i = 0; i < SparkSlots.Count; i++)
        {
            SparkSlots[i].transform.RotateAround(transform.parent.position, Vector3.forward, RotationSpeed * Time.deltaTime);
            if (i < Sparks.Count)
            {
                Sparks[i].springPosition.target = SparkSlots[i].transform.localPosition;
            }
        }

    }

    public void CalculateResultEmotion()
    {
        EmotionsWheel.MixResult = UIManager.Instance.profileManager.pmAlchemy.GetEffect(Mix);

        if (EmotionsWheel.MixResult != null)
        {
            resultLabel.text = LocalizationSystem.NormalizeString(EmotionsWheel.MixResult.Name);
        }
    }

    void ResultFlash()
    {
        ResultFlashParticle.SetActive(true);
        Invoke("ResultFlashOff", 0.1f);
    }

    void ResultFlashOff()
    {
        ResultFlashParticle.SetActive(false);
        Invoke("CalculateResultEmotion", 0.5f);
    }

    public void Reset()
    {
        Mix.Clear();

        for (int i = 0; i < Sparks.Count; i++)
        {
            Destroy(Sparks[i].gameObject);
        }
        Sparks.Clear();


        for (int i = 0; i < SparkSlots.Count; i++)
        {
            Destroy(SparkSlots[i]);
        }
        SparkSlots.Clear();

        resultLabel.text = "";
    }

    public void ClickResult()
    {
        if (EmotionsWheel.MixResult == null)
            return;

        UIManager.Instance.UIEmotions.ShowEmotionDescription();

        UIManager.Instance.ChangeWindow(7);
    }
}
