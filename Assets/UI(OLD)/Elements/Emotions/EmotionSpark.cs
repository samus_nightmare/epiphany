﻿using UnityEngine;
using System.Collections;

public class EmotionSpark : MonoBehaviour
{
    public UISprite Sprite;

    float RotationSpeed = 40;

    public SpringPosition springPosition;

    void Update()
    {
        if (transform.parent != null && !springPosition.enabled)
            transform.RotateAround(transform.parent.position, Vector3.forward, RotationSpeed * Time.deltaTime);
    }

}
