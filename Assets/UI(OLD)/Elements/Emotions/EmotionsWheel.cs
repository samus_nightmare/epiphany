﻿using UnityEngine;
using System.Collections;

public class EmotionsWheel : MonoBehaviour
{
    public GameObject EmotionSparkPrefab;

    public EmotionMixResult EmotionMixResult;

    public EmotionWheelIngredient Anger, Anticipation, Disgust, Fear, Joy, Sadness, Suprise, Trust;

    [HideInInspector]
    public static AlchemySystem.Effect MixResult = null;

    public void LoadEmotions()
    {
        Anger.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.ANGER));
        Anticipation.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.ANTICIPATION));
        Disgust.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.DISGUST));
        Fear.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.FEAR));
        Joy.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.JOY));
        Sadness.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.SADNESS));
        Suprise.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.SURPRISE));
        Trust.numberOfSparks = System.Convert.ToInt32(UIManager.Instance.profileManager.profile.getFeeling(ProfileGlobals.Feelings.TRUST));

        Anger.RefreshEmotion();
        Anticipation.RefreshEmotion();
        Disgust.RefreshEmotion();
        Fear.RefreshEmotion();
        Joy.RefreshEmotion();
        Sadness.RefreshEmotion();
        Suprise.RefreshEmotion();
        Trust.RefreshEmotion();
    }

    public void Reset()
    {
        EmotionMixResult.Reset();

        EmotionsWheel.MixResult = null;

        LoadEmotions();
    }

    public void Enter()
    {
        if (EmotionMixResult.Mix.Count < 1)
            Reset();
    }

    public void ClearButton()
    {
        Reset();
    }

}
