﻿using UnityEngine;
using System.Collections;
using ProfileGlobals;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class EmotionWheelIngredient : MonoBehaviour
{
    public Feelings BasicEmotion;

    public Color Color = Color.white;

    public UILabel NameLabel;

    public UISprite BackSprite;

    [HideInInspector]
    public List<EmotionSpark> Sparks = new List<EmotionSpark>();

    public int numberOfSparks = 0;


    //void Update()
    //{
    //    RefreshEmotion();
    //}

    public void RefreshEmotion()
    {
        //Set color
        NameLabel.color = Color;

        //Set name
        SetName();

        //Set particles floating around
        SetSparks(numberOfSparks);
    }

    void SetName()
    {
        switch (BasicEmotion)
        {
            case Feelings.ANGER:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Anger;
                break;

            case Feelings.ANTICIPATION:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Anticipation;
                break;

            case Feelings.DISGUST:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Disgust;
                break;

            case Feelings.FEAR:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Fear;
                break;

            case Feelings.JOY:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Joy;
                break;

            case Feelings.SADNESS:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Sadness;
                break;

            case Feelings.SURPRISE:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Surprise;
                break;

            case Feelings.TRUST:
                NameLabel.text = LocalizationSystem.Instance.translation.emo_Name_Trust;
                break;

        }
    }

    void SetSparks(int numberOfSparks)
    {
        //Adjust the number of sparks
        while (numberOfSparks < Sparks.Count)
        {
            Destroy(Sparks[0].gameObject);
            Sparks.RemoveAt(0);
        }

        while (numberOfSparks > Sparks.Count)
        {
            AddSpark();
        }

        //Refresh the sparks
        RefreshSparks();

    }

    void AddSpark()
    {
        //Spawn
        GameObject newSparkObj = GameObject.Instantiate(UIManager.Instance.emotionsWheel.EmotionSparkPrefab) as GameObject;
        newSparkObj.transform.parent = transform;
        newSparkObj.transform.localScale = Vector3.one;
        newSparkObj.name = "Spark " + BasicEmotion.ToString() + " " + Sparks.Count;

        //Orbit
        float radius = BackSprite.transform.localScale.y * 0.65f;
        newSparkObj.transform.localPosition = new Vector3(0, radius, 0);

        //Angle deviation
        newSparkObj.transform.RotateAround(transform.position, Vector3.forward, 40 * Sparks.Count);

        EmotionSpark newSpark = newSparkObj.GetComponent<EmotionSpark>();

        Sparks.Add(newSpark);

    }

    void RefreshSparks()
    {
        for (int i = 0; i < Sparks.Count; i++)
        {
            Sparks[i].Sprite.color = Color;
            //todo: otros cambios a los sparks, aquí
        }
    }

    void OnClick()
    {
        UIManager.Instance.emotionsWheel.EmotionMixResult.AddIngredientToMix(this);
    }

}
