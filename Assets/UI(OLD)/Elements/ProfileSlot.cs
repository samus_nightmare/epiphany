﻿using UnityEngine;
using System.Collections;
using System;

public class ProfileSlot : MonoBehaviour
{
    public UILabel NameLabel;

    public UIButtonMessage buttonEdit, buttonDel;


    [HideInInspector]
    public string fileName;

    [HideInInspector]
    public Transform associatedObject;

    [HideInInspector]
    public Action Command = null;


    public void OnClick()
    {
        if (Command == null)
        {
            UIManager.Instance.profileManager.loadProfile(fileName);

            UIManager.Instance.RefreshUI();

            UIManager.Instance.ChangeWindow(1);
        }
        else
        {
            Command();
        }
    }

}
