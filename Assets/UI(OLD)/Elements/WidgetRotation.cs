﻿using UnityEngine;
using System.Collections;

public class WidgetRotation : MonoBehaviour
{
    public float speed = 0.025f;

    void Update()
    {
        transform.RotateAround(Vector3.forward, speed);
        //transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z + speed));
    }
}
