﻿using UnityEngine;
using System.Collections;

public class CustomScrollbar : MonoBehaviour
{
    public UIScrollBar ScrollBar;
    float sensitivity = 0.9f;

    enum input { NOTHING = 0, PRESSED };



    input _input = input.NOTHING;

    Vector2 _origPos = Vector2.zero;

    float _displacement = 0;

    float _strength = 0f;



    void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            bool _mousePressed = Input.GetMouseButton(0);

            if (_mousePressed)
            {
                switch (_input)
                {
                    case input.NOTHING:
                        _input = input.PRESSED;
                        _origPos = Input.mousePosition;
                        break;

                    case input.PRESSED:
                        _displacement = _origPos.y - Input.mousePosition.y;

                        _strength = Mathf.Clamp(_displacement / 900f, -0.5f, 0.5f);

                        ScrollBar.scrollValue = Mathf.Clamp01(ScrollBar.scrollValue + _strength);

                        break;
                }
            }
            else
            {
                _input = input.NOTHING;
            }
        }
    }
}
