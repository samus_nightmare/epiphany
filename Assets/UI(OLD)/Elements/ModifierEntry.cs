﻿using UnityEngine;
using System.Collections;

public class ModifierEntry : MonoBehaviour
{
    public UILabel label;

    [HideInInspector]
    public string modifierName;

    void OnDestroyClick()
    {
        UIManager.Instance.profileUIManager.DeleteModifier(modifierName);
    }

    void OnEditClick()
    {
        UIManager.Instance.profileUIManager.EditModifier(modifierName);
    }
}
