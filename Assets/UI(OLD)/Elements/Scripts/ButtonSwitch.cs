﻿using UnityEngine;
using System.Collections;

public class ButtonSwitch : Button
{
    bool activated = false;

    public override void OnClick()
    {
        activated = !activated;

        if (activated)
            texture.color = Color.blue;
        else
            texture.color = Color.gray;

        uiManager.ReceiveInput(invoke);
    }
}
