﻿using UnityEngine;
using System.Collections;

public class EmotionButton : Button
{
    [HideInInspector]
    public bool active = true;

    public override void OnClick()
    {
        if (active)
        {
            uiManager.ReceiveInput_Emotion(invoke, this);
        }
    }


    public void SetActive(bool set)
    {
        active = set;
        if (active)
            texture.color = Color.gray;
        else
            texture.color = Color.black;

    }
}
