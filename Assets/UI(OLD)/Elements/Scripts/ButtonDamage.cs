﻿using UnityEngine;
using System.Collections;

public class ButtonDamage : Button
{
    [HideInInspector]
    public bool active = false;



    public override void OnClick()
    {
        //if (active)
        {
            uiManager.ReceiveInput(invoke);
        }
    }


    public void SetActive(bool set)
    {
        active = set;
        if (active)
            texture.color = Color.gray;
        else
            texture.color = Color.black;

    }
}
