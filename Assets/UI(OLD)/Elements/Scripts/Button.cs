﻿using UnityEngine;
using System.Collections;


public class Button : MonoBehaviour
{
    public GUIText label;
    public string invoke = "";

    [HideInInspector]
    public GUITexture texture = null;

    [HideInInspector]
    public UIManager uiManager = null;

    int originalSize;

    [HideInInspector]
    public string originalText;

    void Start()
    {
        originalSize = label.fontSize;

        texture = GetComponent<GUITexture>();

        GameObject go = GameObject.Find("UIManager");
        if (go == null)
        {
            Debug.LogError("Cannot find UIManager object in the scene!");
            Destroy(gameObject);
        }

        uiManager = go.GetComponent<UIManager>();

        originalText = label.text;

    }

    public virtual void OnClick()
    {
        uiManager.ReceiveInput(invoke);
    }

    void LateUpdate()
    {
        label.fontSize = Mathf.RoundToInt(originalSize * ResolutionControl.scale.y);
    }

}
