﻿using UnityEngine;
using System.Collections;

public class ModifierSelector : MonoBehaviour
{
    public UIManager _UIManager;

    public UILabel modifierText;


    void Start()
    {
        _UIManager = GameObject.FindObjectOfType<UIManager>();
    }

    void sit_modM()
    {
        _UIManager.ReceiveInput("sit_modM");
    }

    void sit_modP()
    {
        _UIManager.ReceiveInput("sit_modP");
    }

    void dm_modM()
    {
        _UIManager.ReceiveInput("dm_modM");
    }

    void dm_modP()
    {
        _UIManager.ReceiveInput("dm_modP");
    }


    void em_modM()
    {
        _UIManager.ReceiveInput("em_modM");
    }

    void em_modP()
    {
        _UIManager.ReceiveInput("em_modP");
    }

    void em_ok()
    {
        _UIManager.ReceiveInput("em_ok");
    }

}
