﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIProfile : MonoBehaviour
{
    public GameObject profileSlotPrefab;

    public UITable filesTable;

    List<ProfileSlot> slotsPool = new List<ProfileSlot>();

    List<string> filesName = new List<string>();

    public GameObject modifierEntry;

    public UISlider HealthSlider;

    public UIScrollBar profileSelectorScrollBar;

    public UILabel ExperienceLabel;

    public UIInput NameInput;





    public Profile editingProfile = new Profile();




    public void CreateCommandSlot()
    {
        GameObject newSlotObject = GameObject.Instantiate(profileSlotPrefab) as GameObject;
        newSlotObject.transform.parent = filesTable.transform;
        newSlotObject.transform.localScale = Vector3.one;
        newSlotObject.name = "0C_AddNewProfile";
        ProfileSlot newSlot = newSlotObject.GetComponent<ProfileSlot>();
        newSlot.associatedObject = newSlotObject.transform;
        Destroy(newSlot.buttonDel.gameObject);
        Destroy(newSlot.buttonEdit.gameObject);



        newSlot.NameLabel.text = "[428518]" + LocalizationSystem.Instance.translation.prf_addnew + "[-]";
        newSlot.Command = new System.Action(delegate
        {
            //Qué hacer al pulsar "Añadir nuevo personaje"
            int n = 0;
            for (int i = 0; i < filesName.Count; i++)
                if (filesName[i] == "New Character")
                    n++;

            string newCharacterName = "New Character";

            if (n > 0)
                newCharacterName += " " + n.ToString();

            //UIManager.Instance.profileManager.CreateNewEmptyProfile(newCharacterName);

            UIManager.Instance.profileManager.profile = new Profile();

            UIManager.Instance.profileManager.profile.name = newCharacterName;

            editingProfile = UIManager.Instance.profileManager.profile;

            UIManager.Instance.ChangeWindow(6);

            OpenEditor();

            NameInput.selected = true;
        });



        filesTable.Reposition();
    }


    public void OpenSelector()
    {
        filesName = ProfileXMLSerializer.getAllStoredProfiles();
        PopulateTable();
    }

    void OpenEditor()
    {
        SetEmotionNames();

        Refresh();
    }

    #region Reference Values

    public UILabel characterName;
    public UILabel attVigor, attInsight, attSkill, attPresence;
    public UILabel healthLabel;
    public UISlider healthSlider;
    public UILabel emAnticipation, emSurprise, emSadness, emAnger, emFear, emJoy, emTrust, emDisgust;
    public UILabel lbAnticipation, lbSurprise, lbSadness, lbAnger, lbFear, lbJoy, lbTrust, lbDisgust;
    public UITable modifiersTable;

    #endregion

    #region File Selector

    void PopulateTable()
    {
        //todo: Actualiza la tabla con los archivos. si tiene que sustituir del slotsPool, o borrar, o añadir nuevos slots.

        //Update entries
        for (int i = 0; i < filesName.Count; i++)
        {
            if (slotsPool.Exists(f => f.fileName == filesName[i]))
            {
                ChangeProfileSlot(i, filesName[i]);
            }
            else
            {
                CreateProfileSlot();
                ChangeProfileSlot(i, filesName[i]);
            }
        }

        //Delete deprecated entries
        for (int i = 0; i < slotsPool.Count; i++)
        {
            if (!filesName.Exists(f => f == slotsPool[i].fileName))
            {
                DeleteProfileSlot(slotsPool.Count - 1);
            }
        }


        filesTable.Reposition();
    }

    void CreateProfileSlot()
    {
        GameObject newSlotObject = GameObject.Instantiate(profileSlotPrefab) as GameObject;
        newSlotObject.transform.parent = filesTable.transform;
        newSlotObject.transform.localScale = Vector3.one;
        newSlotObject.name = "Profile Slot";
        ProfileSlot newSlot = newSlotObject.GetComponent<ProfileSlot>();

        newSlot.associatedObject = newSlotObject.transform;
        newSlot.buttonDel.target = gameObject;
        newSlot.buttonDel.functionName = "ButtonProfileDelete";
        newSlot.buttonEdit.target = gameObject;
        newSlot.buttonEdit.functionName = "ButtonProfileEdit";


        slotsPool.Add(newSlot);
    }

    void ChangeProfileSlot(int i, string newName)
    {
        slotsPool[i].fileName = newName;

        Profile theProfile = UIManager.Instance.profileManager.getProfileInfo(slotsPool[i].fileName);

        if (!theProfile.unalterable)
            slotsPool[i].NameLabel.text = LocalizationSystem.NormalizeString(theProfile.name);
        else
        {
            slotsPool[i].NameLabel.text = "[50588C]" + LocalizationSystem.NormalizeString(theProfile.name) + "[-]";
            slotsPool[i].buttonDel.gameObject.SetActive(false);
        }
    }

    void DeleteProfileSlot(int i)
    {
        if (slotsPool[i].associatedObject == null)
            return;

        Destroy(slotsPool[i].associatedObject.gameObject);
        slotsPool.RemoveAt(i);
    }

    void ButtonProfileEdit()
    {
        ProfileSlot _profileSlot = UICamera.lastHit.transform.parent.GetComponent<ProfileSlot>() ?? null;

        if (_profileSlot == null)
            return;

        UIManager.Instance.profileManager.loadProfile(_profileSlot.fileName);

        editingProfile = UIManager.Instance.profileManager.profile;

        UIManager.Instance.ChangeWindow(6);

        OpenEditor();
    }

    void ButtonProfileDelete()
    {
        ProfileSlot _profileSlot = UICamera.lastHit.transform.parent.GetComponent<ProfileSlot>();

        if (_profileSlot == null)
            return;

        PopPupControl.Instance.deletingFile = _profileSlot.fileName;
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.deleteProfile);
    }

    #endregion

    #region File Editor



    #endregion

    #region Editor Inputs

    //Name
    void EditName()
    {
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeName);
    }

    void InputName(string newName)
    {
        editingProfile.name = newName;

        NameInput.text = editingProfile.name;

        UIManager.Instance.profileUIManager.SaveAndRefresh();
    }

    //Plus
    void vigor_P()
    {
        editingProfile.modify(ProfileGlobals.Attributes.VIGOR, 1);
        SaveAndRefresh();
    }

    void insight_P()
    {
        editingProfile.modify(ProfileGlobals.Attributes.INSIGHT, 1);
        SaveAndRefresh();
    }

    void skill_P()
    {
        editingProfile.modify(ProfileGlobals.Attributes.SKILL, 1);
        SaveAndRefresh();
    }

    void presence_P()
    {
        editingProfile.modify(ProfileGlobals.Attributes.PRESENCE, 1);
        SaveAndRefresh();
    }


    //Minus
    void vigor_M()
    {
        editingProfile.modify(ProfileGlobals.Attributes.VIGOR, -1);
        SaveAndRefresh();
    }

    void insight_M()
    {
        editingProfile.modify(ProfileGlobals.Attributes.INSIGHT, -1);
        SaveAndRefresh();
    }

    void skill_M()
    {
        editingProfile.modify(ProfileGlobals.Attributes.SKILL, -1);
        SaveAndRefresh();
    }

    void presence_M()
    {
        editingProfile.modify(ProfileGlobals.Attributes.PRESENCE, -1);
        SaveAndRefresh();
    }

    //Health
    void SetHealth()
    {
        healthSlider.numberOfSteps = (int)ProfileGlobals.HealthLevels.length - 1;
        healthSlider.sliderValue = 1 - ((int)UIManager.Instance.profileUIManager.editingProfile.health) * 1f / healthSlider.numberOfSteps;

        healthSlider.onValueChange = delegate
        {
            editingProfile.health = (ProfileGlobals.HealthLevels)(((int)ProfileGlobals.HealthLevels.length - 1) - Mathf.CeilToInt((healthSlider.sliderValue * healthSlider.numberOfSteps)));

            healthLabel.text = LocalizationSystem.NormalizeString(LocalizationSystem.HealthName(editingProfile.health));

            //todo: cambia el color del texto según el grado de daño


        };
    }

    //Emotions
    void anticipationSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.ANTICIPATION;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void sadnessSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.SADNESS;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void fearSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.FEAR;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void surpriseSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.SURPRISE;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void angerSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.ANGER;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void joySet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.JOY;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void trustSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.TRUST;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    void disgustSet()
    {
        ProfileGlobals.Feelings feeling = ProfileGlobals.Feelings.DISGUST;

        int currentValue = System.Convert.ToInt32(editingProfile.getFeeling(feeling));
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.changeEmotion, feeling, currentValue);

        SaveAndRefresh();
    }

    public void StorePopupEmotion(ProfileGlobals.Feelings feeling, int value)
    {
        editingProfile.setFeeling(feeling, value.ToString());

        SaveAndRefresh();
    }


    //Modifiers
    public void EditModifier(string entry)
    {
        //todo: aun por hacer.
        Debug.Log("Edita " + entry);
    }

    public void DeleteModifier(string entry)
    {
        for (int i = 0; i < editingProfile.modifiers.Count; i++)
        {
            if (editingProfile.modifiers[i].name == entry)
            {
                editingProfile.modifiers.RemoveAt(i);
            }
        }

        modifiersTable.Reposition();

        SaveAndRefresh();
    }

    void AddModifier()
    {
        PopPupControl.Instance.OpenPopup(PopPupControl.PopUps.modifiers);
    }

    #endregion

    void Refresh()
    {
        //Pinta el editingprofile

        //Name
        characterName.text = LocalizationSystem.NormalizeString(editingProfile.name);

        //Attributes    //todo: Esto la verdad sería mejor devolver directamente de la API pero asi funciona 
        attVigor.text = LocalizationSystem.NormalizeString(((ProfileGlobals.AttributesLevel)System.Convert.ToInt32(editingProfile.getAtribute(ProfileGlobals.Attributes.VIGOR))).ToString());
        attInsight.text = LocalizationSystem.NormalizeString(((ProfileGlobals.AttributesLevel)System.Convert.ToInt32(editingProfile.getAtribute(ProfileGlobals.Attributes.INSIGHT))).ToString());
        attSkill.text = LocalizationSystem.NormalizeString(((ProfileGlobals.AttributesLevel)System.Convert.ToInt32(editingProfile.getAtribute(ProfileGlobals.Attributes.SKILL))).ToString());
        attPresence.text = LocalizationSystem.NormalizeString(((ProfileGlobals.AttributesLevel)System.Convert.ToInt32(editingProfile.getAtribute(ProfileGlobals.Attributes.PRESENCE))).ToString());

        //Health
        healthLabel.text = LocalizationSystem.NormalizeString(editingProfile.health.ToString());
        SetHealth();

        //Emotions
        emAnger.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.ANGER));
        emAnticipation.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.ANTICIPATION));
        emFear.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.FEAR));
        emJoy.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.JOY));
        emSadness.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.SADNESS));
        emSurprise.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.SURPRISE));
        emTrust.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.TRUST));
        emDisgust.text = LocalizationSystem.NormalizeString(editingProfile.getFeeling(ProfileGlobals.Feelings.DISGUST));

        //Modificators
        RefreshModificators();

        //Exp
        ExperienceLabel.text = ExperienceCalculate(ref editingProfile).ToString();
    }

    void RefreshModificators()
    {
        ModifierEntry[] tlist = modifiersTable.GetComponentsInChildren<ModifierEntry>();

        for (int i = 0; i < tlist.Length; i++)
        {
            DestroyImmediate(tlist[i].gameObject);
        }

        foreach (var element in editingProfile.modifiers)
        {
            GameObject _entry = Instantiate(modifierEntry) as GameObject;

            _entry.transform.parent = modifiersTable.transform;
            _entry.transform.localScale = Vector3.one;
            ModifierEntry _modEntry = _entry.GetComponent<ModifierEntry>();
            _modEntry.modifierName = element.name;

            string sign = "";
            if (element.level > 0)
                sign = "+";

            _modEntry.label.text = LocalizationSystem.NormalizeString(element.name + "  (" + sign + element.level + ")");
        }

        modifiersTable.Reposition();
    }

    public void Save()
    {
        if (string.IsNullOrEmpty(NameInput.text))
            return;

        editingProfile.name = NameInput.text;

        UIManager.Instance.profileManager.profile = editingProfile;

        //Only write permanent changes if it is alterable
        if (!UIManager.Instance.profileManager.profile.unalterable)
            UIManager.Instance.profileManager.writeProfile();
    }

    public void SaveAndRefresh()
    {
        Save();
        Refresh();
    }

    int ExperienceCalculate(ref Profile _profile)
    {
        float xp = 0;
        int auxiliar;
        //todo: cambiar los cálculos en función del propio manual

        //Attributes
        foreach (string _attValue in _profile.attributes)
        {
            auxiliar = System.Convert.ToInt32(_attValue);
            for (int i = 0; i < auxiliar; i++)
            {
                xp += ProfileGlobals.experienceCosts.attributesUpgrading[i];
            }
        }

        //Emotions
        foreach (string _emoValue in _profile.feelings)
        {
            int emoLevel = System.Convert.ToInt32(_emoValue);
            float xpSum = 0;

            for (int i = 0; i < emoLevel; i++)
                xpSum += ProfileGlobals.experienceCosts.basicEmotions[i];

            xp += xpSum;
        }

        //Modifiers
        foreach (var mod in _profile.modifiers)
        {
            if (mod.level > 0)
            {
                auxiliar = Mathf.Clamp(mod.level - 1, 0, ProfileGlobals.e3Values.MaxCharacterModifier);
                xp += ProfileGlobals.experienceCosts.PositiveModifiers[auxiliar];
            }
            else if (mod.level < 0)
            {
                auxiliar = Mathf.Abs(mod.level) - 1;
                xp += ProfileGlobals.experienceCosts.NegativeModifiers[auxiliar];
            }
        }

        return Mathf.CeilToInt(Mathf.Clamp(xp, 0, 999));
    }

    void SetEmotionNames()
    {
        lbAnger.text = LocalizationSystem.Instance.translation.emo_Name_Anger;
        lbAnticipation.text = LocalizationSystem.Instance.translation.emo_Name_Anticipation;
        lbDisgust.text = LocalizationSystem.Instance.translation.emo_Name_Disgust;
        lbFear.text = LocalizationSystem.Instance.translation.emo_Name_Fear;
        lbJoy.text = LocalizationSystem.Instance.translation.emo_Name_Joy;
        lbSadness.text = LocalizationSystem.Instance.translation.emo_Name_Sadness;
        lbSurprise.text = LocalizationSystem.Instance.translation.emo_Name_Surprise;
        lbTrust.text = LocalizationSystem.Instance.translation.emo_Name_Trust;
    }
}
