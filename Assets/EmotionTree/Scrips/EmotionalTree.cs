﻿using UnityEngine;
using System.Linq;
using System.Collections;

public class EmotionalTree : EmotionalNode 
{
    EmotionalNode ActiveNode;

    void Start()
    {
        ActiveNode = this;
    }

    public void Reset() { ActiveNode = this; }

    internal void GoTo(string name)
    {
        var node = ActiveNode.Childs.FirstOrDefault(n => n.name == name);

        if(node != null)
            ActiveNode = node;
    }

    internal void GotoParent()
    {
        ActiveNode = ActiveNode.Parent;
    }

    public override EmotionalNode Parent
    {
        get { return null; }
    }
}
