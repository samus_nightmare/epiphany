﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EmotionalNode : MonoBehaviour
{
    #region Localization

    [System.Serializable]
    public class Data
    {
        public string language = "Spanish";
        public string name = "Unknown";
        public string description = "Description";
    }

    #endregion

    public Texture Icon;

    public int cost;

    public Data[] Info;

    EmotionalTree Tree
    {
        get
        {
            var Tree = transform.root.GetComponent<EmotionalTree>() as EmotionalTree;
            if (Tree == null) Debug.Log(name + " is out of the EmotionalTree hierarchy");
            return Tree;
        }
    }

    internal void Goto(string name) { Tree.GoTo(name); }

    internal void GoParent() { Tree.GotoParent(); }

    public virtual List<EmotionalNode> Childs
    {
        get
        {
            List<EmotionalNode> childs = new List<EmotionalNode>();
            foreach (Transform child in transform)
            {
                childs.Add(child.gameObject.GetComponent<EmotionalNode>());
            }

            return childs;
        }
    }

    public virtual EmotionalNode Parent
    {
        get
        {
            var p = transform.parent.gameObject.GetComponent<EmotionalNode>();
            if (p == null) Debug.Log(name + "must be in a EmotionalTree hierarchy");
            return p;
        }
    }

    public List<EmotionalNode> GetParentTrace() 
    {
        EmotionalNode node = this;
        List<EmotionalNode> trace = new List<EmotionalNode>();

        if (node.Parent != null)
            while (node.Parent != null)
            {
                trace.Add(node);
                node = node.Parent;
            }
        
        return trace;
    }

    public bool isLeaf { get { return Childs.Count < 1; } }
}
