﻿using UnityEngine;
using System.Collections;

public class SituationsInputReceiver : MonoBehaviour
{
    public enum buttonStatus { closed = 0, busyOpening, opened, busyClosing }
    static float buttonOffsetWhenPressed = 120;

    public UIManager _uiManager;

    public TweenPosition buttonVIGOR, buttonINSIGHT, buttonSKILL, buttonPRESENCE;
    buttonStatus statusVIGOR = buttonStatus.closed, statusINSIGHT = buttonStatus.closed, statusSKILL = buttonStatus.closed, statusPRESENCE = buttonStatus.closed;

    public UISprite statusArrowVIGOR, statusArrowINSIGHT, statusArrowSKILL, statusArrowPRESENCE;



    void buttonPressVIGOR()
    {
        switch (statusVIGOR)
        {
            case buttonStatus.closed:
                statusVIGOR = buttonStatus.busyOpening;
                OpenButton(buttonVIGOR);
                break;

            case buttonStatus.opened:
                statusVIGOR = buttonStatus.busyClosing;
                CloseButton(buttonVIGOR);
                break;

            default: return;
        }

        _uiManager.ReceiveInput("sit_vigor");
    }

    void buttonPressINSIGHT()
    {
        switch (statusINSIGHT)
        {
            case buttonStatus.closed:
                statusINSIGHT = buttonStatus.busyOpening;
                OpenButton(buttonINSIGHT);
                break;

            case buttonStatus.opened:
                statusINSIGHT = buttonStatus.busyClosing;
                CloseButton(buttonINSIGHT);
                break;

            default: return;
        }

        _uiManager.ReceiveInput("sit_insight");
    }

    void buttonPressSKILL()
    {
        switch (statusSKILL)
        {
            case buttonStatus.closed:
                statusSKILL = buttonStatus.busyOpening;
                OpenButton(buttonSKILL);
                break;

            case buttonStatus.opened:
                statusSKILL = buttonStatus.busyClosing;
                CloseButton(buttonSKILL);
                break;

            default: return;
        }

        _uiManager.ReceiveInput("sit_skill");
    }

    void buttonPressPRESENCE()
    {
        switch (statusPRESENCE)
        {
            case buttonStatus.closed:
                statusPRESENCE = buttonStatus.busyOpening;
                OpenButton(buttonPRESENCE);
                break;

            case buttonStatus.opened:
                statusPRESENCE = buttonStatus.busyClosing;
                CloseButton(buttonPRESENCE);
                break;

            default: return;
        }

        _uiManager.ReceiveInput("sit_presence");
    }

    void OpenButton(TweenPosition button)
    {
        button.method = UITweener.Method.BounceIn;

        button.from = new Vector3(0, button.transform.localPosition.y, 0);

        button.to = new Vector3(buttonOffsetWhenPressed, button.transform.localPosition.y, 0);

        button.duration = 0.5f;

        button.enabled = true;

        button.Play(true);
    }

    void CloseButton(TweenPosition button)
    {
        button.method = UITweener.Method.EaseIn;

        button.from = new Vector3(0, button.transform.localPosition.y, 0);

        button.to = new Vector3(buttonOffsetWhenPressed, button.transform.localPosition.y, 0);

        button.duration = 0.33f;

        button.enabled = true;

        button.Play(false);
    }





    void EndAnimationVIGOR()
    {
        EndAnimation(buttonVIGOR, ref statusVIGOR, ref statusArrowVIGOR);
    }

    void EndAnimationINSIGHT()
    {
        EndAnimation(buttonINSIGHT, ref statusINSIGHT, ref statusArrowINSIGHT);
    }

    void EndAnimationSKILL()
    {
        EndAnimation(buttonSKILL, ref statusSKILL, ref statusArrowSKILL);
    }

    void EndAnimationPRESENCE()
    {
        EndAnimation(buttonPRESENCE, ref statusPRESENCE, ref statusArrowPRESENCE);
    }

    void EndAnimation(TweenPosition button, ref buttonStatus status, ref UISprite statusArrow)
    {
        button.enabled = false;

        switch (status)
        {
            case buttonStatus.busyOpening:
                status = buttonStatus.opened;
                statusArrow.spriteName = "arrows_L";
                break;

            case buttonStatus.busyClosing:
                status = buttonStatus.closed;
                statusArrow.spriteName = "arrows_R";
                break;
        }
    }

}
